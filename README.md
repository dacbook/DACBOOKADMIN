# dacweb


## Tech Stack

**Client:** Flutter

**Server:** Firebase

**State Management:** GetX, Provider

**Local DB:** GetStorage, Hive, Sqlite

## Requirement

 - [Flutter](https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_3.7.5-stable.zip)
 - [Windows PSv5](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell)
 - [Android Studio](https://developer.android.com/studio)


## Installation

Configuration android license

```bash
  flutter doctor --android-licenses
```
Check flutter config

```bash
  flutter doctor
```
    
## Run Locally

Clone this project

```bash
  git clone https://gitlab.com/dacbook/DACBOOKADMIN.git
```

Go to the project directory

```bash
  cd dacbook
```

Install dependencies

```bash
  pub get
```

Generate dependencies

```bash
  flutter pub run build_runner build --delete-conflicting-outputs
```


Run

```bash
  flutter run 
```

## Release

To run release version, run the following command

```bash
  firebase deploy --only hosting
```

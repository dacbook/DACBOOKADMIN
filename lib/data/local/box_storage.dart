import 'dart:convert';

import 'package:encrypt/encrypt.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'package:flutter/foundation.dart';
import 'package:get_storage/get_storage.dart';

class BoxStorage {
  final GetStorage _getStorage = GetStorage();

  final iv = IV.fromLength(16);
  final keys = Key.fromUtf8('my 32 length kes................');
  final key = Key.fromUtf8('my wifeismyworld................');

  static const _firstInstallKey = 'first_install';
  static const _selectedSplashScreenKey = 'splash_screen';
  static const _userIdKey = 'user_id';
  static const _userKepsekBookmasterId = 'user_kepsek_bookmaster_id';
  static const _usergooglelogin = 'user_google';
  static const _userAvatarKey = 'user_avatar';
  static const _userNameKey = 'user_username';
  static const _userPasswordKey = 'user_password';
  static const _userEmailKey = 'user_email';
  static const _userNisnKey = 'user_nisn';
  static const _userRememberMe = 'user_remember_me';
  static const _followerList = 'user_follower_list';
  static const _userfcmtoken = 'user_fcm_token';
  static const _avatarSelectedCache = 'user_avatar_selected_cache';
  static const _userGoogleCredetials = 'user_google_credentials';

  String encrypt(String keyData) {
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(keyData, iv: iv);
    return encrypted.base64;
  }

  String descript(String keyData) {
    final encrypter = Encrypter(AES(key));
    final decrypted = encrypter.decrypt(Encrypted.fromBase64(keyData), iv: iv);
    return decrypted;
  }

  void setSplashScreen(String splashScreenKey) {
    _getStorage.write(_selectedSplashScreenKey, splashScreenKey);
  }

  String getSplashScreen() => _getStorage.read(_selectedSplashScreenKey) ?? '';

  void deleteSplashScreen() => _getStorage.remove(_selectedSplashScreenKey);

  void setUserId(String userid) {
    _getStorage.write(_userIdKey, userid);
  }

  String getUserId() => _getStorage.read(_userIdKey) ?? '';

  void deleteUserId() => _getStorage.remove(_userIdKey);

  void setUserKepsekBookmasterId(String bookmasterid) {
    _getStorage.write(_userKepsekBookmasterId, bookmasterid);
  }

  String getUserKepsekBookmasterId() =>
      _getStorage.read(_userKepsekBookmasterId) ?? '';

  void deleteUserKepsekBookmasterId() =>
      _getStorage.remove(_userKepsekBookmasterId);

  void setUserGoogleCredential(AuthCredential credential) {
    String credentialstring = credential.toString();
    _getStorage.write(_userGoogleCredetials, credentialstring);
    //debugPrint('success save $credentialstring');
  }

  String getUserGoogleCredential() =>
      _getStorage.read(_userGoogleCredetials) ?? '';

  void deleteUserGoogleCredential() =>
      _getStorage.remove(_userGoogleCredetials);

  void setUserEmail(String userEmail) {
    String encript = encrypt(userEmail);
    _getStorage.write(_userEmailKey, encript);
  }

  //String getUserEmail() => _getStorage.read(_userEmailKey) ?? '';
  String getUserEmail() {
    String cacheUseremail = _getStorage.read(_userEmailKey) ?? '';
    if (cacheUseremail != '' || cacheUseremail.isNotEmpty) {
      String desEmail = descript(cacheUseremail);
      return desEmail;
    } else {
      return cacheUseremail;
    }
  }

  void deleteUserEmail() => _getStorage.remove(_userEmailKey);

  void setUserAvatarSelectedCache(String userid) {
    _getStorage.write(_avatarSelectedCache, userid);
  }

  String getUserAvatarSelectedCache() =>
      _getStorage.read(_avatarSelectedCache) ?? '';

  void deleteUserAvatarSelectedCache() =>
      _getStorage.remove(_avatarSelectedCache);

  void setUserFcmToken(String userid) {
    _getStorage.write(_userfcmtoken, userid);
  }

  String getUserFcmToken() => _getStorage.read(_userfcmtoken) ?? '';

  void deleteUserFcmToken() => _getStorage.remove(_userfcmtoken);

  void setUserAvatar(String userAvatar) {
    _getStorage.write(_userAvatarKey, userAvatar);
  }

  String getUserAvatar() => _getStorage.read(_userAvatarKey) ?? '';

  void deleteUserAvatar() => _getStorage.remove(_userAvatarKey);

  void setUserName(String username) {
    _getStorage.write(_userNameKey, username);
  }

  String getUserName() => _getStorage.read(_userNameKey) ?? '';

  void deleteUserName() => _getStorage.remove(_userNameKey);

  void setUserPassword(String password) {
    String encript = encrypt(password);
    _getStorage.write(_userPasswordKey, encript);
  }

  String getUserPassword() {
    String cache = _getStorage.read(_userPasswordKey) ?? '';
    if (cache != '' || cache.isNotEmpty) {
      String descryptString = descript(cache);
      return descryptString;
    } else {
      return cache;
    }
  }

  void deleteUserPassword() => _getStorage.remove(_userPasswordKey);

  void setUserNisn(String nisn) {
    _getStorage.write(_userNisnKey, nisn);
  }

  String getUserNisn() => _getStorage.read(_userNisnKey) ?? '';

  void deleteUserNisn() => _getStorage.remove(_userNisnKey);

  void setUserRememberMe() {
    _getStorage.write(_userRememberMe, true);
  }

  bool getUserRememberMe() {
    final isRememberMe = _getStorage.read(_userRememberMe);
    if (isRememberMe != null) return true;
    return false;
  }

  void deleteUserRememberMe() => _getStorage.remove(_userRememberMe);

  void setUserGoogleLogin() {
    _getStorage.write(_usergooglelogin, true);
  }

  bool getUserGoogleLogin() {
    final isRememberMe = _getStorage.read(_usergooglelogin);
    if (isRememberMe != null) return true;
    return false;
  }

  void deleteUserGoogleLogin() => _getStorage.remove(_usergooglelogin);

  void setFirstInstall() {
    _getStorage.write(_firstInstallKey, true);
    return;
  }

  void deleteFirstInstall() => _getStorage.remove(_firstInstallKey);

  bool getFirstInstall() {
    final isFirstInstall = _getStorage.read(_firstInstallKey);
    if (isFirstInstall != null) return true;
    return false;
  }

  Future<void> clearCache() {
    final isClear = _getStorage.erase();
    return isClear;
  }
}

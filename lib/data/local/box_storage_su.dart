// ignore_for_file: avoid_print

import 'package:encrypt/encrypt.dart';
//import 'package:flutter/foundation.dart';
import 'package:get_storage/get_storage.dart';

class BoxStorageSU {
  final GetStorage _getStorage = GetStorage();

  final iv = IV.fromLength(16);
  final keys = Key.fromUtf8('my 32 length kes................');
  final key = Key.fromUtf8('my wifeismyworld................');
  String salt = '@myWifeiseverythings';
  
  static const _userEmailKey = 'su_book_data';
  static const _suBookData = 'su_book_data';
  static const _suUserData = 'su_user_data';
  
  String encrypt(String keyData) {
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(keyData, iv: iv);
    return encrypted.base64;
  }

  String descript(String keyData) {
    final encrypter = Encrypter(AES(key));
    final decrypted = encrypter.decrypt(Encrypted.fromBase64(keyData), iv: iv);
    return decrypted;
  }

  
  void setUserEmail(String userEmail) {
    String encript = encrypt(userEmail);
    _getStorage.write(_userEmailKey, encript);
  }

  //String getUserEmail() => _getStorage.read(_userEmailKey) ?? '';
  String getUserEmail() {
    String cacheUseremail = _getStorage.read(_userEmailKey) ?? '';
    if (cacheUseremail != '' || cacheUseremail.isNotEmpty) {
      String desEmail = descript(cacheUseremail);
      return desEmail;
    } else {
      return cacheUseremail;
    }
  }

  void deleteUserEmail() => _getStorage.remove(_userEmailKey);

  void setBookData(String data) {
    String encript = encrypt(data.toString());
    _getStorage.write(_suBookData, encript);
  }

  //String getUserEmail() => _getStorage.read(_userEmailKey) ?? '';
  String getBookData() {
    String cache = _getStorage.read(_suBookData) ?? '';
    if (cache != '' || cache.isNotEmpty) {
      String desEmail = descript(cache);
      return desEmail;
    } else {
      return cache;
    }
  }

  void deleteBookData() => _getStorage.remove(_suBookData);

  void setUserData(String data) {
   // print('set user data storage : $data');
    String encript = encrypt(data.toString());
    _getStorage.write(_suUserData, encript);
  }

  //String getUserEmail() => _getStorage.read(_userEmailKey) ?? '';
  String getUserData() {
    String cache = _getStorage.read(_suUserData) ?? '';
    //print('get user data storage : $cache');
    if (cache != '' || cache.isNotEmpty) {
      String descrypt = descript(cache);
      return descrypt;
    } else {
      return cache;
    }
  }

  void deleteUserData() => _getStorage.remove(_suUserData);

  
}

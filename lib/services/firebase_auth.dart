import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/main.dart';
import 'package:dacweb/screens/login/login_screen.dart';
import 'package:dacweb/screens/login_su/login_su_screen.dart';
import 'package:dacweb/screens/main/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../data/local/box_storage.dart';
import '../models/user/user_model.dart';
import 'dart:html' as html;


class FirebaseAuthServices {
  final BoxStorage _boxStorage = BoxStorage();
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  final CollectionReference collectionuser =
      FirebaseFirestore.instance.collection("user");
  String? userUid;
  List userList = [];
  late UserModel? userModel;

  Future<UserModel> loginCurrentUser(String uid) async {
    return collectionuser.doc(uid).get().then((value) =>
        UserModel.fromJson(value.data() as Map<String, dynamic>));
  }

  Future<UserModel> loginUser() async {
    String uid = _boxStorage.getUserId();
    debugPrint('clog login user ($uid) data');
    return collectionuser.doc(uid).get().then((value) =>
        UserModel.fromJson(value.data() as Map<String, dynamic>));
  }

  Stream<UserModel> getUserdetail() {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('user')
        .doc(uid)
        .snapshots()
        .map((event) => UserModel.fromJson(event.data() as Map<String, dynamic>));
  }

  Future<String> login({
    required String email,
    required String password,
  }) async {
    debugPrint('$email - $password');
    try {
      // await FirebaseAuth.instance.signOut();
      UserCredential result = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      debugPrint('er : $result');
      // ignore: unnecessary_null_comparison
      if (result != null) {
        debugPrint('er 2 : $result');
        _boxStorage.setUserId(result.user!.uid);
        //_boxStorage.setUserEmail(result.user!.email!);
        debugPrint('run cloduser..');
        String useriddata = _boxStorage.getUserId();
        debugPrint('uid : $useriddata');
        _boxStorage.setUserEmail(email);
        _boxStorage.setUserPassword(password);
        // UserModel usermodel = await loginCurrentUser(result.user!.uid);
        // String username = usermodel.username;
        // String photo = usermodel.avatar;
        // _boxStorage.setUserAvatar(photo);
        // _boxStorage.setUserName(username);
        // Get.snackbar('Berhasil Masuk', 'Halo : ${result.user!.displayName}');
        if (useriddata != '') {
          UserModel usermodel = await loginCurrentUser(result.user!.uid);
          switch (usermodel.userRole){
            case 'user':
            Get.snackbar('Information', 'user is logged in');
            Get.offAll(() => const MyApp(isloggedin: true,));
            break;
            case 'kepala sekolah':
            Get.snackbar('Information', 'user is logged in');
            Get.offAll(() => const MyApp(isloggedin: true,));
            break;
            case 'superadmin':
            Get.snackbar('Information', 'super admin is logged in');
            Get.offAll(() => const LoginSUScreen());
            break;
            default:
             Get.snackbar('Information', 'unable access on application');
              Get.offAll(() => const MyApp(isloggedin: false,));
             _boxStorage.clearCache(); 
          }
         //html.window.location.reload();
        
        } else{
          html.window.location.reload();
        }
      }
      return 'success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        debugPrint('No user found for that email.');
        Get.snackbar('Information', e.code.toString());
        _boxStorage.clearCache();
      } else if (e.code == 'wrong-password') {
        debugPrint('Wrong password provided for that user.');
        Get.snackbar('Information', e.code.toString());
        _boxStorage.clearCache();
      } else if (e.code == 'invalid-email') {
        debugPrint('error Case invalid-email.');
        Get.snackbar('Information', e.code.toString());
        _boxStorage.clearCache();
      }
      debugPrint('Case ${e.code}');
      // Get.snackbar('Information', e.code.toString());
      _boxStorage.clearCache();
      //html.window.location.reload();
      return 'failed';
    }
  }
}

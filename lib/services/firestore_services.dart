import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/models/user/user_model.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../models/book/book_model.dart';
import '../screens/login_su/controllers/loginsu_controller.dart';

class FireStoreDataBase {
  List bookList = [];
  final CollectionReference collectionRef =
      FirebaseFirestore.instance.collection("book");
  final LoginSUController loginSUController = Get.put(LoginSUController());

  CollectionReference<Map<String, dynamic>> streamData(
      {String? collectionName}) {
    final CollectionReference<Map<String, dynamic>> collectionRef =
        FirebaseFirestore.instance.collection(collectionName!);
    CollectionReference<Map<String, dynamic>> datastream = collectionRef;
    return datastream;
  }

  CollectionReference streamCollectionGroup({String? collectionName}) {
    final CollectionReference collectionRef =
        FirebaseFirestore.instance.collection(collectionName!);
    return collectionRef;
  }

  CollectionReference<Map<String, dynamic>> streamDataWithSubCollection(
      {required String collectionName,
      required String documentId,
      required String subcollectionName}) {
    final CollectionReference<Map<String, dynamic>> collectionRef =
        FirebaseFirestore.instance
            .collection(collectionName)
            .doc(documentId)
            .collection(subcollectionName);
    CollectionReference<Map<String, dynamic>> datastream = collectionRef;
    return datastream;
  }

  Future<List> getAllBookData() async {
    try {
      //to get data from a single/particular document alone.
      // var temp = await collectionRef.doc("<your document ID here>").get();

      // to get data from all documents sequentially
      await collectionRef.get().then((querySnapshot) {
        for (var result in querySnapshot.docs) {
          bookList.add(result.data());
        }
      });

      return bookList;
    } catch (e) {
      debugPrint("Error - $e");
      return [];
    }
  }

  Future<String> createBookWithCSV(
      List csvListData, String bookmasterid) async {
    try {
      var date = DateTime.now();
      final time = DateFormat('hh:mm');

      for (var element in csvListData) {
        debugPrint('eksekusi data $element');
        DocumentReference qsBook = collectionRef.doc();
        await qsBook.set({
          'id': qsBook.id,
          'bookmasterid': bookmasterid,
          'ismasterbook': false,
          'bookname': element[0].toString(),
          'description': element[1].toString(),
          'bookimage':
              'https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo.png?alt=media&token=2375fed1-8208-44f0-84e6-86b8ea8a0525',
          'date': date.toString(),
          'time': time.format(DateTime.now()),
          'isofficial': 'official',
          'ispublic': 'public',
          'istype': 'reguler',
          'ownerid': 'admin',
          'pin': element[2].toString(),
          'sharelink': 'sharelink',
          'status': 'aktif',
          'totalmember': FieldValue.increment(0),
          'bookmember': FieldValue.arrayUnion([]),
          'bookadmin': FieldValue.arrayUnion([]),
          'bookmemberrequest': FieldValue.arrayUnion([]),
        });
        debugPrint('eksekusi data end');
      }
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> createDocument(
      {required String collectionName,
      required Map<String, dynamic> object}) async {
    CollectionReference collectionRef =
        FirebaseFirestore.instance.collection(collectionName);
    DocumentReference queryreference = collectionRef.doc();

    try {
      queryreference.set({'id': queryreference.id, 'data': object});
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> updateDocument(
      {required String collectionName,
      required String documentId,
      required Map<String, dynamic> object}) async {
    CollectionReference collectionRef =
        FirebaseFirestore.instance.collection(collectionName);
    DocumentReference queryreference = collectionRef.doc(documentId);

    try {
      queryreference.update({'data': object});
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> updateDocumentBookmember(
      {required String collectionName,
      required String bookid,
      required String documentId,
      required String subcollectionName,
      required String level}) async {
    CollectionReference collectionRef =
        FirebaseFirestore.instance.collection(collectionName);
    DocumentReference queryreference =
        collectionRef.doc(bookid).collection(subcollectionName).doc(documentId);
    debugPrint(
        'collenction name : $collectionName($subcollectionName) - level : $level - documentid : $documentId ');
    try {
      CollectionReference collectionRefBookAdmin =
          FirebaseFirestore.instance.collection(collectionName);
      DocumentReference queryreferenceBookAdmin =
          collectionRefBookAdmin.doc(bookid);
      debugPrint(
          'collenction name : $collectionName($subcollectionName) - level : $level - documentid : $documentId ');
      if (level != 'admin') {
        queryreferenceBookAdmin.update({
          'bookadmin': FieldValue.arrayRemove([documentId])
        });
      } else {
        queryreferenceBookAdmin.update({
          'bookadmin': FieldValue.arrayUnion([documentId])
        });
      }
      queryreference.update({'memberlevel': level});
      // delete from admin member

      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> createDocumentMix(
      {required String collectionName,
      required Map<String, dynamic> object}) async {
    debugPrint('running create doc mix');
    CollectionReference collectionRef =
        FirebaseFirestore.instance.collection(collectionName);
    DocumentReference queryreference = collectionRef.doc();
    var date = DateTime.now();
    final time = DateFormat('hh:mm');
    Map<String, dynamic> data = {
      'id': queryreference.id,
      'bookmasterid': queryreference.id,
      'ismasterbook': true,
      'bookname': object['bookname'],
      'description': object['description'],
      'bookimage':
          'https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo.png?alt=media&token=2375fed1-8208-44f0-84e6-86b8ea8a0525',
      'date': date.toString(),
      'time': time.format(DateTime.now()),
      'isofficial': 'official',
      'ispublic': 'public',
      'istype': 'reguler',
      'ownerid': 'admin',
      'pin': object['pin'],
      'sharelink': 'sharelink',
      'status': 'aktif',
      'totalmember': FieldValue.increment(0),
      'bookmember': FieldValue.arrayUnion([]),
      'bookadmin': FieldValue.arrayUnion([]),
      'bookmemberrequest': FieldValue.arrayUnion([]),
    };
    try {
      debugPrint('process create doc mix');
      queryreference.set(data);
      await FirebaseFirestore.instance
          .collection('book')
          .doc(queryreference.id)
          .get()
          .then((value) {
        debugPrint('get data ${queryreference.id} - ${value.data()}');
        BookModel bookModel = BookModel.fromJson(value.data()!);
        debugPrint('get data $bookModel');
        loginSUController.onAddUpdateBook(bookModel);
      });
      debugPrint('success create doc mix');
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> updateDocumentMix(
      {required String collectionName,
      required String documentId,
      required Map<String, dynamic> object,
      required BookModel databook}) async {
    var collectionRef =  FirebaseFirestore.instance.collection(collectionName);
  print('target : $collectionName, $documentId , $object');
    try {
      databook = BookModel.fromJson(object);
      debugPrint('data buku : $databook');

      loginSUController.onUpdateLocalBook(databook);
      collectionRef.doc(documentId).update({
        //'bookimage': object['bookimage'],
        'bookname': object['bookname'],
        'pin': object['pin'],
        'description': object['description'],
      });
        await FirebaseFirestore.instance
          .collection('book')
          .doc(documentId)
          .get()
          .then((value) {
        debugPrint('get data $documentId - ${value.data()}');
        BookModel bookModel = BookModel.fromJson(value.data()!);
        debugPrint('get data $bookModel');
        loginSUController.onAddUpdateBook(bookModel);
      });
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }
  Future<String> updateBook(
      {
        required BookModel databook,
      required String documentId,
      required Map<String, dynamic> object,}) async {
    var collectionRef = FirebaseFirestore.instance.collection('book');

    try {
      debugPrint('data ($documentId) map : $object');
      collectionRef.doc(documentId).update({
        //'bookimage': object['bookimage'],
        'bookname': object['bookname'],
        'pin': object['pin'],
        'description': object['description'],
      });
      databook = BookModel.fromJson(object);
      debugPrint('data buku : $databook');

      loginSUController.onUpdateLocalBook(databook);
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }
  Future<String> updateBookGalery(
      {
      required String documentId,
      required Map<String, dynamic> object,}) async {
    var collectionRef = FirebaseFirestore.instance.collection('bookgallery');

    try {
      debugPrint('data map : $object');
      collectionRef.doc(documentId).update({
        'title': object['title'],
        'description': object['description'],
      });
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }
  Future<String> updateBookForum(
      {
      required String documentId,
      required Map<String, dynamic> object,}) async {
    var collectionRef = FirebaseFirestore.instance.collection('bookforum');

    try {
      debugPrint('data map : $object');
      collectionRef.doc(documentId).update({
        'title': object['title'],
        'description': object['description'],
      });

      
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> updateDocumentBookLogo(
      {required String collectionName,
      required String documentId,
      required Map<String, dynamic> object}) async {
    var collectionRef = FirebaseFirestore.instance.collection(collectionName);

    try {
      collectionRef.doc(documentId).update(object);
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> updateDocumentDinamic(
      {required String collectionName,
      required String documentId,
      required Map<String, dynamic> object}) async {
    var collectionRef = FirebaseFirestore.instance.collection(collectionName);

    try {
      collectionRef.doc(documentId).update(object);
      return 'success';
    } catch (e) {
      debugPrint("Error - $e");
      return e.toString();
    }
  }

  Future<String> deleteDocument(
      {required String documentId, required String collectionName}) async {
    CollectionReference collectionRef =
        FirebaseFirestore.instance.collection(collectionName);
    DocumentReference qsBook = collectionRef.doc(documentId);
    try {
      qsBook.delete();
      debugPrint("Success deleted");
      return 'success';
    } catch (e) {
      debugPrint("Error - ${e.toString()}");
      return 'failed';
    }
  }

  Future<String> deleteDocumentOnSubCollection(
      {required String documentId,
      required String collectionName,
      required String subdocumentid,
      required String subcollectionname}) async {
    CollectionReference collectionRef = FirebaseFirestore.instance
        .collection(collectionName)
        .doc(documentId)
        .collection(subcollectionname);
    DocumentReference qsBook = collectionRef.doc(subdocumentid);
    try {
      qsBook.delete();
      debugPrint("Success deleted");
      return 'success';
    } catch (e) {
      debugPrint("Error - ${e.toString()}");
      return 'failed';
    }
  }
}

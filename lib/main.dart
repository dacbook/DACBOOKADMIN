import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/constants.dart';
import 'package:dacweb/controllers/menu_controller.dart' as menucontroller;
import 'package:dacweb/screens/login/login_screen.dart';
import 'package:dacweb/screens/main/main_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_web_plugins/url_strategy.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  usePathUrlStrategy();
  await GetStorage.init();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyCnMtR-oOiMz8tGJnxlMLdS6tO8HskJfEE",
          authDomain: "digital-alumni-ilmci.firebaseapp.com",
          databaseURL:
              "https://digital-alumni-ilmci-default-rtdb.asia-southeast1.firebasedatabase.app",
          projectId: "digital-alumni-ilmci",
          storageBucket: "digital-alumni-ilmci.appspot.com",
          messagingSenderId: "933898109767",
          appId: "1:933898109767:web:d7ddd48c33919d8f7fc262",
          measurementId: "G-WGQ1BV7PSB"));
  runApp(const GetMaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(isloggedin: false),
  ));
}



class MyApp extends StatelessWidget {
  const MyApp({super.key, required this.isloggedin});
  final bool isloggedin;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final Future<FirebaseApp> initialization = Firebase.initializeApp();
    return FutureBuilder(
      // Initialize FlutterFire:
      future: initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return const Center(
            child: Text('Internal Network Error'),
          );
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return AdaptiveTheme(
            light: ThemeData.light().copyWith(
              scaffoldBackgroundColor: creamColor,
              textTheme:
                  (Theme.of(context).textTheme).apply(bodyColor: Colors.black),
              canvasColor: snowColor,
            ),
            dark: ThemeData.dark().copyWith(
              scaffoldBackgroundColor: bgColor,
              textTheme:
                  (Theme.of(context).textTheme).apply(bodyColor: Colors.white),
              canvasColor: secondaryColor,
            ),
            initial: AdaptiveThemeMode.light,
            builder: (theme, darkTheme) => MaterialApp(
              //navigatorKey: context.read<menucontroller.MenuController>().scaffoldKey,
              title: 'Digital Alumni',
              theme: theme,
              darkTheme: darkTheme,
              debugShowCheckedModeBanner: false,
              home: MultiProvider(
                providers: [
                  ChangeNotifierProvider(
                    create: (context) => menucontroller.MenuController(),
                  ),
                ],
                // child: const MainScreen(),
                child: isloggedin == true
                    ? const MainScreen()
                    : const LoginScreen(),
              ),
            ),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return const Center(
          child: SizedBox(
              height: 100, width: 100, child: CircularProgressIndicator()),
        );
      },
    );
  }
}

/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsIconsGen {
  const $AssetsIconsGen();

  /// File path: assets/icons/Documents.svg
  SvgGenImage get documents => const SvgGenImage('assets/icons/Documents.svg');

  /// File path: assets/icons/Figma_file.svg
  SvgGenImage get figmaFile => const SvgGenImage('assets/icons/Figma_file.svg');

  /// File path: assets/icons/Search.svg
  SvgGenImage get search => const SvgGenImage('assets/icons/Search.svg');

  /// File path: assets/icons/doc_file.svg
  SvgGenImage get docFile => const SvgGenImage('assets/icons/doc_file.svg');

  /// File path: assets/icons/drop_box.svg
  SvgGenImage get dropBox => const SvgGenImage('assets/icons/drop_box.svg');

  /// File path: assets/icons/excle_file.svg
  SvgGenImage get excleFile => const SvgGenImage('assets/icons/excle_file.svg');

  /// File path: assets/icons/folder.svg
  SvgGenImage get folder => const SvgGenImage('assets/icons/folder.svg');

  /// File path: assets/icons/google_drive.svg
  SvgGenImage get googleDrive =>
      const SvgGenImage('assets/icons/google_drive.svg');

  /// File path: assets/icons/logo.svg
  SvgGenImage get logo => const SvgGenImage('assets/icons/logo.svg');

  /// File path: assets/icons/media.svg
  SvgGenImage get media => const SvgGenImage('assets/icons/media.svg');

  /// File path: assets/icons/media_file.svg
  SvgGenImage get mediaFile => const SvgGenImage('assets/icons/media_file.svg');

  /// File path: assets/icons/menu_dashbord.svg
  SvgGenImage get menuDashbord =>
      const SvgGenImage('assets/icons/menu_dashbord.svg');

  /// File path: assets/icons/menu_doc.svg
  SvgGenImage get menuDoc => const SvgGenImage('assets/icons/menu_doc.svg');

  /// File path: assets/icons/menu_notification.svg
  SvgGenImage get menuNotification =>
      const SvgGenImage('assets/icons/menu_notification.svg');

  /// File path: assets/icons/menu_profile.svg
  SvgGenImage get menuProfile =>
      const SvgGenImage('assets/icons/menu_profile.svg');

  /// File path: assets/icons/menu_setting.svg
  SvgGenImage get menuSetting =>
      const SvgGenImage('assets/icons/menu_setting.svg');

  /// File path: assets/icons/menu_store.svg
  SvgGenImage get menuStore => const SvgGenImage('assets/icons/menu_store.svg');

  /// File path: assets/icons/menu_task.svg
  SvgGenImage get menuTask => const SvgGenImage('assets/icons/menu_task.svg');

  /// File path: assets/icons/menu_tran.svg
  SvgGenImage get menuTran => const SvgGenImage('assets/icons/menu_tran.svg');

  /// File path: assets/icons/one_drive.svg
  SvgGenImage get oneDrive => const SvgGenImage('assets/icons/one_drive.svg');

  /// File path: assets/icons/pdf_file.svg
  SvgGenImage get pdfFile => const SvgGenImage('assets/icons/pdf_file.svg');

  /// File path: assets/icons/sound_file.svg
  SvgGenImage get soundFile => const SvgGenImage('assets/icons/sound_file.svg');

  /// File path: assets/icons/unknown.svg
  SvgGenImage get unknown => const SvgGenImage('assets/icons/unknown.svg');

  /// File path: assets/icons/xd_file.svg
  SvgGenImage get xdFile => const SvgGenImage('assets/icons/xd_file.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        documents,
        figmaFile,
        search,
        docFile,
        dropBox,
        excleFile,
        folder,
        googleDrive,
        logo,
        media,
        mediaFile,
        menuDashbord,
        menuDoc,
        menuNotification,
        menuProfile,
        menuSetting,
        menuStore,
        menuTask,
        menuTran,
        oneDrive,
        pdfFile,
        soundFile,
        unknown,
        xdFile
      ];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/dac cover 1.png
  AssetGenImage get dacCover1 =>
      const AssetGenImage('assets/images/dac cover 1.png');

  /// File path: assets/images/ilmci logo.png
  AssetGenImage get ilmciLogo =>
      const AssetGenImage('assets/images/ilmci logo.png');

  /// File path: assets/images/login wave.png
  AssetGenImage get loginWave =>
      const AssetGenImage('assets/images/login wave.png');

  /// File path: assets/images/logo.png
  AssetGenImage get logo => const AssetGenImage('assets/images/logo.png');

  /// File path: assets/images/logo_backup.png
  AssetGenImage get logoBackup =>
      const AssetGenImage('assets/images/logo_backup.png');

  /// File path: assets/images/mountant.jpg
  AssetGenImage get mountant =>
      const AssetGenImage('assets/images/mountant.jpg');

  /// File path: assets/images/profile_pic.png
  AssetGenImage get profilePic =>
      const AssetGenImage('assets/images/profile_pic.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [dacCover1, ilmciLogo, loginWave, logo, logoBackup, mountant, profilePic];
}

class Assets {
  Assets._();

  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider() => AssetImage(_assetName);

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated Clip? clipBehavior,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
     // colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
     // clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

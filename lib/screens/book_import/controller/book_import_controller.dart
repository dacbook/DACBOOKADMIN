import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

import '../../../services/firestore_services.dart';

class BookImportController extends GetxController{

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  String collectionName = 'book';
  
  Stream<QuerySnapshot<Object?>> getBookMaster(){
    var databook = FireStoreDataBase().streamData(collectionName: collectionName);
    return databook.where('ismasterbook' , isEqualTo: true).snapshots();
  }

}
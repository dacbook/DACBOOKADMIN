// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csv/csv.dart';
import 'package:dacweb/data/local/box_storage.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
//import 'package:data_table_2/data_table_2.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:dacweb/main.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../../services/firestore_services.dart';
import '../dashboard/components/storage_info_card.dart';
import '../data_buku/controller/book_controller.dart';
import 'controller/book_import_controller.dart';

class BookImportScreen extends StatefulWidget {
  const BookImportScreen({Key? key, required this.iskepsek}) : super(key: key);
  final bool iskepsek;
  @override
  State<BookImportScreen> createState() => _BookImportScreenState();
}

class _BookImportScreenState extends State<BookImportScreen> {
  final db = FirebaseFirestore.instance;
  final BoxStorage boxStorage = BoxStorage();
  PlatformFile? selectedFile;
  String? selectedValue;
  List csvListData = [];

  final TextEditingController textEditingController = TextEditingController();
  //String? fileName;

  List<PlatformFile>? _paths;
  String downloadcsv =
      'https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Fdac_book_master.csv?alt=media&token=e26a4779-10dd-457c-b628-fe994399d3df';

  final BookImportController bookImportController =
      Get.put(BookImportController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.iskepsek == true) {
      setState(() {
        bookmasterid = boxStorage.getUserKepsekBookmasterId();
      });
    }
  }

  void pickFiles() async {
    try {
      _paths = (await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: false,
        onFileLoading: (FilePickerStatus status) => print(status),
        allowedExtensions: ['csv'],
      ))
          ?.files;
    } on PlatformException catch (e) {
      debugPrint('Unsupported operation$e');
    } catch (e) {
      debugPrint(e.toString());
    }
    setState(() {
      if (_paths != null) {
        if (_paths != null) {
          //decode bytes back to utf8
          final bytes = utf8.decode(_paths!.first.bytes!);
          //from the csv plugin
          List<List<dynamic>> rowsAsListOfValues =
              const CsvToListConverter().convert(bytes);

          setState(() {
            csvListData = rowsAsListOfValues;
          });
        }
      } else {
        _paths = null;
      }
    });
  }

  selectCSVFile() async {
    resetState();
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      // allowMultiple: false,
      // withData: true,
      // type: FileType.custom,
      //allowedExtensions: ['csv']
      allowedExtensions: ['csv'],
      type: FileType.custom,
      allowMultiple: true,
      withReadStream: true,
      withData: true,
      onFileLoading: (FilePickerStatus status) => print(status),
    );

    if (result != null) {
      // Uint8List? fileBytes = result.files.first.bytes;
      // fileName = result.files.first.name;
      // selectedFile = result.files.first;
      debugPrint('file : ${selectedFile!.path}');
      // await loadCSVFile();
    } else {
      selectedFile = null;
      Get.to(const MyApp(
        isloggedin: true,
      ));
    }
  }

  loadCSVFile(String fileName) async {
    final rawData = fileName;
    List listData = const CsvToListConverter().convert(rawData);
    setState(() {
      csvListData = listData;
    });
  }

  loadCSVFile2() async {
    debugPrint('tes data');
    // final rawData = fileName;
    // List listData = const CsvToListConverter().convert(rawData);
    // setState(() {
    //   csvListData = listData;
    // });
  }

  void resetState() {
    if (!mounted) {
      return;
    }
    setState(() {
      csvListData = [];
      _paths = null;
      selectedFile = null;
    });
  }

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw Exception('Could not launch $url');
    }
  }

  String seletectedCategory = '';
  String? bookmasterid;
  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Import Data Buku Alumni"),
      ),
      body: _paths == null
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                    width: 500,
                    child: Text(
                        'Mohon download Template Buku dan edit excel dengan memasukan nama kelas di sekolah anda dengan format cth: Kota Tangerang-SDN 1 Belendung-6A-2023. Lalu masukan file tersebut di import buku')),
                const SizedBox(
                  height: 50,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton.icon(
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                          horizontal: defaultPadding * 1.5,
                          vertical: defaultPadding /
                              (Responsive.isMobile(context) ? 2 : 1),
                        ),
                      ),
                      onPressed: () async {
                        final Uri url = Uri.parse(downloadcsv);
                        _launchInBrowser(url);
                      },
                      icon: const Icon(Icons.import_contacts),
                      label: const Text("Template Buku"),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    ElevatedButton.icon(
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                          horizontal: defaultPadding * 1.5,
                          vertical: defaultPadding /
                              (Responsive.isMobile(context) ? 2 : 1),
                        ),
                      ),
                      onPressed: () async {
                        pickFiles();
                      },
                      icon: const Icon(Icons.import_contacts),
                      label: const Text("Import Buku"),
                    ),
                  ],
                ),
              ],
            ))
          : ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton.icon(
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: defaultPadding * 1.5,
                            vertical: defaultPadding /
                                (Responsive.isMobile(context) ? 2 : 1),
                          ),
                        ),
                        onPressed: () async {
                          pickFiles();
                        },
                        icon: const Icon(Icons.import_contacts),
                        label: const Text("Import Buku"),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      isloading == false
                          ? ElevatedButton.icon(
                              style: TextButton.styleFrom(
                                backgroundColor: seletectedCategory == ''
                                    ? Colors.red
                                    : Colors.blue,
                                padding: EdgeInsets.symmetric(
                                  horizontal: defaultPadding * 1.5,
                                  vertical: defaultPadding /
                                      (Responsive.isMobile(context) ? 2 : 1),
                                ),
                              ),
                              onPressed: () async {
                                setState(() {
                                  isloading = true;
                                });
                                if (csvListData.isNotEmpty &&
                                    csvListData != [] &&
                                    bookmasterid != null) {
                                  try {
                                    await FireStoreDataBase().createBookWithCSV(
                                        csvListData, bookmasterid!);
                                    Get.snackbar(
                                        'Buku sudah berhasil di upload',
                                        'terima kasih');

                                    setState(() {
                                      csvListData = [];
                                      bookmasterid = null;
                                    });
                                  } catch (e) {
                                    debugPrint("Error - $e");
                                  }
                                } else {
                                  Get.snackbar('Information',
                                      'You get error access, select master book',
                                      backgroundColor: Colors.white);
                                }
                                setState(() {
                                  isloading = false;
                                });
                              },
                              icon: const Icon(Icons.send),
                              label: const Text("Submit Buku"),
                            )
                          : const CircularProgressIndicator()
                    ],
                  ),
                ),
                widget.iskepsek == true
                    ? const SizedBox.shrink()
                    : Container(
                        height: 60,
                        padding: const EdgeInsets.all(5),
                        child: StreamBuilder<QuerySnapshot>(
                            stream: db
                                .collection('book')
                                .where('ismasterbook', isEqualTo: true)
                                .snapshots(),
                            builder: (context, snapshot) {
                              if (!snapshot.hasData) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              }

                              return Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton2<String>(
                                    isExpanded: true,
                                    hint: Text(
                                      'Tentukan master buku',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Theme.of(context).hintColor,
                                      ),
                                    ),
                                    items: snapshot.data!.docs
                                        .map((DocumentSnapshot doc) {
                                      return DropdownMenuItem<String>(
                                          onTap: () {
                                            bookmasterid =
                                                doc['bookmasterid'];
                                            debugPrint(
                                                'book id : $bookmasterid');
                                          },
                                          value: doc['bookname'],
                                          child: Text(doc['bookname']));
                                    }).toList(),
                                    value: selectedValue,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedValue = value as String;
                                      });
                                    },
                                    buttonStyleData: const ButtonStyleData(
                                      height: 40,
                                      width: 500,
                                    ),
                                    dropdownStyleData:
                                        const DropdownStyleData(
                                      maxHeight: 300,
                                    ),
                                    menuItemStyleData:
                                        const MenuItemStyleData(
                                      height: 40,
                                    ),
                                    dropdownSearchData: DropdownSearchData(
                                      searchController:
                                          textEditingController,
                                      searchInnerWidgetHeight: 50,
                                      searchInnerWidget: Container(
                                        height: 50,
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                          bottom: 4,
                                          right: 8,
                                          left: 8,
                                        ),
                                        child: TextFormField(
                                          expands: true,
                                          maxLines: null,
                                          controller: textEditingController,
                                          decoration: InputDecoration(
                                            isDense: true,
                                            contentPadding:
                                                const EdgeInsets.symmetric(
                                              horizontal: 10,
                                              vertical: 8,
                                            ),
                                            hintText:
                                                'Search for an item...',
                                            hintStyle: const TextStyle(
                                                fontSize: 12),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                            ),
                                          ),
                                        ),
                                      ),
                                      searchMatchFn: (item, searchValue) {
                                        return (item.value
                                            .toString()
                                            .toLowerCase()
                                            .contains(searchValue));
                                      },
                                    ),
                                    //This to clear the search value when you close the menu
                                    onMenuStateChange: (isOpen) {
                                      if (!isOpen) {
                                        textEditingController.clear();
                                      }
                                    },
                                  ),
                                ),
                              );
                            }),
                      ),
                Center(
                  child: StorageInfoCard(
                    svgSrc: "assets/icons/Documents.svg",
                    title: 'Total buku yang akan di input kedalam database',
                    amountOfFiles: '',
                    ismasterbook: false,
                    numOfFiles: csvListData.length,
                    isimage: false,
                    isbook: true,
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: DataTable(
                    columnSpacing: defaultPadding,
                    // minWidth: 700,
                    columns: const [
                      DataColumn(
                        label: Text(
                          "Nama Buku",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      DataColumn(
                        label: Text("Keterangan",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      DataColumn(
                        label: Text("Pin",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    ],
                    rows: List.generate(
                      csvListData.length,
                      (index) => recentFileDataRow(csvListData[index]),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}

DataRow recentFileDataRow(dataList) {
  return DataRow(
    cells: [
      DataCell(
        Text(
          dataList[0].toString(),
          overflow: TextOverflow.ellipsis,
        ),
      ),
      DataCell(Text(
        dataList[1].toString(),
        overflow: TextOverflow.ellipsis,
      )),
      DataCell(Text(
        dataList[2].toString(),
        overflow: TextOverflow.ellipsis,
      )),
    ],
  );
}

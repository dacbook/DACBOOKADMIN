import 'package:dacweb/models/user/user_model.dart';
import 'package:easy_sidemenu/easy_sidemenu.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/local/box_storage.dart';
import '../../../services/firebase_auth.dart';
import '../../dashboard/dashboard_screen.dart';
import '../../data_buku/book_data_screen.dart';
import '../../management_role/management_role_screen.dart';

class MainController extends GetxController {
  @override
  void onInit() async {
    // todo
    super.onInit();
    getUser();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  final usermodel = Rxn<UserModel>();
  final FirebaseAuthServices firebaseAuthServices = FirebaseAuthServices();

  var selectedIndex = 0.obs;
  var selectedPage = <Widget>[].obs;
  var pagename = ''.obs;
  var selectedPages = const <Widget>[DashboardScreen(), BookDataScreen(), ManagementRoleScreen()];
  var menurail = const <NavigationRailDestination>[
    NavigationRailDestination(
      icon: SizedBox.shrink(),
      selectedIcon: SizedBox.shrink(),
      label: Text('Dashboard'),
    ),
    NavigationRailDestination(
      icon: SizedBox.shrink(),
      selectedIcon: SizedBox.shrink(),
      label: Text('Data Buku'),
    ),
    NavigationRailDestination(
      icon: SizedBox.shrink(),
      selectedIcon: SizedBox.shrink(),
      label: Text('Data Role'),
    ),
    NavigationRailDestination(
      icon: SizedBox.shrink(),
      selectedIcon: SizedBox.shrink(),
      label: Text('Data Pengguna'),
    ),
    NavigationRailDestination(
      icon: SizedBox.shrink(),
      selectedIcon: SizedBox.shrink(),
      label: Text('Kirim Notifikasi'),
    ),
  ].obs;

  var menulabel = [
    'Dashboard',
    'Data Buku',
    'Data Role',
    'Data Pengguna',
    'Kirim Notifikasi'
  ].obs;

  int get index => selectedIndex.value;
  String get namepage => pagename.value;
  List<Widget> get pages => selectedPages;

  rebuildScreen(int currentIndex, BuildContext context, String pagenames) => {
        selectedIndex.value = currentIndex,
        selectedPage.value = selectedPages,
        pagename.value = pagenames,
      };

  getUser() async {
    usermodel.value = await firebaseAuthServices.loginUser();
    debugPrint('user role : ${usermodel.value!.userRole}');
    return usermodel.value;
  }

 

}

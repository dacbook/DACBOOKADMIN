import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/controllers/menu_controller.dart' as menucontroller;
import 'dart:html' as html;
import 'package:dacweb/data/local/box_storage.dart';
import 'package:dacweb/main.dart';
import 'package:dacweb/responsive.dart';
import 'package:dacweb/screens/dashboard/dashboard_screen.dart';
import 'package:dacweb/screens/login/login_screen.dart';
import 'package:dacweb/screens/main/controller/main_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:easy_sidemenu/easy_sidemenu.dart';

import '../../gen/assets.gen.dart';
import '../../models/user/user_model.dart';
import '../../services/firebase_auth.dart';
import '../data_buku/book_data_screen.dart';
import '../data_buku/controller/book_controller.dart';
import '../data_feeder/feeder_data_screen.dart';
import '../data_user/user_data_screen.dart';
import '../management_role/management_role_screen.dart';
import '../setting/setting_screen.dart';
//import 'components/side_menu.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  NavigationRailLabelType labelType = NavigationRailLabelType.all;
  bool showLeading = false;
  bool showTrailing = false;
  double groupAligment = -1.0;
  final FirebaseAuthServices firebaseAuthServices = FirebaseAuthServices();
  final MainController mainController = Get.put(MainController());

  final BookController bookController = Get.put(BookController());

  PageController page = PageController();
  SideMenuController sideMenu = SideMenuController();
  String? userrole;
  final db = FirebaseFirestore.instance;
  final BoxStorage boxStorage = BoxStorage();
  String? localid;
  @override
  void initState() {
    sideMenu.addListener((p0) {
      page.jumpToPage(p0);
    });

    setState(() {
      localid = boxStorage.getUserId();
    });
    super.initState();
  }

  void onSlidingUser(int pages, SideMenuController sideMenuController) {
    sideMenu.changePage(pages);
  }

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: context.read<menucontroller.MenuController>().scaffoldKey,
      body: StreamBuilder(
          stream: db.collection('user').doc(localid).snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            var doc = snapshot.data!.data();
            UserModel usermodel = UserModel.fromJson(doc!);
            userrole = usermodel.userRole;
            debugPrint('user roles : $userrole');
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SideMenu(
                  controller: sideMenu,
                  style: SideMenuStyle(
                    // showTooltip: false,
                    displayMode: SideMenuDisplayMode.auto,
                    hoverColor: Colors.blue[100],
                    selectedColor: Colors.lightBlue,
                    selectedTitleTextStyle:
                        const TextStyle(color: Colors.white),
                    selectedIconColor: Colors.white,
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.all(Radius.circular(10)),
                    // ),
                    // backgroundColor: Colors.blueGrey[700]
                  ),
                  title: Column(
                    children: [
                      ConstrainedBox(
                          constraints: const BoxConstraints(
                            maxHeight: 150,
                            maxWidth: 150,
                          ),
                          child: Assets.images.logo.image()),
                      const Divider(
                        indent: 8.0,
                        endIndent: 8.0,
                      ),
                      Column(
                        children: [
                          Text(usermodel.username),
                          Text(
                            usermodel.userRole,
                            style: const TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      const Divider(
                        indent: 8.0,
                        endIndent: 8.0,
                      ),
                    ],
                  ),
                  footer: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'DIGITAL ALUMNI @2023',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  items: [
                    SideMenuItem(
                      priority: 0,
                      title: 'Dashboard',
                      onTap: (page, _) {
                        bookController.bookmasteridlocal.value = '';
                        sideMenu.changePage(page);
                      },
                      icon: const Icon(Icons.home),
                      badgeContent: const Text(
                        '3',
                        style: TextStyle(color: Colors.white),
                      ),
                      tooltipContent: "This is a tooltip for Dashboard item",
                    ),
                    if (userrole != 'kepala sekolah')
                      SideMenuItem(
                        priority: 1,
                        title: 'Users',
                        onTap: (page, _) {
                          sideMenu.changePage(page);
                        },
                        icon: const Icon(Icons.supervisor_account),
                      ),
                    if (userrole != 'kepala sekolah')
                      SideMenuItem(
                        priority: 2,
                        title: 'Story',
                        onTap: (page, _) {
                          sideMenu.changePage(page);
                        },
                        icon: const Icon(Icons.supervisor_account),
                      ),

                    SideMenuItem(
                      priority: 3,
                      title: 'Data Buku',
                      onTap: (page, _) {
                        bool isdetail = bookController.isdetailbook.value;
                        if (isdetail == true &&
                            bookController.bookmasteridlocal.value != '') {
                          setState(() {
                            bookController.isdetailbook.value = false;
                            bookController.bookmasteridlocal.value = '';
                          });
                        }
                        sideMenu.changePage(page);
                      },
                      icon: const Icon(Icons.file_copy_rounded),
                      trailing: Container(
                          decoration: const BoxDecoration(
                              color: Colors.amber,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(6))),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 6.0, vertical: 3),
                            child: Text(
                              'New',
                              style: TextStyle(
                                  fontSize: 11, color: Colors.grey[800]),
                            ),
                          )),
                    ),
                    if (userrole != 'kepala sekolah')
                      SideMenuItem(
                        priority: 4,
                        title: 'Role Management',
                        onTap: (page, _) {
                          sideMenu.changePage(page);
                        },
                        icon: const Icon(Icons.download),
                      ),
                    // SideMenuItem(
                    //   priority: 5,
                    //   title: 'FMS',
                    //   onTap: (page, _) {
                    //     sideMenu.changePage(page);
                    //   },
                    //   icon: const Icon(Icons.download),
                    // ),
                     if (userrole != 'kepala sekolah')
                    SideMenuItem(
                      priority: 5,
                      title: 'Settings',
                      onTap: (page, _) {
                        sideMenu.changePage(page);
                      },
                      icon: const Icon(Icons.settings),
                    ),
                    SideMenuItem(
                      priority: 7,
                      title: 'Exit',
                      icon: const Icon(Icons.exit_to_app),
                      onTap: (page, _) {
                        final box = BoxStorage();
                        box.deleteUserEmail();
                        box.deleteUserPassword();
                        //box.clearCache();
                        debugPrint('offline user');
                       
                        html.window.location.reload();
                      },
                    ),
                  ],
                ),
                Expanded(
                  child: PageView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: page,
                    children: const [
                     DashboardScreen(),
                     UserDataScreen(),
                     FeederDataScreen(),
                      BookDataScreen(),
                      ManagementRoleScreen(),
                     // SettingScreen()
                    ],
                  ),
                ),
              ],
            );
          }),
    );
  }
}

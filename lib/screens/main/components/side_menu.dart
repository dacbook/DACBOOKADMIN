import 'package:dacweb/controllers/menu_controller.dart' as menucontroller;
import 'package:dacweb/screens/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';
import '../../book/book_screen.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Image.asset("assets/images/logo.png"),
          ),
          DrawerListTile(
            title: "Dashboard",
            svgSrc: "assets/icons/menu_dashbord.svg",
            press: () {
              Navigator.push(
                  context
                      .read<menucontroller.MenuController>()
                      .scaffoldKey
                      .currentContext!,
                  MaterialPageRoute(
                      builder: (context) => const MyApp(
                           isloggedin: true,
                          )));
                         
            },
          ),
          DrawerListTile(
            title: "Data Pengguna",
            svgSrc: "assets/icons/menu_tran.svg",
            press: () {},
          ),
          DrawerListTile(
            title: "Data Role",
            svgSrc: "assets/icons/menu_task.svg",
            press: () {},
          ),
          // DrawerListTile(
          //   title: "Data Cerita Pengguna",
          //   svgSrc: "assets/icons/menu_doc.svg",
          //   press: () {},
          // ),
          DrawerListTile(
            title: "Data Buku Alumni",
            svgSrc: "assets/icons/menu_store.svg",
            press: () {
              Navigator.push(
                  context
                      .read<menucontroller.MenuController>()
                      .scaffoldKey
                      .currentContext!,
                  MaterialPageRoute(builder: (context) => const BookScreen()));
            },
          ),
          DrawerListTile(
            title: "Kirim Notifikasi",
            svgSrc: "assets/icons/menu_notification.svg",
            press: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        color: Colors.white54,
        height: 16,
      ),
      title: Text(
        title,
        style: Theme.of(context).textTheme.bodyText1,
      ),
    );
  }
}

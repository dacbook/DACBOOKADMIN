import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../services/firestore_services.dart';

class ManagementRoleController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    iscreated.close();
    super.onClose();
  }

  String collectionName = 'mRole';
  var iscreated = false.obs;
  var isupdated = false.obs;

  var id = ''.obs;

  var txtControllerRolename = TextEditingController().obs;

  Stream<QuerySnapshot<Object?>> getMasterRolelist() {
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return databook.snapshots();
  }

  Future<void> createMasterRole(
      {required Map<String, dynamic> createData}) async {
    try {
      String data = await FireStoreDataBase()
          .createDocument(collectionName: collectionName, object: createData);
          if(data == 'success'){
            Get.snackbar('Information', 'Success submit data');
          } else{
             Get.snackbar('Information', 'Failed');
          }
    } catch (e) {
       Get.snackbar('Information', '$e');
      debugPrint("Error - $e");
    }
  }

   Future<void> deleteMasterRole(String id) async{
    await FireStoreDataBase().deleteDocument(documentId: id, collectionName: collectionName);
    debugPrint('success to delete');
  }
   Future<void> updateMasterRole({required String id, required Map<String, dynamic> object}) async{
    await FireStoreDataBase().updateDocument(documentId: id, collectionName: collectionName, object: object);
    debugPrint('success to update');
  }
}

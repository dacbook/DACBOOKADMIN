import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../main/controller/main_controller.dart';
import '../controller/management_role_controller.dart';
import 'create_role.dart';

class ManagementRoleComponentWeb extends StatefulWidget {
  const ManagementRoleComponentWeb({super.key});

  @override
  State<ManagementRoleComponentWeb> createState() =>
      _ManagementRoleComponentWebState();
}

class _ManagementRoleComponentWebState
    extends State<ManagementRoleComponentWeb> {
  final MainController mainController = Get.put(MainController());
  String? userrole;
  @override
  void initState() {
    super.initState();
    userrole = mainController.usermodel.value!.userRole;
    debugPrint('is user role data $userrole');
    // filteredList = dataList;
  }

  @override
  Widget build(BuildContext context) {
    final ManagementRoleController managementRoleController =
        Get.put(ManagementRoleController());
    //   List dataList = [];
    return StreamBuilder<QuerySnapshot<Object?>>(
      stream: managementRoleController.getMasterRolelist(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text(
            "Something went wrong - no data found",
          );
        }
        if (snapshot.connectionState == ConnectionState.active) {
          var dataList = snapshot.data!.docs;

          return Column(
            children: [
              Text(
                "Data Master Role",
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const SizedBox.shrink(),
                    userrole != 'user' &&
                            userrole != 'kepala dinas' &&
                            userrole != 'kepala sekolah'
                        ? TextButton(
                            onPressed: () {
                              setState(() {
                                if (managementRoleController.iscreated.value ==
                                    false) {
                                  managementRoleController.iscreated.value =
                                      true;
                                } else {
                                  managementRoleController.iscreated.value =
                                      false;
                                }
                              });
                            },
                            child: const Text('Tambah role'),
                            //...
                          )
                        : const SizedBox.shrink(),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              managementRoleController.iscreated.value == true
                  ? const Padding(
                      padding: EdgeInsets.all(12.0),
                      child: CreateManagementRole(),
                    )
                  : const SizedBox.shrink(),
              SizedBox(
                width: double.infinity,
                child: DataTable(
                  columnSpacing: 10,
                  //minWidth: 700,
                  columns:  [
                    const DataColumn(
                      label: SizedBox(
                          width: 100,
                          child: Text(
                            "Nama Role",
                            overflow: TextOverflow.ellipsis,
                          )),
                    ),
                    const DataColumn(
                      label: SizedBox(
                          width: 100,
                          child:
                              Text("status", overflow: TextOverflow.ellipsis)),
                    ),
                    const DataColumn(
                      label: SizedBox(
                          width: 100,
                          child: Text("created at",
                              overflow: TextOverflow.ellipsis)),
                    ),
                    const DataColumn(
                      label: SizedBox(
                          width: 100,
                          child: Text("created by",
                              overflow: TextOverflow.ellipsis)),
                    ),
                    DataColumn(
                      label:   userrole != 'user' && userrole != 'kepala dinas' && userrole != 'kepala sekolah' ?  const SizedBox(
                          width: 100,
                          child: Text("Aksi", overflow: TextOverflow.ellipsis)) : const SizedBox.shrink(),
                    ),
                  ],
                  rows: List.generate(
                    dataList.length,
                    (index) {
                      var data = dataList[index].data() as Map<String, dynamic>;
                      return recentFileDataRow(data);
                    },
                  ),
                ),
              ),
            ],
          );
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  DataRow recentFileDataRow(dataList) {
    final ManagementRoleController managementRoleController =
        Get.put(ManagementRoleController());
    var id = dataList['id'];
    var data = dataList['data'];
    return DataRow(
      cells: [
        DataCell(
          Text(
            data["rolename"],
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(data["status"] == true
            ? const Text(
                'Aktif',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              )
            : const Text(
                'Tidak Aktif',
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              )),
        DataCell(Text(data["createdAt"].toString())),
        DataCell(Text(data["createdBy"].toString())),
        DataCell(
            userrole != 'user' && userrole != 'kepala dinas' && userrole != 'kepala sekolah' ? 
          SizedBox(
          width: 100,
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            // IconButton(
            //     icon: const Icon(
            //       Icons.edit,
            //       color: Colors.blue,
            //     ),
            //     tooltip: 'Edit Role',
            //     onPressed: (){
            //       setState(() {
            //          managementRoleController.iscreated.value = true;
            //        managementRoleController.isupdated.value = true;
            //        managementRoleController.id.value = id;
            //        managementRoleController.txtControllerRolename.value.text = data["rolename"];

            //       });
            //     }),

            TextButton(
              onPressed: () => managementRoleController.deleteMasterRole(id),
              child: const Text('Hapus'),
              //...
            ),
          ]),
        ): const SizedBox.shrink()),
      ],
    );
  }
}

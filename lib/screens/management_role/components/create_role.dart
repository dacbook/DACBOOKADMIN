import 'package:dacweb/data/local/box_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/management_role_controller.dart';

class CreateManagementRole extends StatefulWidget {
  const CreateManagementRole({super.key});

  @override
  State<CreateManagementRole> createState() => _CreateManagementRoleState();
}

class _CreateManagementRoleState extends State<CreateManagementRole> {
  final _formKey = GlobalKey<FormState>();
  final ManagementRoleController managementRoleController =
      Get.put(ManagementRoleController());
  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      // constraints: const BoxConstraints(maxWidth: 21),
      height: 150,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child: Column(
            //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextFormField(
                controller:
                    managementRoleController.txtControllerRolename.value,
                keyboardType: TextInputType.text,
                //  autofillHints: const [AutofillHints.email],
                textInputAction: TextInputAction.next,
                onSaved: (String? value) {},
                onChanged: (value) =>
                    managementRoleController.txtControllerRolename.value,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "rolename cannot be empty";
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'masukan role name',
                  labelText: 'User',
                ),
              ),
             const SizedBox(height: 20,),
              isloading == false
                  ? TextButton(
                      onPressed: () {
                        Future.delayed(const Duration(seconds: 2), () {
                          // <-- Delay here
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              isloading = true;
                            });
                            var date = DateTime.now();
                            BoxStorage boxStorage = BoxStorage();
                            Map<String, dynamic> data = {
                              'rolename': managementRoleController
                                  .txtControllerRolename.value.text,
                              'status': true,
                              'createdAt': date.toString(),
                              'createdBy': boxStorage.getUserId(),
                            };
                            FocusManager.instance.primaryFocus?.unfocus();
                            if (managementRoleController.isupdated.value ==
                                    true &&
                                managementRoleController.id.value != '') {
                             
                              managementRoleController.updateMasterRole(
                                  id: managementRoleController.id.value,
                                  object: data);
                              managementRoleController.id.close();
                            } else {
                              managementRoleController.createMasterRole(
                                  createData: data);
                            }

                            managementRoleController.iscreated.value = false;
                            managementRoleController.isupdated.value = false;
                            managementRoleController.txtControllerRolename.value
                                .clear();

                            setState(() {
                              isloading == false;
                            });
                          } else {
                            Get.snackbar('Login Proses', 'login in progress');
                            debugPrint('login in progress..');
                            setState(() {
                              isloading = false;
                            });
                          }
                        });
                      },
                      child: managementRoleController.isupdated.value == true
                          ? const Text('update')
                          : const Text('create'),
                      //...
                    )
                  : const Center(
                      child: SizedBox(
                          height: 30,
                          width: 30,
                          child: CircularProgressIndicator()),
                    ),
              const SizedBox(
                height: 30,
              )
            ]),
      ),
    );
  }
}

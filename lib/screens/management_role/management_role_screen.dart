import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../dashboard/components/header.dart';
import 'components/create_role.dart';
import 'components/management_role_component_web.dart';

class ManagementRoleScreen extends StatefulWidget {
  const ManagementRoleScreen({super.key});

  @override
  State<ManagementRoleScreen> createState() => _ManagementRoleScreenState();
}

class _ManagementRoleScreenState extends State<ManagementRoleScreen> {
 
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
            padding: const EdgeInsets.all(defaultPadding),
            child: Column(
              children: [
                //const Header(),
                const SizedBox(height: defaultPadding),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children:  [
                         if(Responsive.isDesktop(context))
                      // const CreateManagementRole(),
                         const ManagementRoleComponentWeb(),
                         if(Responsive.isMobile(context))
                        // const BookComponentMobile(),
                        const Center(child: Text('not support')),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )));
  }
}

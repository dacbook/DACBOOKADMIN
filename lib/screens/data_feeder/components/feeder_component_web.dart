import 'package:dacweb/screens/data_feeder/controller/feeder_controller.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../models/feed/feed_model.dart';
import '../../../models/user/user_model.dart';
import '../../main/controller/main_controller.dart';
import 'feed_source_datatable.dart';

class FeederComponentWeb extends StatefulWidget {
  const FeederComponentWeb({super.key});

  @override
  State<FeederComponentWeb> createState() => _FeederComponentWebState();
}

class _FeederComponentWebState extends State<FeederComponentWeb> {
  bool isfiltered = false;
  var keyword = TextEditingController();
  String searchResult = '';
  List<FeedModel> listData = [];
  bool isStopped = false;
  bool isChecker = false;

  UserModel? userModel;
  final MainController mainController = Get.put(MainController());

  final List<String> feederCategory = [
    'cerita alumni',
    'cerita bisnis',
    'kegiatan satuan',
    'cerita keluarga',
    'all',
  ];
  String? feederCategorySelected;

  @override
  void initState() {
    super.initState();
    userModel = mainController.usermodel.value!;
    // filteredList = dataList;
  }

  @override
  Widget build(BuildContext context) {
    final FeederController feederController = Get.put(FeederController());
    //   List dataList = [];
    return StreamBuilder(
        stream: feederCategorySelected == null ?  feederController.getFeedList(): feederController.getFeedListFilter(filter: feederCategorySelected!),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Text(
              "Something went wrong - no data found",
            );
          }
          if (snapshot.connectionState == ConnectionState.active) {
            listData = snapshot.data!;
            if (isfiltered == true) {
              String listitems = keyword.text;
              listData = listData.where((FeedModel doc) {
                return listitems
                    .contains(doc.username.toLowerCase().toString());
              }).toList();
            } else {
              listData;
            }

            FeederDataSource dataSource =
                FeederDataSource(listData, userModel!.userRole);
            return Column(
              children: [
                Text(
                  "Data Story Alumni",
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                          width: 300,
                          child: TextField(
                            controller: keyword,
                            decoration: InputDecoration(
                              hintText: "Cari",
                              fillColor: Theme.of(context).canvasColor,
                              filled: true,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              suffixIcon: InkWell(
                                onTap: () {
                                  if (isfiltered == true) {
                                    setState(() {
                                      isfiltered = false;
                                      keyword.clear();
                                      searchResult = '';
                                    });
                                  } else {
                                    setState(() {
                                      isfiltered = true;
                                    });
                                  }
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(
                                      defaultPadding * 0.75),
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: defaultPadding / 2),
                                  decoration: BoxDecoration(
                                    color: isfiltered == false
                                        ? primaryColor
                                        : Colors.red,
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: isfiltered == false
                                      ? SvgPicture.asset(
                                          "assets/icons/Search.svg")
                                      : const Icon(Icons.clear),
                                ),
                              ),
                            ),
                          )),
                      Row(
                        children: const [
                          SizedBox.shrink()
                          // TextButton(
                          //   onPressed: () {
                          //     Get.to(const BookImportScreen());
                          //   },
                          //   child: const Text(
                          //     'Import Buku',
                          //     style: TextStyle(color: Colors.red),
                          //   ),
                          // ),
                          // TextButton(
                          //   onPressed: () {
                          //     setState(() {
                          //       if (bookController.iscreated.value == false) {
                          //         bookController.iscreated.value = true;
                          //       } else {
                          //         bookController.iscreated.value = false;
                          //       }
                          //     });
                          //   },
                          //   child: const Text('Tambah Master Buku'),
                          // ),
                        ],
                      ),
                      DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          hint: Text(
                            'pilih kategori',
                            style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context).hintColor,
                            ),
                          ),
                          items: feederCategory
                              .map((item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Text(
                                      item,
                                      style: const TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                  ))
                              .toList(),
                          value: feederCategorySelected,
                          onChanged: (value) {
                            setState(() {
                              feederCategorySelected = value as String;
                            });
                          },
                          buttonStyleData: const ButtonStyleData(
                            height: 40,
                            width: 140,
                          ),
                          menuItemStyleData: const MenuItemStyleData(
                            height: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                // Obx(
                //   () =>  bookController.iscreated.value == true
                //     ? const Padding(
                //         padding: EdgeInsets.all(12.0),
                //         child: UpdateBook(isupdate : true),
                //       )
                //     : const SizedBox.shrink(),),

                SizedBox(
                  width: double.infinity,
                  child: PaginatedDataTable(
                    source: dataSource,
                    columnSpacing: 10,
                    horizontalMargin: 10,
                    rowsPerPage: 10,
                    showCheckboxColumn: false,
                    //minWidth: 700,
                    columns: const [
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child: Text(
                              "Username",
                              overflow: TextOverflow.ellipsis,
                            )),
                      ),
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child: Text(
                              "Date",
                              overflow: TextOverflow.ellipsis,
                            )),
                      ),
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child:
                                Text("Like", overflow: TextOverflow.ellipsis)),
                      ),
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child: Text("Deskripsi",
                                overflow: TextOverflow.ellipsis)),
                      ),
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child: Text("Kategori",
                                overflow: TextOverflow.ellipsis)),
                      ),
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child: Text("Media",
                                overflow: TextOverflow.ellipsis)),
                      ),
                      // DataColumn(
                      //   label: SizedBox(
                      //       width: 100,
                      //       child: Text("Catatan buku",
                      //           overflow: TextOverflow.ellipsis)),
                      // ),
                      DataColumn(
                        label: SizedBox(
                            width: 100,
                            child:
                                Text("Aksi", overflow: TextOverflow.ellipsis)),
                      ),
                    ],
                  ),
                )
              ],
            );
          }
          return const Center(child: CircularProgressIndicator());
        });
  }
}

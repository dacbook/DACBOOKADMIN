import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:js' as js;

import '../../../models/feed/feed_model.dart';
import '../../main/controller/main_controller.dart';
import '../controller/feeder_controller.dart';

final FeederController bookController = Get.put(FeederController());

class FeederDataSource extends DataTableSource {
  List<FeedModel> dataList;
   String userrole;
  FeederDataSource(this.dataList, this.userrole) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    List count = data.followersfavorites;
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.username,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.date,
            overflow: TextOverflow.ellipsis,
          ),
        ),
       
        DataCell(Text(count.length.toString())),
        DataCell(Text(data.description.toString(), overflow: TextOverflow.ellipsis,)),
        DataCell(Text(data.category[0].toString())),
       data.mediaFeedList.isEmpty ? const DataCell(SizedBox.shrink()) :  DataCell(SizedBox(
          width: 100,
          child: TextButton(
            onPressed: () {
               js.context.callMethod('open', [data.mediaFeedList[0]]);
              // if (data.category[0] == 'foto') {
              
              // }
              // if (data.category[0] == 'video'){
              //    Get.to(CustomVideoControlls(
              //     videoUrl: data.mediaFeedList[0],
              //   ));
              // }
              // if (data.category[0] == 'file'){
              //   Get.snackbar('Information', 'this media file cant prewvoew');
              // }
            },
            child: const Text('Lihat Media'),
            //...
          ),
        )),
        DataCell(
          userrole != 'user' && userrole != 'kepala dinas' ?
          SizedBox(
          width: 100,
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            // IconButton(
            //     icon: const Icon(
            //       Icons.edit,
            //       color: Colors.blue,
            //     ),
            //     tooltip: 'Edit Book',
            //     onPressed: () {
            //       bookController.updateData(
            //           data.bookname, data.pin, data.description, data.id);
            //     }),
            TextButton(
              onPressed: () => bookController.deletebook(data.id),
              child: const Text('Hapus'),
              //...
            ),
          ]),
        ): const SizedBox.shrink()),
      ],
    );
  }
}

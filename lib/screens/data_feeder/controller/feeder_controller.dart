import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../../../models/feed/feed_model.dart';
import '../../../services/firestore_services.dart';

class FeederController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  String collectionName = 'story';

  var iscreated = false.obs;
  var isupdated = false.obs;
  var isfiltered = false.obs;
  var ismasterbook = false.obs;

  Stream<List<FeedModel>> getFeedList() {
    var datasnap =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return datasnap.snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }
  Stream<List<FeedModel>> getFeedListFilter({required String filter}) {
    var datasnap =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return datasnap.where('category', arrayContainsAny: [filter]).snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

   Future<void> deletebook(String bookid) async {
    await FireStoreDataBase()
        .deleteDocument(documentId: bookid, collectionName: collectionName);
    debugPrint('success to delete');
  }

}

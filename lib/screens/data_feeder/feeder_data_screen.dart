import 'package:dacweb/screens/data_feeder/components/feeder_component_web.dart';
import 'package:dacweb/screens/data_user/components/user_component_web.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../responsive.dart';

class FeederDataScreen extends StatefulWidget {
  const FeederDataScreen({super.key});

  @override
  State<FeederDataScreen> createState() => _FeederDataScreenState();
}

class _FeederDataScreenState extends State<FeederDataScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
            padding: const EdgeInsets.all(defaultPadding),
            child: Column(
              children: [
                // const Header(),
                const SizedBox(height: defaultPadding),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          if (Responsive.isDesktop(context))
                            const FeederComponentWeb(),
                          // if (Responsive.isMobile(context))
                          //   const UserComponentMobile(),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )));
  }
}

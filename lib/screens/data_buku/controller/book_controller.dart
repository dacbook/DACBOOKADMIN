import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/services/firestore_services.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/book/book_model.dart';

class BookController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  final FirebaseFirestore _db = FirebaseFirestore.instance;

  var tempbooklist = [].obs;
  var iscreated = false.obs;
  var isupdated = false.obs;
  var isfiltered = false.obs;
  var ismasterbook = false.obs;
  var isdetailbook = false.obs;
  var searchData = ''.obs;
  var bookmasteridlocal = ''.obs;

  List booklist = [];
  String collectionName = 'book';

  var txtcontrollerbookname = TextEditingController().obs;
  var txtcontrollerpin = TextEditingController().obs;
  var txtcontrollerdeskripsi = TextEditingController().obs;
  var id = ''.obs;

  final bookModel = Rxn<BookModel>();
  var isdetail = false.obs;

  Stream<QuerySnapshot<Object?>> getBooklist() {
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return databook.orderBy('date', descending: true).snapshots();
  }

  Stream<List<BookModel>> getBooklistwithModel() {
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return databook.orderBy('date', descending: true).snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data()),
              )
              .toList(),
        );
  }

  Stream<List<BookModel>> getMasterBooklistwithModel() {
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return databook
        .orderBy('date', descending: true)
        .where('ismasterbook', isEqualTo: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data()),
              )
              .toList(),
        );
  }

  Stream<List<BookModel>> getBooklistKepsek(String bookmasterid) {
    debugPrint('bookmaster id : $bookmasterid');
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return databook
        .where('bookmasterid', isEqualTo: bookmasterid)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data()),
              )
              .toList(),
        );
  }

  Stream<QuerySnapshot<Object?>> getBooklistWithFilter(String bookname) {
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return databook
        .orderBy('date', descending: true)
        .where('bookname', isEqualTo: bookname)
        .snapshots();
  }

  Future<void> deletebook(String bookid) async {
    await FireStoreDataBase()
        .deleteDocument(documentId: bookid, collectionName: collectionName);
    debugPrint('success to delete');
  }

  Future<void> updateBook(
      {required String id, required Map<String, dynamic> object, required BookModel databook}) async {
   debugPrint('getting data $object');
    await FireStoreDataBase().updateDocumentMix(
        documentId: id, collectionName: collectionName, object: object, databook: databook);
    debugPrint('success to update');
  }

  Future<void> updateBookLogo(
      {required String id, required Map<String, dynamic> object}) async {
    await FireStoreDataBase().updateDocumentBookLogo(
        documentId: id, collectionName: collectionName, object: object);
    debugPrint('success to update');
  }

  Future<String> createBook({required Map<String, dynamic> createData}) async {
    try {
      debugPrint('data create $createData');
      String data = await FireStoreDataBase().createDocumentMix(
          collectionName: collectionName, object: createData);
      if (data == 'success') {
       // Get.snackbar('Information', 'Success submit data');
        return 'success';
      } else {
       // Get.snackbar('Information', 'Failed');
        return 'error';
      }
    } catch (e) {
    //  Get.snackbar('Information', '$e');
      debugPrint("Error - $e");
      return 'error';
    }
  }

  void updateData(BookModel bookdata) {
    iscreated.value = true;
    isupdated.value = true;
    isdetail.value = true;
    bookModel.value = bookdata;
  }

  void getDetailBookFromTable(BookModel bookdata) {
    iscreated.value = false;
    isupdated.value = false;
    isdetail.value = true;
    bookModel.value = bookdata;
  }

  gotoDetailBook({required bool isdetailbookvalue}) {
    isdetailbook.value = isdetailbookvalue;
  }

  var isdataforumonly = false.obs;
  var isdatabookonly = false.obs;
  var isdatagaleryonly = false.obs;
  var isdatastrukturonly = false.obs;
  var isdataagendaonly = false.obs;
  var isdatauseronly = false.obs;
  var isactived = false.obs;

  var populatealumni = 0.obs;

  selectedOnBookMenus(int index) {
    switch (index) {
      //book
      case 0:
        isdatabookonly.value = true;

        isdatastrukturonly.value = true;
        isdataforumonly.value = false;
        isdatagaleryonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //struktur
      case 1:
        isdatastrukturonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatagaleryonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //galeri
      case 2:
        isdatagaleryonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //forum
      case 3:
        isdataforumonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //agenda
      case 4:
        isdataagendaonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
      case 5:
        isdatauseronly.value = true;

        isdataagendaonly.value = false;
        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
    }
  }

  selectedOnSubBookMenus(int index) {
    switch (index) {
      //struktur
      case 0:
        debugPrint('struktur');
        isdatastrukturonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatagaleryonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //galeri
      case 1:
        debugPrint('galeri');
        isdatagaleryonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //forum
      case 2:
        debugPrint('forum');
        isdataforumonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //agenda
      case 3:
        debugPrint('agenda');
        isdataagendaonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
      case 4:
        debugPrint('member book');
        isdatauseronly.value = true;

        isdataagendaonly.value = false;
        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
    }
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import '../../models/book/book_model.dart';
import '../../responsive.dart';

import 'components/book_component_web.dart';
import 'components/book_component_mobile.dart';
import 'controller/book_controller.dart';

class BookDataScreen extends StatefulWidget {
  const BookDataScreen({super.key});

  @override
  State<BookDataScreen> createState() => _BookDataScreenState();
}

class _BookDataScreenState extends State<BookDataScreen> {

  final BookController bookController = Get.put(BookController());

  @override
  void initState() {
    super.initState();
  }

  

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
            padding: const EdgeInsets.all(defaultPadding),
            child: Column(
              children: [
                // const Header(),
                const SizedBox(height: defaultPadding),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          if (Responsive.isDesktop(context))
                           const BookComponentWeb(),
                          if (Responsive.isMobile(context))
                            const BookComponentMobile(),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )));
  }
}

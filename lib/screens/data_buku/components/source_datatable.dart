// The "soruce" of the table
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../models/book/book_model.dart';
import '../../../models/dialog/dialog_utils.dart';
import '../../../services/firestore_services.dart';
import '../controller/book_controller.dart';

final BookController bookController = Get.put(BookController());
final formKey = GlobalKey<FormState>();
TextEditingController txtcontrollerbookname = TextEditingController();
TextEditingController txtcontrollerpin = TextEditingController();
TextEditingController txtcontrollerdeskripsi = TextEditingController();

class DataSource extends DataTableSource {
  BuildContext context;
  List<BookModel> dataList;
  String userrole;
  DataSource(this.dataList, this.userrole, this.context) {
    //print(dataList);
  }
  // Generate some made-up data

  void onsort<T>(Comparable<T> Function(BookModel d) getField, bool ascending) {
    dataList.sort((BookModel a, BookModel b) {
      if (!ascending) {
        final BookModel c = a;
        a = b;
        b = c;
      }
      final Comparable<T> aValue = getField(a);
      final Comparable<T> bValue = getField(b);
      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;

  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    List countbookmember = data.bookmember;
    return DataRow(
      cells: [
        DataCell(
          SizedBox(
            width: 70,
            child: Image.network(data.bookimage)
          ),
        ),
        DataCell(
          SizedBox(
            width: 200,
            child: Text(
              data.bookname,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        DataCell(
          SizedBox(
            width: 100,
            child: Text(
              data.pin,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        DataCell(data.ismasterbook == true
            ? const Text(
                'Master Buku',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              )
            : const Text(
                'Sub Buku',
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              )),
        DataCell(Text(countbookmember.length.toString())),
        DataCell(SizedBox(
            width: 200,
            child: Text(
              data.description.toString(),
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(userrole != 'user' && userrole != 'kepala dinas'
            ? SizedBox(
                width: 100,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // IconButton(
                      //     icon: const Icon(
                      //       Icons.book,
                      //       color: Colors.blue,
                      //     ),
                      //     tooltip: 'Detail',
                      //     onPressed: () {

                      //      bookController.getDetailBookFromTable(data);

                      //      debugPrint('data book id : $data');
                      //     }),

                      IconButton(
                          icon: const Icon(
                            Icons.edit,
                            color: Colors.blue,
                          ),
                          tooltip: 'Edit Book',
                          onPressed: () {
                            debugPrint('edited');
                            // bookController.updateData(data);
                            txtcontrollerbookname.text = data.bookname;
                            txtcontrollerdeskripsi.text = data.description;
                            txtcontrollerpin.text = data.pin;
                            List<DialogUtilsModel> dialogCreateMasterBuku = [
                              DialogUtilsModel(
                                  icon: const Icon(
                                    Icons.book_outlined,
                                  ),
                                  label: 'Master Buku',
                                  hint: 'Nama Master Buku',
                                  textEditingController: txtcontrollerbookname),
                              DialogUtilsModel(
                                  icon: const Icon(
                                    Icons.lock,
                                  ),
                                  label: 'Pin Buku',
                                  hint: 'Masukan Pin Buku',
                                  textEditingController: txtcontrollerpin),
                              DialogUtilsModel(
                                  icon: const Icon(
                                    Icons.description,
                                  ),
                                  label: 'Keterangan',
                                  hint: 'Masukan Keterangan',
                                  textEditingController:
                                      txtcontrollerdeskripsi),
                            ];
                            createMasterbookBox(
                                context: context,
                                formKey: formKey,
                                buttonLabel: 'Update',
                                title: 'Edit Master Buku',
                                dialogUtilsModel: dialogCreateMasterBuku,
                                databook: data);
                          }),
                      // TextButton(
                      //   onPressed: () => bookController.deletebook(data.id),
                      //   child: const Text('Hapus'),
                      //   //...
                      // ),
                    ]),
              )
            : const SizedBox.shrink()),
      ],
    );
  }

  createMasterbookBox({
    required BuildContext context,
    required GlobalKey<FormState> formKey,
    required String title,
    required String buttonLabel,
    required List<DialogUtilsModel> dialogUtilsModel,
    required BookModel databook,
  }) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            content: Stack(
              clipBehavior: Clip.none,
              children: <Widget>[
                Positioned(
                  right: -10.0,
                  top: -10.0,
                  child: InkResponse(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: const Icon(Icons.close)),
                ),
                SizedBox(
                  width: 450,
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          title,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        IconButton(
                            onPressed: () async {
                              var picked =
                                  await FilePicker.platform.pickFiles();

                              if (picked != null) {
                                print(picked.files.first.name);
                                print(picked.files);
                              }
                            },
                            icon: Icon(Icons.browse_gallery)),
                        for (var dialogModel in dialogUtilsModel)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                                controller: dialogModel.textEditingController,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "cannot be empty";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    isDense: false,
                                    labelText: dialogModel.label,
                                    hintText: dialogModel.hint,
                                    prefixIcon: dialogModel.icon)),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GFButton(
                              size: GFSize.SMALL,
                              text: buttonLabel,
                              textStyle: const TextStyle(
                                  fontSize: 16, color: GFColors.WHITE),
                              // icon: const Icon(
                              //   Icons.check,
                              // ),
                              color: Colors.red,
                              blockButton: true,
                              onPressed: ()
                                  //=>onPressed
                                  {
                                if (formKey.currentState!.validate()) {
                                  formKey.currentState!.save();
                                  print('on pressed validation');

                                  _onUpdateMasterBook(databook);
                                } else {
                                  print('on pressed no validation');
                                }
                                Navigator.of(context).pop();
                              }),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  _onUpdateMasterBook(BookModel bookdata) async {
    if (txtcontrollerbookname.value.text.isNotEmpty &&
        txtcontrollerpin.value.text.isNotEmpty &&
        txtcontrollerpin.value.text.isNotEmpty) {
      print('update masterbook proses..');
      Map<String, dynamic> datamap = {
       // 'bookname': txtcontrollerbookname.text,
        'bookname': txtcontrollerbookname.text,
        'pin': txtcontrollerpin.text,
        'description': txtcontrollerdeskripsi.text,
      };
    
           await FireStoreDataBase()
        .updateBook(documentId: bookdata.id, object: datamap, databook: bookdata);
      txtcontrollerdeskripsi.clear();
      txtcontrollerbookname.clear();
      txtcontrollerpin.clear();
    } else {
      //null;
      print('create masterbook invalid..');
    }
  }
}

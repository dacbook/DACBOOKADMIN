import 'dart:async';

import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/screens/dashboard/components/masterbook_information_dashboard.dart';
import 'package:dacweb/screens/data_buku/controller/book_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutterfire_ui/firestore.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../constants.dart';
import '../../../models/book/book_model.dart';
import '../../../models/dialog/dialog_utils.dart';
import '../../../services/firestore_services.dart';
import '../../../utils/alert_dialog/alert_dialog_dac_controller.dart';
import '../../book_import/book_import_screen.dart';
import '../../dashboard/components/header.dart';
import '../../dashboard/components/subbook_information_dashboard.dart';
import '../../login_su/controllers/loginsu_controller.dart';
import '../../main/controller/main_controller.dart';
import 'book_detail_component.dart';
import 'search_component.dart';
import 'source_datatable.dart';
import 'update_book.dart';
import 'package:anim_search_bar/anim_search_bar.dart';

class BookComponentWeb extends StatefulWidget {
  const BookComponentWeb({
    super.key,
  });

  @override
  State<BookComponentWeb> createState() => _BookComponentWebState();
}

class _BookComponentWebState extends State<BookComponentWeb> {
  bool isfiltered = false;
  Timer? timer;
  var keyword = TextEditingController();
  String searchResult = '';
  List<BookModel> listData = [];
  List<BookModel> listMasterBookLocal = [];
  List<BookModel> filteredData = [];
  final searchController = TextEditingController();
  bool isStopped = false;
  bool isChecker = false;
  bool isselectedMasterbook = false;
  bool isSearchActive = false;
  bool isfilterActive = false;
  final MainController mainController = Get.put(MainController());
  final BookController bookController = Get.put(BookController());
  final LoginSUController loginSUController = Get.put(LoginSUController());
  final AlertDialogDACController alertDialogDACController =
      Get.put(AlertDialogDACController());
  String? bookid;
  String? userrole;
  String? bookmasterid;
  String dropdownvalue = 'master';

  late DataSource data;

  TextEditingController textController = TextEditingController();
  TextEditingController textController2 = TextEditingController();

  TextEditingController txtcontrollerbookname = TextEditingController();
  TextEditingController txtcontrollerpin = TextEditingController();
  TextEditingController txtcontrollerdeskripsi = TextEditingController();

  List<BookModel> listbookdata = [];
  List<String> listDropdown = ['master', 'sub'];
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    userrole = mainController.usermodel.value!.userRole;

    data = DataSource(listbookdata, userrole!, context);
    if (userrole == 'kepala sekolah') {
      bookmasterid = mainController.usermodel.value!.bookmasterkepsek;
    }
    debugPrint('is user role data $userrole');
    fetchAllBook();

    super.initState();
    scheduler();
    // filteredList = dataList;
  }

  @override
  void dispose() {
    super.dispose();
    txtcontrollerbookname.clear();
    txtcontrollerdeskripsi.clear();
    txtcontrollerpin.clear();
  }

  final CollectionReference _collectionRef =
      FirebaseFirestore.instance.collection('book');

  Future<List<BookModel>> getlistLocal() async {
    listbookdata = loginSUController.bookdataSU;
    return listbookdata;
  }

  Future<void> checkmasterbook() async {
    // Get docs from collection reference
    QuerySnapshot querySnapshot =
        await _collectionRef.where('ismasterbook', isEqualTo: true).get();

    // Get data from docs and convert map to List
    listMasterBookLocal = querySnapshot.docs
        .map((doc) => BookModel.fromJson(doc.data() as Map<String, dynamic>))
        .toList();

    debugPrint('running all data : $listMasterBookLocal');
  }

  Future<void> masterbookvalidation() async {}

  Future<void> fetchAllBook() async {
    if (userrole == 'kepala sekolah') {
      QuerySnapshot querySnapshot = await _db
          .collection('book')
          .where('bookmasterid', isEqualTo: bookmasterid)
          .limit(10)
          .get();
      listbookdata = querySnapshot.docs
          .map((doc) => BookModel.fromJson(doc.data() as Map<String, dynamic>))
          .toList();
      setState(() {
      loginSUController.bookdataSU = listbookdata;
        data = DataSource(loginSUController.bookdataSU, userrole!, context);
      });
    } else {
      await Future.delayed(const Duration(seconds: 2), () async {
        loginSUController.bookdataSU = await loginSUController.sugetBooklist();
        setState(() {
          data = DataSource(loginSUController.bookdataSU, userrole!, context);
        });
      });
      debugPrint('book data in web : ${listbookdata.length}');
    }
  }

  void scheduler() {
    timer = Timer.periodic(
        const Duration(seconds: 2), (Timer t) => _onCheckIncomingData());
  }

  void _onSearchTextChanged(String value) {
    print('text (${textController.text.isEmpty}) : ${textController.text}');
    setState(() {
      bookController.isdetail.value = false;
      if (textController.text.isEmpty) {
        filteredData = listbookdata;
        isSearchActive = false;
        textController.text = value;
      } else {
        isSearchActive = true;

        filteredData = listbookdata
            .where((item) => item.bookname
                .toLowerCase()
                .contains(textController.text.toLowerCase()))
            .toList();
      }

      data = DataSource(filteredData, userrole!, context);
    });
    debugPrint('filtered key ${textController.text} data buku : $filteredData');
  }

  void _onSearchTypeBook(String value) {
    if (isfilterActive == true) {
      if (textController2.text == 'master') {
        setState(() {
          isselectedMasterbook = true;
        });
      } else {
        setState(() {
          isselectedMasterbook = false;
        });
      }
      setState(() {
        isSearchActive = true;
        textController2.text = value;
        bookController.isdetail.value = false;
        filteredData = listbookdata
            .where((item) => item.ismasterbook == isselectedMasterbook)
            .toList();
        data = DataSource(filteredData, userrole!, context);
      });
    } else {
      setState(() {
        isSearchActive = false;
        data = DataSource(listbookdata, userrole!, context);
      });
    }

    debugPrint(
        'filtered key : $isselectedMasterbook - ${textController2.text} data buku : $filteredData');
  }

  _onFilterInactived() {
    setState(() {
      textController2.text = '';
      textController2.clear();
      isSearchActive = false;
      isfilterActive = false;

      data = DataSource(listbookdata, userrole!, context);
    });
    print('on filter : $isfilterActive');
  }

  _onFilterActived() {
    setState(() {
      isfilterActive = true;
    });
    print('on filter : $isfilterActive');
  }

  _onCheckIncomingData() {
    //debugPrint('is filter active : $isfilterActive');
    if (isSearchActive == true) {
      setState(() {
        data = DataSource(filteredData, userrole!, context);
        //print('is search actived ${filteredData.length}');
      });
      // print('on search is filtered');
    } else {
      //  print('is search inactived');
      setState(() {
        listbookdata = loginSUController.bookdataSU;
        data = DataSource(listbookdata, userrole!, context);
      });
      // print('on search is clear');
      //    print(
      //     'book lenght (${listbookdata.length}) - (${loginSUController.bookdataSU.length})');
    }
  }

  _onCreateMasterBook()async {

   bool ischecked = await onCheckMasterBook(txtcontrollerbookname.value.text.toLowerCase());
   if(ischecked == true){
     setState(() {
      processReceived = 0.4;
    });
    if (txtcontrollerbookname.value.text.isNotEmpty &&
        txtcontrollerpin.value.text.isNotEmpty &&
        txtcontrollerpin.value.text.isNotEmpty) {
      print('create masterbook proses..');
      Map<String, dynamic> dataregis = {
        'bookname': txtcontrollerbookname.value.text.toLowerCase(),
        'pin': txtcontrollerpin.value.text,
        'description': txtcontrollerdeskripsi.value.text,
      };

      bookController.createBook(createData: dataregis);
      txtcontrollerbookname.text = '';
      txtcontrollerpin.text = '';
      txtcontrollerdeskripsi.text = '';
       await Future.delayed(const Duration(seconds: 2), () async {
        loginSUController.bookdataSU = await loginSUController.sugetBooklist();
        setState(() {
          data = DataSource(loginSUController.bookdataSU, userrole!, context);
        });
      });
      debugPrint('book data in web : ${listbookdata.length}');

    } else {
      //null;
       Get.snackbar('Information', 'Buku telah terdaftar');
      print('create masterbook invalid..');
    }
          setState(() {
        processReceived = 0.9;
        iscreatedBook = false;
      });
   } else{

   }
   
  }

  onCheckMasterBook<bool>(String bookname)async{
      var data =  await FirebaseFirestore.instance.collection("book").where('bookname', isEqualTo: bookname).get();
      if(data.docs.isNotEmpty){
        return false;
      } else{
        return true;
      }

  }

 

  int _sortColumnIndex = 0;
  bool _sortAscending = true;

  void _sort<T>(Comparable<T> Function(BookModel d) getField, int columnIndex,
      bool ascending) {
    data.onsort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  bool iscreatedBook = false;
  double processReceived = 0.0;

  @override
  Widget build(BuildContext context) {
    //   List dataList = [];
    return StreamBuilder(
        // stream: userrole == 'kepala sekolah'
        //     ? bookController.getBooklistKepsek(bookmasterid!)
        //     : bookController.getBooklistwithModel(),
        builder: (contexts, snapshot) {
      if (snapshot.hasError) {
        return const Text(
          "Something went wrong - no data found",
        );
      }

      return listbookdata == []
          ? const CircularProgressIndicator()
          : Column(
              children: [
                Text(
                  "Data Buku Alumni",
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: iscreatedBook == false
                      ? const SizedBox.shrink()
                      : GFProgressBar(
                          percentage: processReceived,
                          backgroundColor: Colors.black26,
                          progressBarColor: GFColors.DANGER),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      userrole != 'user' && userrole != 'kepala dinas'
                          ? SizedBox(
                              width: 340,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SizedBox(
                                      width: 110,
                                      child: GFButton(
                                          size: GFSize.SMALL,
                                          text: 'Import Buku',
                                          textStyle: const TextStyle(
                                              fontSize: 16,
                                              color: GFColors.WHITE),
                                          color: Colors.blueAccent,
                                          blockButton: true,
                                          onPressed: () {
                                            if (userrole == 'kepala sekolah') {
                                              Get.to(const BookImportScreen(
                                                iskepsek: true,
                                              ));
                                            } else {
                                              Get.to(const BookImportScreen(
                                                iskepsek: false,
                                              ));
                                            }
                                          })),
                                  userrole != 'kepala sekolah'
                                      ? SizedBox(
                                          width: 200,
                                          child: GFButton(
                                              size: GFSize.SMALL,
                                              text: 'Tambah Master Buku',
                                              textStyle: const TextStyle(
                                                  fontSize: 16,
                                                  color: GFColors.WHITE),
                                              color: Colors.redAccent,
                                              blockButton: true,
                                              onPressed: () {
                                                List<DialogUtilsModel>
                                                    dialogCreateMasterBuku = [
                                                  DialogUtilsModel(
                                                      icon: const Icon(
                                                        Icons.book_outlined,
                                                      ),
                                                      label: 'Master Buku',
                                                      hint: 'Nama Master Buku',
                                                      textEditingController:
                                                          txtcontrollerbookname),
                                                  DialogUtilsModel(
                                                      icon: const Icon(
                                                        Icons.lock,
                                                      ),
                                                      label: 'Pin Buku',
                                                      hint: 'Masukan Pin Buku',
                                                      textEditingController:
                                                          txtcontrollerpin),
                                                  DialogUtilsModel(
                                                      icon: const Icon(
                                                        Icons.description,
                                                      ),
                                                      label: 'Keterangan',
                                                      hint:
                                                          'Masukan Keterangan',
                                                      textEditingController:
                                                          txtcontrollerdeskripsi),
                                                ];

                                                createMasterbookBox(
                                                  context: context,
                                                  formKey: formKey,
                                                  buttonLabel: 'Tambah',
                                                  title: 'Tambah Master Buku',
                                                  dialogUtilsModel:
                                                      dialogCreateMasterBuku,
                                                );
                                              }))
                                      : const SizedBox.shrink(),
                                ],
                              ),
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                bookController.iscreated.value == true
                    ? Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: UpdateBook(
                          isupdate: true,
                          masterbooklocal: listbookdata,
                        ),
                      )
                    : const SizedBox.shrink(),
                Theme(
                  data: Theme.of(context).copyWith(
                      // cardColor: Colors.white70,
                      // dividerColor: Colors.black12,
                      // highlightColor: Colors.red
                      ),
                  child: PaginatedDataTable(
                    header: const Text('Data Buku'),
                    // source: DataSource(snapshot.docs as List<BookModel>, userrole!),
                    source: data,
                    columnSpacing: 10,
                    horizontalMargin: 10,
                    rowsPerPage: 5,
                    showCheckboxColumn: false,
                    sortColumnIndex: _sortColumnIndex,
                    sortAscending: _sortAscending,
                    //minWidth: 700,
                    // ignore: prefer_const_literals_to_create_immutables
                    actions: [
                      if (isfilterActive)
                        SizedBox(
                          width: 100,
                          height: 55,
                          child: Center(
                            child: CustomDropdown(
                              hintText: 'filter',
                              items: listDropdown,
                              controller: textController2,
                              excludeSelected: false,
                              onChanged: (value) => _onSearchTypeBook(value),
                            ),
                          ),
                        ),
                      SizedBox(
                        height: 55,
                        width: 55,
                        child: isfilterActive == false
                            ? IconButton(
                                splashRadius: 15.0,
                                onPressed: () => _onFilterActived(),
                                icon: const Icon(Icons.filter_list))
                            : IconButton(
                                splashRadius: 15.0,
                                onPressed: () => _onFilterInactived(),
                                icon: const Icon(Icons.cancel)),
                      ),
                      AnimSearchBar(
                        width: 400,
                        autoFocus: true,
                        closeSearchOnSuffixTap: true,
                        textController: textController,
                        onSuffixTap: () {
                          setState(() {
                            textController.clear();
                            isSearchActive = false;
                            data = DataSource(listbookdata, userrole!, context);
                          });
                        },
                        onSubmitted: (value) => _onSearchTextChanged(value),
                      ),
                    ],
                    // ignore: prefer_const_literals_to_create_immutables
                    columns: [
                     const DataColumn(
                        label:  SizedBox(
                            width: 60,
                            child: Text(
                              "Cover",
                              overflow: TextOverflow.ellipsis,
                            )),
                        // onSort: (int columnIndex, bool ascending) =>
                        //     _sort<String>(
                        //         (BookModel d) => d.id, columnIndex, ascending),
                      ),
                      DataColumn(
                        label: const SizedBox(
                            width: 100,
                            child: Text(
                              "Nama Buku",
                              overflow: TextOverflow.ellipsis,
                            )),
                        onSort: (int columnIndex, bool ascending) =>
                            _sort<String>(
                                (BookModel d) => d.id, columnIndex, ascending),
                      ),
                      DataColumn(
                        label: const SizedBox(
                            width: 100,
                            child: Text(
                              "PIN Buku",
                              overflow: TextOverflow.ellipsis,
                            )),
                        onSort: (int columnIndex, bool ascending) =>
                            _sort<String>(
                                (BookModel d) => d.pin, columnIndex, ascending),
                      ),
                      const DataColumn(
                        label: SizedBox(
                            width: 100,
                            child: Text("Status Buku",
                                overflow: TextOverflow.ellipsis)),
                        //   onSort: (int columnIndex, bool ascending) => _sort<bool>((BookModel d) => d.ismasterbook, columnIndex, ascending),
                      ),
                      DataColumn(
                        label: const SizedBox(
                            width: 100,
                            child: Text("Total alumni",
                                overflow: TextOverflow.ellipsis)),
                        onSort: (int columnIndex, bool ascending) => _sort<num>(
                            (BookModel d) => d.totalmember,
                            columnIndex,
                            ascending),
                      ),
                      DataColumn(
                        label: const SizedBox(
                            width: 100,
                            child: Text("Catatan buku",
                                overflow: TextOverflow.ellipsis)),
                        onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((BookModel d) => d.description,
                                columnIndex, ascending),
                      ),
                      DataColumn(
                        label: userrole != 'user' && userrole != 'kepala dinas'
                            ? const SizedBox(
                                width: 100,
                                child: Text("Aksi",
                                    overflow: TextOverflow.ellipsis))
                            : const SizedBox.shrink(),
                      ),
                    ],
                  ),
                ),
                Obx(() => bookController.isdetail.value == false
                    ? const SizedBox.shrink()
                    : BookDetailInformation(
                        bookmodel: bookController.bookModel.value!,
                        userrole: userrole!,
                      )),
              ],
            );
    });
  }

  createMasterbookBox({
    required BuildContext context,
    required GlobalKey<FormState> formKey,
    required String title,
    required String buttonLabel,
    required List<DialogUtilsModel> dialogUtilsModel,
  }) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            content: Stack(
              clipBehavior: Clip.none,
              children: <Widget>[
                Positioned(
                  right: -10.0,
                  top: -10.0,
                  child: InkResponse(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: const Icon(Icons.close)),
                ),
                SizedBox(
                  width: 450,
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          title,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        for (var dialogModel in dialogUtilsModel)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                                controller: dialogModel.textEditingController,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "cannot be empty";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    isDense: false,
                                    labelText: dialogModel.label,
                                    hintText: dialogModel.hint,
                                    prefixIcon: dialogModel.icon)),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GFButton(
                              size: GFSize.SMALL,
                              text: buttonLabel,
                              textStyle: const TextStyle(
                                  fontSize: 16, color: GFColors.WHITE),
                              // icon: const Icon(
                              //   Icons.check,
                              // ),
                              color: Colors.red,
                              blockButton: true,
                              onPressed: ()
                                  //=>onPressed
                                async  {
                                if (formKey.currentState!.validate()) {
                                  formKey.currentState!.save();
                                  print('on pressed validation');
                                  setState(() {
                                    iscreatedBook = true;
                                    processReceived = 0.2;
                                  });

                                  _onCreateMasterBook();
                                  
                                } else {
                                  
                                  print('on pressed no validation');
                                }
                                Navigator.of(context).pop();
                              }),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}

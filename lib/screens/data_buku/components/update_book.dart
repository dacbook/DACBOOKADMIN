import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../data/local/box_storage.dart';
import '../../../main.dart';
import '../../../models/book/book_model.dart';
import '../../../utils/alert_dialog/alert_dialog_dac_controller.dart';
import '../controller/book_controller.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class UpdateBook extends StatefulWidget {
  const UpdateBook(
      {super.key, required this.isupdate, required this.masterbooklocal});
  final bool isupdate;
  final List<BookModel> masterbooklocal;
  @override
  State<UpdateBook> createState() => _UpdateBookState();
}

class _UpdateBookState extends State<UpdateBook> {
  final _formKey = GlobalKey<FormState>();
  final BookController bookController = Get.put(BookController());
  final AlertDialogDACController alertDialogDACController = Get.put(AlertDialogDACController());
  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      // constraints: const BoxConstraints(maxWidth: 21),
      height: 400,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          bookController.isupdated.value == true
              ? const SizedBox.shrink()
              : Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 60,
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.black)),
                    child: const Text(
                      'Mohon diperhatikan, anda sedang membuat buku master. buku ini akan menjadi sebuah induksi dari masing-masing sub-buku yang akan dikelola. apakah anda yakin ?',
                      // overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
          TextFormField(
            controller: bookController.txtcontrollerbookname.value,
            keyboardType: TextInputType.text,
            //  autofillHints: const [AutofillHints.email],
            textInputAction: TextInputAction.next,
            onSaved: (String? value) {},
            onChanged: (value) => bookController.txtcontrollerbookname.value,
            validator: (value) {
              if (value!.isEmpty) {
                return "bookname cannot be empty";
              } else {
                var contain = widget.masterbooklocal
                    .where((element) => element.bookname == value);
                if (contain.isNotEmpty) {
                  return "buku sudah ada";
                }
              }

              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'masukan nama buku',
              labelText: 'buku dac',
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: bookController.txtcontrollerpin.value,
            keyboardType: TextInputType.text,
            //  autofillHints: const [AutofillHints.email],
            textInputAction: TextInputAction.next,
            onSaved: (String? value) {},
            onChanged: (value) => bookController.txtcontrollerpin.value,
            validator: (value) {
              if (value!.isEmpty) {
                return "pin cannot be empty";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'masukan pin buku',
              labelText: '123456',
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: bookController.txtcontrollerdeskripsi.value,
            keyboardType: TextInputType.text,
            //  autofillHints: const [AutofillHints.email],
            textInputAction: TextInputAction.next,
            onSaved: (String? value) {},
            onChanged: (value) => bookController.txtcontrollerdeskripsi.value,
            validator: (value) {
              if (value!.isEmpty) {
                return "description cannot be empty";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'masukan deskripsi buku',
              labelText: 'buku dac adalah..',
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
              width: 200,
              child: GFButton(
                  size: GFSize.SMALL,
                  text: bookController.isupdated.value == true
                      ? 'Simpan Buku'
                      : 'membuat buku master',
                  textStyle:
                      const TextStyle(fontSize: 16, color: GFColors.WHITE),
                  color: Colors.redAccent,
                  blockButton: true,
                  onPressed: () {
                   

                    
                    Future.delayed(const Duration(seconds: 1), () {
                      // <-- Delay here
                      if (_formKey.currentState!.validate()) {
                        setState(() {
                          isloading = true;
                        });

                        Map<String, dynamic> data = {
                          'bookname':
                              bookController.txtcontrollerbookname.value.text,
                          'pin': bookController.txtcontrollerpin.value.text,
                          'description':
                              bookController.txtcontrollerdeskripsi.value.text,
                        };
                        FocusManager.instance.primaryFocus?.unfocus();
                        if (bookController.isupdated.value == true ||
                            bookController.id.value != '') {
                          debugPrint('update book');
                          // bookController.updateBook(
                          //     id: bookController.id.value, object: data, databook: widget.masterbooklocal);
                          bookController.id.close();
                        } else {
                          debugPrint('create book');
                          bookController.createBook(createData: data);
                          String title = 'Successfully';
                          String description = 'Master buku telah dibuat, selanjutnya anda menambahkan sub buku pada menu import buku';
                          alertDialogDACController.onDialogOpened(context: context, title: title, description: description);
                          setState(() {
                            isloading == false;
                          });
                          Get.to(() => const MyApp(
                                isloggedin: true,
                              ));
                        }

                        bookController.iscreated.value = false;
                        bookController.isupdated.value = false;
                        bookController.ismasterbook.value = false;
                        bookController.txtcontrollerbookname.value.clear();
                        bookController.txtcontrollerdeskripsi.value.clear();
                        bookController.txtcontrollerpin.value.clear();
                      } else {
                        Get.snackbar('Login Proses', 'login in progress');
                        debugPrint('login in progress..');
                        setState(() {
                          isloading = false;
                        });
                      }
                      setState(() {
                        isloading == false;
                      });
                    });
                    
                  })),

          //  const Center(
          //     child: SizedBox(
          //         height: 30,
          //         width: 30,
          //         child: CircularProgressIndicator()),
          //   ),
          const SizedBox(
            height: 30,
          )
        ]),
      ),
    );
  }

}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../controller/book_controller.dart';

class SearchFieldBookComponent extends StatelessWidget {
  const SearchFieldBookComponent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final BookController bookController = Get.put(BookController());
    return TextField(
     // controller: bookController.txtsearchData.value,
      decoration: InputDecoration(
        hintText: "Cari",
        fillColor: Theme.of(context).canvasColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        suffixIcon: InkWell(
          onTap: () {
          //  bookController.filterList(true);
            bookController.getBooklist();
               print('is filtered on');
          },
          child: Container(
            padding: const EdgeInsets.all(defaultPadding * 0.75),
            margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
            decoration:  BoxDecoration(
              color: bookController.isfiltered.value == false ? primaryColor : Colors.red,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child:  bookController.isfiltered.value == false ? SvgPicture.asset("assets/icons/Search.svg") : const Icon(Icons.clear),
          ),
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/gen/colors.gen.dart';
import 'package:dacweb/responsive.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:path_provider/path_provider.dart';

import '../../../constants.dart';
import '../../../data/local/box_storage.dart';
import '../../../main.dart';
import '../../../models/book/book_model.dart';
import '../../../models/myfiles.dart';
import '../../dashboard/components/chart.dart';
import '../../dashboard/components/dashboard_dataagenda_table.dart';
import '../../dashboard/components/dashboard_dataforum_table.dart';
import '../../dashboard/components/dashboard_datagalery_table.dart';
import '../../dashboard/components/dashboard_dataorganization_table.dart';
import '../../dashboard/components/dashboard_datauser_table.dart';
import '../../dashboard/components/file_info_card.dart';
import '../../data_buku/controller/book_controller.dart';

final db = FirebaseFirestore.instance;

class BookDetailInformation extends StatefulWidget {
  const BookDetailInformation(
      {super.key, required this.bookmodel, required this.userrole});
  final BookModel bookmodel;
  final String userrole;
  @override
  State<BookDetailInformation> createState() => _BookDetailInformationState();
}

class _BookDetailInformationState extends State<BookDetailInformation> {
  List bookmembers = [];

  String? selectedValue;
  String? kepsekselectedValue;
  final TextEditingController textEditingController = TextEditingController();
  final TextEditingController kepsekEditingController = TextEditingController();

  final BookController bookController = Get.put(BookController());

  final BoxStorage boxStorage = BoxStorage();

  final FirebaseFirestore _db = FirebaseFirestore.instance;

  String? bookmastername;
  bool iskepsekmode = false;
  BookModel? bookdata;
  String? bookimage;

  Uint8List? logotemp;

  double progressUploadMedia = 0.0;

  @override
  void initState() {
    super.initState();
    // bookid = widget.bookmodel.id;
    bookdata = widget.bookmodel;
    setState(() {
      if (widget.userrole == 'kepala sekolah') {
        iskepsekmode = true;
      } else {
        iskepsekmode = false;
      }
      bookController.isactived.value = true;
      bookController.isdatauseronly.value = true;
      bookController.isdatabookonly.value = false;
      bookController.isdataagendaonly.value = false;
      bookController.isdataforumonly.value = false;
      bookController.isdatagaleryonly.value = false;
      bookController.isdatastrukturonly.value = false;
    });
  }


  bool isactived = false;
  bool iskepsekActived = false;

  bool isuploadProcess = false;

  refreshData() {
    setState(() {});
  }

  refreshBookData(String id) async {
    var docSnapshot = await _db.collection('book').doc(id).get();
    if (docSnapshot.exists) {
      setState(() {
        bookdata = BookModel.fromJson(docSnapshot.data()!);
        bookController.getDetailBookFromTable(bookdata!);
        debugPrint('book data : $bookdata');
      });
    }
  }

  upload() async {
// Create the file metadata
    final metadata = SettableMetadata(contentType: "image/jpeg");

// Create a reference to the Firebase Storage bucket
    final storageRef = FirebaseStorage.instance.ref();

// Upload file and metadata to the path 'images/mountains.jpg'
    final uploadTask = storageRef
        .child("bookinfo/jamu45${widget.bookmodel.id}/booklogo.png")
        .putData(logotemp!, metadata);
    final downloadFile = storageRef
        .child("bookinfo/jamu45${widget.bookmodel.id}/booklogo.png")
        .getDownloadURL();

// Listen for state changes, errors, and completion of the upload.
    uploadTask.snapshotEvents.listen((TaskSnapshot taskSnapshot) {
      switch (taskSnapshot.state) {
        case TaskState.running:
          final progress =
              100.0 * (taskSnapshot.bytesTransferred / taskSnapshot.totalBytes);
          setState(() {
            progressUploadMedia = progress;
          });
          debugPrint("Upload is $progressUploadMedia% complete.");
          break;
        case TaskState.paused:
          debugPrint("Upload is paused.");
          break;
        case TaskState.canceled:
          debugPrint("Upload was canceled");
          break;
        case TaskState.error:
          // Handle unsuccessful uploads
          debugPrint("Upload was error");
          break;
        case TaskState.success:
          debugPrint("Upload was successfully");
          downloadFile.then((value) {
            Map<String, dynamic> object = {"bookimage": value};
            bookController.updateBookLogo(
                id: bookdata!.id, object: object);
            //refreshBookData(widget.bookmodel.id);
            setState(() {
              logotemp = null;
            });
          });

          // Handle successful uploads on complete
          // ...
          break;
      }
    });
  }

  pickImageweb() async {
    setState(() {
      bookimage = null;
    });
    Uint8List? bytesFromPicker = await ImagePickerWeb.getImageAsBytes();

    if (bytesFromPicker != null) {
      debugPrint('pick success');

      setState(() {
        logotemp = bytesFromPicker;
      });
      //  uploadFirebaseStorage(logotemp!);
    } else {
      setState(() {
        logotemp = null;
      });
      debugPrint('Error pick image');
    }
  }

  Future<BookModel> getCurrentBook(String bookid) async {
    debugPrint('current bookid : $bookid');
    if (bookid.isEmpty || bookid == '') {
      Get.offAll(() => const MyApp(
            isloggedin: false,
          ));
    }
    return _db
        .collection('book')
        .doc(bookid)
        .get()
        .then((value) => BookModel.fromJson(value.data()!));
  }


  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return FutureBuilder(
        future: getCurrentBook(widget.bookmodel.id),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Text(
              "Something went wrong - no data found",
            );
          }
          if (snapshot.connectionState == ConnectionState.active) {
            //  print('is list ${snapshot.data}');
            //  listData = snapshot.data!;
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            //  print('is list ${snapshot.data}');
            //  listData = snapshot.data!;
            return const Center(child: CircularProgressIndicator());
          }
          bookdata = snapshot.data;
          return Column(
            children: [
              const SizedBox(height: defaultPadding),
              Text(
                "Populasi ${bookdata!.bookname}",
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: defaultPadding),
              SizedBox(
                height: 250,
                width: Get.width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        if (bookdata != null)
                          GestureDetector(
                            // behavior: HitTestBehavior.translucent,
                            onTap: () => pickImageweb(),
                            child: SizedBox(
                              child: SizedBox(
                                width: 180,
                                height: 180,
                                child: logotemp != null
                                    ? Image.memory(logotemp!)
                                    : Image(
                                        height: 200,
                                        width: 200,
                                        image: bookdata!.bookimage.isNotEmpty
                                            ? CachedNetworkImageProvider(
                                               bookdata!.bookimage)
                                            : const CachedNetworkImageProvider(
                                                "https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo_dac.png?alt=media&token=24d7fe3a-e5f0-4215-88cc-2ba9783b55e3&_gl=1*1xs4e0b*_ga*MjE0NDQ4MzM1My4xNjQxMTM2MTIz*_ga_CW55HF8NVT*MTY4NTQzMTAyOS41MC4xLjE2ODU0MzExNDUuMC4wLjA",
                                              ),
                                      ),
                              ),
                            ),
                          ),
                        const SizedBox(
                          height: 20,
                        ),
                        if (logotemp != null)
                          SizedBox(
                            // width: 200,
                            child: isuploadProcess == true
                                ? GFProgressBar(
                                    percentage: progressUploadMedia,
                                  )
                                : Row(
                                    children: [
                                      ElevatedButton(
                                        style: TextButton.styleFrom(
                                          backgroundColor: ColorName.redprimary,
                                          padding: EdgeInsets.symmetric(
                                            horizontal: defaultPadding * 1.5,
                                            vertical: defaultPadding /
                                                (Responsive.isMobile(context)
                                                    ? 2
                                                    : 1),
                                          ),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            logotemp = null;
                                          });
                                        },
                                        child: const Text("Batal"),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      ElevatedButton(
                                        style: TextButton.styleFrom(
                                          backgroundColor:
                                              ColorName.greenprimary,
                                          padding: EdgeInsets.symmetric(
                                            horizontal: defaultPadding * 1.5,
                                            vertical: defaultPadding /
                                                (Responsive.isMobile(context)
                                                    ? 2
                                                    : 1),
                                          ),
                                        ),
                                        onPressed: () => upload(),
                                        child: const Text("Ganti Logo"),
                                      )
                                    ],
                                  ),
                          ),
                      ],
                    ),
                    if (iskepsekmode == true)
                      SizedBox(
                        width: 200,
                        height: 200,
                        child: Chart(
                          bookmasterid: bookdata!.id,
                        ),
                      ),
                    if (iskepsekmode == false)
                      bookdata!.id == ''
                          ? const SizedBox.shrink()
                          : SizedBox(
                              width: 200,
                              height: 200,
                              child: Chart(
                                bookmasterid: bookdata!.id,
                              ),
                            ),
                  ],
                ),
              ),
              const SizedBox(height: defaultPadding),
              bookdata!.id == ''
                  ? const SizedBox.shrink()
                  : ElevatedButton.icon(
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                          horizontal: defaultPadding * 1.5,
                          vertical: defaultPadding /
                              (Responsive.isMobile(context) ? 2 : 1),
                        ),
                      ),
                      onPressed: () {
                        bookController.selectedOnSubBookMenus(4);
                      },
                      icon: const Icon(Icons.verified_user),
                      label: const Text("Alumni"),
                    ),
              const SizedBox(height: defaultPadding),
              Responsive(
                mobile: FileInfoCardGridView(
                  crossAxisCount: size.width < 650 ? 2 : 4,
                  childAspectRatio:
                      size.width < 650 && size.width > 350 ? 1.3 : 1,
                ),
                tablet: FileInfoCardGridView(
                  isactivedcard: isactived,
                ),
                desktop: FileInfoCardGridView(
                  isactivedcard: isactived,
                  crossAxisCount: 5,
                  childAspectRatio: size.width < 1400 ? 4 : 3.5,
                ),
              ),
              bookdata!.id == ''
                  ? SizedBox(
                      height: Get.height * 0.4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Tidak ada data buku',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.grey[600]),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                        ],
                      ),
                    )
                  : Obx(
                      () => Column(
                        children: [
                          if (bookController.isdatauseronly.value == true)
                            DashboardDatauserTable(
                              bookmasterid: bookdata!.id,
                            ),
                          // if (bookController.isdatabookonly.value == true)
                          //   DatabookDashboard(
                          //     bookmasterid: bookmasterid!,
                          //   ),
                          if (bookController.isdataforumonly.value == true)
                            DashboardDataforumTable(
                              bookmasterid: bookdata!.id,
                            ),
                          if (bookController.isdatagaleryonly.value == true)
                            DashboardDatagaleryTable(
                              bookmasterid: bookdata!.id,
                            ),
                          if (bookController.isdatastrukturonly.value == true)
                            DashboardDataorganizationTable(
                              bookmasterid: bookdata!.id,
                            ),
                          if (bookController.isdataagendaonly.value == true)
                            DashboardDataagendaTable(
                              bookmasterid: bookdata!.id,
                            ),
                          const SizedBox(
                            height: 100,
                          ),
                        ],
                      ),
                    )
            ],
          );
        });
  }
}

class FileInfoCardGridView extends StatelessWidget {
  const FileInfoCardGridView(
      {Key? key,
      this.crossAxisCount = 4,
      this.childAspectRatio = 1,
      this.isactivedcard = false})
      : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;
  final bool isactivedcard;

  @override
  Widget build(BuildContext context) {
    final BookController bookController = Get.put(BookController());
    return GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: isactivedcard == true
            ? subbookfiles.length
            : subbookInActiveCard.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          crossAxisSpacing: defaultPadding,
          mainAxisSpacing: defaultPadding,
          childAspectRatio: childAspectRatio,
        ),
        itemBuilder: (context, index) {
          return Obx(() => bookController.isactived.value == true
              ? GestureDetector(
                  onTap: () => bookController.selectedOnSubBookMenus(index),
                  child: FileInfoCard(info: subbookfiles[index]))
              : FileInfoCard(info: subbookInActiveCard[index]));
        });
  }
}

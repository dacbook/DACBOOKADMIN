import 'package:dacweb/screens/login/components/login_mobile.dart';
import 'package:dacweb/screens/login/components/login_tablet.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'components/login_web.dart';

import 'package:dacweb/controllers/menu_controller.dart' as menucontroller;

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
 @override
  Widget build(BuildContext context) {
    return Scaffold(
       key: context.read<menucontroller.MenuController>().scaffoldKey,
      body: SafeArea(
        child: LayoutBuilder(builder: (context, constraints){
          if(constraints.maxWidth < 850){
            return const Center(child: Text('the application not support on your devices, please use web browser'));
          }else if(constraints.maxWidth < 1100 && constraints.maxWidth >= 850){
           // return const LoginTablet();
             return const Center(child: Text('the application not support on your devices, please use web browser'));
          }
           else{
            return const LoginWeb();
          }
        }),
      ),
    );
  }
}
// ignore_for_file: unnecessary_overrides

import 'package:dacweb/data/local/box_storage.dart';
import 'package:dacweb/services/firebase_auth.dart';
import 'package:encrypt/encrypt.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../login_su/components/loginsu_web.dart';

class LoginController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    // as developer
     //onLoginAuto(email: 'developer@dac.com', password: '123456');
    // as kepsek
    // onLoginAuto(email: 'ami@dac.com', password: '123456');
    onsavedLogin();
  }

  @override
  void onClose() {
    super.onClose();
  }

  final BoxStorage boxStorage = BoxStorage();

  final FirebaseAuthServices firebaseAuthServices = FirebaseAuthServices();

  var txtControllerpassword = TextEditingController().obs;
  var txtControlleremail = TextEditingController().obs;
  var isPasswordVisible = true.obs;

  String? localemail;
  String? localpassword;

  Future hidepassword() async {
    isPasswordVisible.value
        ? isPasswordVisible.value = false
        : isPasswordVisible.value = true;
    debugPrint('is password ${isPasswordVisible.value}');
  }

  Future<String> onLogin(
      {required String email, required String password}) async {
    String result =
        await firebaseAuthServices.login(email: email, password: password);
    if (result == 'success') {}
    return result;
  }

  Future<String> onLoginAuto(
      {required String email, required String password}) async {
    String result =
        await firebaseAuthServices.login(email: email, password: password);

    return result;
  }

  onsavedLogin() {
    localemail =  boxStorage.getUserEmail();
    localpassword = boxStorage.getUserPassword();
    boxStorage.getUserRememberMe();
    debugPrint('email : ($localemail) - password ($localpassword)');
    if (
     localemail != '' || 
        localpassword != '') {
          debugPrint('login ..');
      onLogin(email: localemail!, password: localpassword!);
    } else{
         debugPrint('off login ..');
      txtControllerpassword.value.clear();
      txtControlleremail.value.clear();
      boxStorage.clearCache();
      
    }
  }

  onRememberLoginGetX(bool isremember, String email, String password) async{
   debugPrint('email : $email');
      txtControlleremail.value.text = email;
      txtControllerpassword.value.text = password;
      //Encrypted emailEnc =  boxStorage.encrypt(email);
      boxStorage.setUserEmail(email.toString());
      boxStorage.setUserPassword(password);
      boxStorage.setUserRememberMe();
      //debugPrint('enc : $emailEnc');
      debugPrint('getx : ${txtControlleremail.value.text} ${txtControllerpassword.value.text}');
    debugPrint('is data saved : $isremember - $email - $password');
     await onLogin(email: email, password: password);
    //Get.to(const LoginSUWeb());
  }


}

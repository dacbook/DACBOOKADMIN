import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../data/local/box_storage.dart';
import '../../../gen/assets.gen.dart';
import '../controllers/login_controller.dart';

class LoginWeb extends StatefulWidget {
  const LoginWeb({super.key});

  @override
  State<LoginWeb> createState() => _LoginWebState();
}

class _LoginWebState extends State<LoginWeb> {
  bool isChecked = false;
  final _formKey = GlobalKey<FormState>();
  final LoginController logincontroller = Get.put(LoginController());
  bool isloading = false;
  bool isobservated = true;
  bool isremember = false;
  String? result;

  bool isDAC = true;
  bool isKontakDAC = false;
  bool isOrderDAC = false;

  final BoxStorage boxStorage = BoxStorage();

  @override
  void initState() {
    super.initState();
    onload();
  }

  onload() async{
    String email = boxStorage.getUserEmail();
    debugPrint('email : $email');
    logincontroller.txtControllerpassword.value.text =
        boxStorage.getUserPassword();
    isremember = boxStorage.getUserRememberMe();
    if(email.isNotEmpty){
      logincontroller.txtControlleremail.value.text = email;
    }
    debugPrint(
        'Saving on login : ${logincontroller.txtControlleremail.value.text} - ${logincontroller.txtControllerpassword.value.text} - $isremember');

  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        // Padding(
        //   padding: const EdgeInsets.only(right: 12.0),
        //   child: SizedBox(
        //     width: 90,
        //     child: Assets.images.loginWave.image(fit: BoxFit.fitHeight),
        //   ),
        // ),
        Column(
          children: [
            Expanded(
              //<-- Expanded widget
              // flex: 9,
              child: Row(
                children: [
                  Container(
                    //  color: Colors.green,
                    width: 500, height: 600,
                    //  constraints: const BoxConstraints(maxWidth: 21),
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: AutofillGroup(
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: 200,
                              child: Assets.images.logo
                                  .image(fit: BoxFit.fitHeight),
                            ),
                            const SizedBox(height: 8),
                            TextFormField(
                              controller:
                                  logincontroller.txtControlleremail.value,
                              keyboardType: TextInputType.text,
                              autofillHints: const [AutofillHints.email],
                              textInputAction: TextInputAction.next,
                              onSaved: (String? value) {},
                              onChanged: (value) =>
                                  logincontroller.txtControlleremail.value,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "email cannot be empty";
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'cooper@example.com',
                                labelText: 'Email',
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              controller:
                                  logincontroller.txtControllerpassword.value,
                              obscureText: isobservated,
                              keyboardType: TextInputType.text,
                              autofillHints: const [AutofillHints.password],
                              onEditingComplete: () =>
                                  TextInput.finishAutofillContext(),
                              onChanged: (value) =>
                                  logincontroller.txtControllerpassword.value,
                              onSaved: (String? value) {},
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "password cannot be empty";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                labelText: 'Password',
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    isobservated
                                        ? Icons.visibility_off_sharp
                                        : Icons.visibility,
                                    color: Colors.black,
                                  ),
                                  onPressed: () async {
                                    setState(() {
                                      isobservated
                                          ? isobservated = false
                                          : isobservated = true;
                                    });
                                    debugPrint('is password $isobservated');
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 30.0, left: 25.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (isremember == false) {
                                                setState(() {
                                                  isremember = true;
                                                });
                                              } else {
                                                setState(() {
                                                  isremember = false;
                                                });
                                              }
                                            });
                                          },
                                          child: isremember == true
                                              ? const Icon(Icons.check_box)
                                              : const Icon(Icons
                                                  .check_box_outline_blank_outlined)),
                                      const Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "Ingat saya ",
                                          style: TextStyle(fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 20.0),
                                    child: GestureDetector(
                                      // onTap: () => {Get.to(const ForgotPasswordView())},
                                      child: const Text(
                                        "Lupa password ?",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            color: Colors.black),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 25),
                            isloading == false
                                ?
                                //  Padding(
                                //     padding: const EdgeInsets.all(8.0),
                                //     child: SizedBox(
                                //       width: 150,
                                //       child:
                                //        GFButton(
                                //           size: GFSize.MEDIUM,
                                //           text: 'Masuk',
                                //           textStyle: const TextStyle(
                                //               fontSize: 16,
                                //               color: GFColors.WHITE),
                                //           icon: const Icon(
                                //             Icons.login,
                                //           ),
                                //           color: Colors.red,
                                //           blockButton: true,
                                //           onPressed: () {
                                //             // Get.snackbar('Informasi',
                                //             //     'Halaman sedang dalam perbaikan',
                                //             //     backgroundColor: Colors.red,
                                //             //     colorText: Colors.white);
                                //             /*
                                //             Future.delayed(
                                //                 const Duration(seconds: 1),
                                //                 () async {
                                //               // <-- Delay here
                                //               if (_formKey.currentState!
                                //                   .validate()) {
                                //                 setState(() {
                                //                   isloading = true;
                                //                 });
                                //                 FocusManager
                                //                     .instance.primaryFocus
                                //                     ?.unfocus();
                                //                 debugPrint('login process');
                                //                 result = await logincontroller
                                //                     .onLogin(
                                //                         email: logincontroller
                                //                             .txtControlleremail
                                //                             .value
                                //                             .text,
                                //                         password: logincontroller
                                //                             .txtControllerpassword
                                //                             .value
                                //                             .text);
                                //                 debugPrint(
                                //                     'result login : $result');
                                //                 setState(() {
                                //                   isloading = false;
                                //                 });
                                //               } else {
                                //                 Get.snackbar('Login Proses',
                                //                     'login in progress');
                                //                 debugPrint(
                                //                     'login in progress..');
                                //                 setState(() {
                                //                   isloading = false;
                                //                 });
                                //               }
                                //               // if (result == 'failed') {
                                //               //   debugPrint('result logins : $result');
                                //               //   setState(() {
                                //               //     isloading == false;
                                //               //   });
                                //               // }
                                //             });
                                //             */
                                //             logincontroller.onRememberLoginGetX(
                                //                 isremember,
                                //                 logincontroller
                                //                     .txtControlleremail
                                //                     .value
                                //                     .text,
                                //                 logincontroller
                                //                     .txtControllerpassword
                                //                     .value
                                //                     .text);
                                //             debugPrint(
                                //                 'result logins isloading : $isloading - issaved : $isremember ');
                                //           }),
                                //     ),
                                //   )

                                 Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: SizedBox(
                                      width: 150,
                                      child:
                                       GFButton(
                                          size: GFSize.MEDIUM,
                                          text: 'Login',
                                          textStyle: const TextStyle(
                                              fontSize: 16,
                                              color: GFColors.WHITE),
                                          icon: isloading == false ? const Icon(
                                            Icons.login,
                                          ): const CircularProgressIndicator(),
                                          color: Colors.red,
                                          blockButton: true,
                                          onPressed: () async{
                                            if(isloading == false){
                                              setState(() {
                                                isloading = true;
                                              });
                                             await logincontroller.onRememberLoginGetX(
                                                isremember,
                                                logincontroller
                                                    .txtControlleremail
                                                    .value
                                                    .text,
                                                logincontroller
                                                    .txtControllerpassword
                                                    .value
                                                    .text);
                                                     setState(() {
                                                isloading = false;
                                              });
                                            } else{
                                               setState(() {
                                                isloading = false;
                                              });
                                            }
                                           
                                          }
                                       )
                                    ),
                                  )
                                : const Center(
                                    child: SizedBox(
                                        height: 30,
                                        width: 30,
                                        child: CircularProgressIndicator()),
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SizedBox(
                                width: 150,
                                child: GFButton(
                                    size: GFSize.MEDIUM,
                                    text: 'Apa itu DAC?',
                                    textStyle: const TextStyle(
                                        fontSize: 16, color: GFColors.WHITE),
                                    icon: const Icon(
                                      Icons.question_mark,
                                    ),
                                    color: Colors.red,
                                    blockButton: true,
                                    onPressed: () {
                                      setState(() {
                                        isDAC = true;
                                        isKontakDAC = false;
                                        isOrderDAC = false;
                                      });
                                    }),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SizedBox(
                                width: 150,
                                child: GFButton(
                                    size: GFSize.MEDIUM,
                                    text: 'Pemesanan',
                                    textStyle: const TextStyle(
                                        fontSize: 16, color: GFColors.WHITE),
                                    icon: const Icon(
                                      Icons.book_online,
                                    ),
                                    color: Colors.red,
                                    blockButton: true,
                                    onPressed: () {
                                      setState(() {
                                        isDAC = false;
                                        isKontakDAC = false;
                                        isOrderDAC = true;
                                      });
                                    }),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SizedBox(
                                width: 150,
                                child: GFButton(
                                    size: GFSize.MEDIUM,
                                    text: 'Kontak Kami',
                                    textStyle: const TextStyle(
                                        fontSize: 16, color: GFColors.WHITE),
                                    icon: const Icon(
                                      Icons.call,
                                    ),
                                    color: Colors.red,
                                    blockButton: true,
                                    onPressed: () {
                                      setState(() {
                                        isDAC = false;
                                        isKontakDAC = true;
                                        isOrderDAC = false;
                                      });
                                    }),
                              ),
                            ),
                          ],
                        ),
                        if (isDAC) const IsDAC(),
                        if (isKontakDAC) const KontakKamiDAC(),
                        if (isOrderDAC) const OrderDAC(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: SizedBox(
                height: 50,
                child: Row(
                  children: [
                    Assets.images.ilmciLogo.image(fit: BoxFit.fitHeight),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      '© 2023 PT ILMCI INDONESIA. Hak Cipta Dilindungi Undang-Undang',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class IsDAC extends StatelessWidget {
  const IsDAC({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: SizedBox(
        width: 600,
        child: Column(
          children: [
            Assets.images.dacCover1.image(fit: BoxFit.fitWidth),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'DIGITAL ALUMNI & COMUNITY (DAC) adalah sebuah platform digital yang bertujuan untuk menghubungkan alumni-alumni agar dapat berkomunikasi, berbagi cerita dan membuka peluang bisnis.',
                style: TextStyle(fontWeight: FontWeight.normal),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class OrderDAC extends StatelessWidget {
  const OrderDAC({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: Padding(
      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: SizedBox(
          width: 600,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Center(
                child: Text('Fitur sedang dalam pengembangan mohon ditunggu')),
          )),
    ));
  }
}

class KontakKamiDAC extends StatelessWidget {
  const KontakKamiDAC({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: Padding(
      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: SizedBox(
          width: 600,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Center(
                child: Text('Fitur sedang dalam pengembangan mohon ditunggu')),
          )),
    ));
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../gen/colors.gen.dart';
import '../controller/user_controller.dart';

final db = FirebaseFirestore.instance;

class CreateUser extends StatefulWidget {
  const CreateUser({super.key, required this.isupdate});
  final bool isupdate;
  @override
  State<CreateUser> createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {
  final _formKey = GlobalKey<FormState>();
  final UserController userController = Get.put(UserController());
  bool isloading = false;
  String? mRole;
  String? selectedValue;
  String? bookmasterid;
  final TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Theme.of(context).canvasColor,
        // constraints: const BoxConstraints(maxWidth: 21),
        height: 530,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Form(
          key: _formKey,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                StreamBuilder<QuerySnapshot>(
                    stream: db.collection('mRole').snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }

                      return Padding(
                        padding: const EdgeInsets.only(
                            top: 20.0, left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              'Role user',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Container(
                              padding: const EdgeInsets.all(5),
                              child: DropdownButton(
                                value: mRole,
                                isDense: true,
                                items: snapshot.data!.docs
                                    .map((DocumentSnapshot doc) {
                                  return DropdownMenuItem<String>(
                                      onTap: () {},
                                      value: doc['data']['rolename'],
                                      child: Text(doc['data']['rolename']));
                                }).toList(),
                                hint: const Text("tentukan role"),
                                onChanged: (value) {
                                  setState(() {
                                    mRole = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: TextFormField(
                    controller: userController.txtControlleremail.value,
                    keyboardType: TextInputType.text,
                    autofillHints: const [AutofillHints.email],
                    textInputAction: TextInputAction.next,
                    onSaved: (String? value) {},
                    onChanged: (value) =>
                        userController.txtControlleremail.value,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "email cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'bagus@dac.com',
                      labelText: 'Email',
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: TextFormField(
                    controller: userController.txtControllerpassword.value,
                    obscureText: userController.isPasswordVisible.value,
                    keyboardType: TextInputType.text,
                    autofillHints: const [AutofillHints.password],
                    onEditingComplete: () => TextInput.finishAutofillContext(),
                    onChanged: (value) =>
                        userController.txtControllerpassword.value,
                    onSaved: (String? value) {},
                    validator: (value) {
                      if (value == null || value.isEmpty || value.length < 5) {
                        return "password belum lengkap atau minimal 6 karakter";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: 'Password',
                      suffixIcon: IconButton(
                        icon: Icon(
                          userController.isPasswordVisible.value
                              ? Icons.visibility_off_sharp
                              : Icons.visibility,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          userController.hidepassword();
                        },
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: TextFormField(
                    controller: userController.txtControllerusername.value,
                    keyboardType: TextInputType.text,
                    onSaved: (String? value) {},
                    onChanged: (value) =>
                        userController.txtControllerusername.value,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "username cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'benydac',
                      labelText: 'Username',
                    ),
                  ),
                ),
                /*
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: TextFormField(
                    controller: userController.txtControllerpin.value,
                    keyboardType: TextInputType.text,
                    onSaved: (String? value) {},
                    onChanged: (value) => userController.txtControllerpin.value,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "pin cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'DAC123456',
                      labelText: 'PIN',
                    ),
                  ),
                ),
                */
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: TextFormField(
                    controller: userController.txtControllerphone.value,
                    keyboardType: TextInputType.number,
                    onSaved: (String? value) {},
                    onChanged: (value) =>
                        userController.txtControllerphone.value,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "phone cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: '6281192911433',
                      labelText: 'Phone',
                    ),
                  ),
                ),
                mRole == 'kepala sekolah'
                    ? Padding(
                        padding: const EdgeInsets.only(
                            top: 20.0, left: 20, right: 20),
                        child: StreamBuilder<QuerySnapshot>(
                            stream: db
                                .collection('book')
                                .where('ismasterbook', isEqualTo: true)
                                .snapshots(),
                            builder: (context, snapshot) {
                              if (!snapshot.hasData) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              }

                              return Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Text(
                                      'Kepala Sekolah Akan Terdaftar Pada Master Buku Yang Dipilih',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    DropdownButtonHideUnderline(
                                      child: DropdownButton2<String>(
                                        isExpanded: true,
                                        hint: Text(
                                          'Tentukan master buku',
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Theme.of(context).hintColor,
                                          ),
                                        ),
                                        items: snapshot.data!.docs
                                            .map((DocumentSnapshot doc) {
                                          return DropdownMenuItem<String>(
                                              onTap: () {
                                                List bookmembers =
                                                    doc['bookmember'];

                                                bookmasterid =
                                                    doc['bookmasterid'];
                                                debugPrint(
                                                    'book id : $bookmasterid');
                                              },
                                              value: doc['bookname'],
                                              child: Text(doc['bookname']));
                                        }).toList(),
                                        value: selectedValue,
                                        onChanged: (value) {
                                          setState(() {
                                            selectedValue = value as String;
                                            setState(() {
                                              //bookmasterid = value;

                                              debugPrint(
                                                  'is bookmaster $bookmasterid');
                                            }); 
                                          });
                                        },
                                        buttonStyleData: const ButtonStyleData(
                                          height: 40,
                                          width: 400,
                                        ),
                                        dropdownStyleData:
                                            const DropdownStyleData(
                                          maxHeight: 200,
                                        ),
                                        menuItemStyleData:
                                            const MenuItemStyleData(
                                          height: 40,
                                        ),
                                        dropdownSearchData: DropdownSearchData(
                                          searchController:
                                              textEditingController,
                                          searchInnerWidgetHeight: 50,
                                          searchInnerWidget: Container(
                                            height: 50,
                                            padding: const EdgeInsets.only(
                                              top: 8,
                                              bottom: 4,
                                              right: 8,
                                              left: 8,
                                            ),
                                            child: TextFormField(
                                              expands: true,
                                              maxLines: null,
                                              controller: textEditingController,
                                              decoration: InputDecoration(
                                                isDense: true,
                                                contentPadding:
                                                    const EdgeInsets.symmetric(
                                                  horizontal: 10,
                                                  vertical: 8,
                                                ),
                                                hintText:
                                                    'Search for an item...',
                                                hintStyle: const TextStyle(
                                                    fontSize: 12),
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                              ),
                                            ),
                                          ),
                                          searchMatchFn: (item, searchValue) {
                                            return (item.value
                                                .toString()
                                                .toLowerCase()
                                                .contains(searchValue));
                                          },
                                        ),
                                        //This to clear the search value when you close the menu
                                        onMenuStateChange: (isOpen) {
                                          if (!isOpen) {
                                            textEditingController.clear();
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }),
                      )
                    : const SizedBox.shrink(),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            ColorName.blueprimary),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.purplelow)))),
                    onPressed: () {
                      //controller.getAuth(ischeckedValue);
                      setState(() {
                        isloading = true;

                        userController.isLogginProcess.value = false;
                      });
                      if (userController.isLogginProcess.value == false &&
                              mRole != null ||
                          bookmasterid != null) {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            userController.txtmRole.value.text = mRole!;
                          });

                          Get.snackbar(
                              'Informasi', "Selamat berhasil ditambahkan");
                          userController.checkPin(
                              isgoogleaccount: false,
                              iskepsek: true,
                              mRole: mRole,
                              masterbookid: bookmasterid);
                          setState(() {
                            userController.isLogginProcess.value = true;
                            isloading = false;
                            mRole = null;
                            userController.txtControlleremail.value.clear();
                            userController.txtControllerpassword.value.clear();
                            userController.txtControllerusername.value.clear();
                            userController.txtControllerphone.value.clear();
                            userController.txtControllerpin.value.clear();
                            userController.txtmRole.value.clear();
                          });
                        } else {
                          Get.snackbar(
                              'Registration Failed', 'Lengkapi data kamu');
                        }
                        setState(() {
                          isloading = false;
                          userController.isLogginProcess.value = false;
                        });
                      } else {
                        if (mRole == null) {
                          Get.snackbar('Registration Invalid',
                              'Please select role access');
                        } else {
                          Get.snackbar('Registration Proses',
                              'Registration still in progress');
                        }

                        debugPrint('registration with auth in progress..');
                      }
                      setState(() {
                        isloading = false;
                        userController.isLogginProcess.value = false;
                      });
                    },
                    child: Obx(() => Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              userController.isLogginProcess.value == false ||
                                      isloading == true
                                  ? Text("Registrasi Akun DAC".toUpperCase(),
                                      style: const TextStyle(fontSize: 14))
                                  : const CircularProgressIndicator(),
                            ],
                          ),
                        )),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/screens/data_user/controller/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../models/book/book_model.dart';
import '../../../models/user/user_model.dart';
import '../../data_buku/components/source_datatable.dart';
import '../../main/controller/main_controller.dart';
import 'create_user.dart';
import 'user_source_datatable.dart';

class UserComponentWeb extends StatefulWidget {
  const UserComponentWeb({super.key});

  @override
  State<UserComponentWeb> createState() => _UserComponentWebState();
}

class _UserComponentWebState extends State<UserComponentWeb> {
  bool isfiltered = false;
  var keyword = TextEditingController();
  String searchResult = '';
  List<UserModel> listData = [];
  List<UserModel> listDatas = [];
  List<UserModel> filteredData = [];
  bool isStopped = false;
  bool isChecker = false;
  UserModel? userModel;
  final MainController mainController = Get.put(MainController());
  final searchController = TextEditingController();

  late UserDataSource dataSource;

  List<UserModel> listuserdata = [];
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    userModel = mainController.usermodel.value!;
    dataSource = UserDataSource(listData);
    setState(() {
      debugPrint('list data $listData');
      dataSource = UserDataSource(listData);
    });

    fetchAllUser();

    // filteredList = dataList;
  }

  Future<void> fetchAllUser() async {
    QuerySnapshot querySnapshot = await _db.collection('user').get();
    listuserdata = querySnapshot.docs
        .map((doc) =>
            UserModel.fromJson(doc.data() as Map<String, dynamic>))
        .toList();
    // debugPrint('$listbookdata');
    setState(() {
      listuserdata;
      //  debugPrint('user data $listuserdata');
      dataSource = UserDataSource(listuserdata);
    });
  }

   void _onSearchTextChanged(String text) {
    setState(() {
      filteredData = text.isEmpty
          ? listuserdata
          : listuserdata
              .where((item) =>
                  item.username.toLowerCase().contains(text.toLowerCase())|| item.email.toLowerCase().contains(text.toLowerCase()))
              .toList();
      dataSource = UserDataSource(filteredData);
    });
    debugPrint('filtered key $text data buku : ');
  }

  @override
  Widget build(BuildContext context) {
    final UserController userController = Get.put(UserController());
    //   List dataList = [];
    return StreamBuilder(
        //stream: userController.getUserList(),
        builder: (context, snapshot) {
      if (snapshot.hasError) {
        return const Text(
          "Something went wrong - no data found",
        );
      }
      if (snapshot.connectionState == ConnectionState.active) {
        //  print('is list ${snapshot.data}');
        //  listData = snapshot.data!;
        return const Center(child: CircularProgressIndicator());
      }
      if (snapshot.connectionState == ConnectionState.waiting) {
        //  print('is list ${snapshot.data}');
        //  listData = snapshot.data!;
        return const Center(child: CircularProgressIndicator());
      }

      return listuserdata == []
          ? const CircularProgressIndicator()
          : Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Obx(
            () => userController.iscreated.value == true
                ? const Padding(
                    padding: EdgeInsets.all(12.0),
                    child: CreateUser(isupdate: true),
                  )
                : const SizedBox.shrink(),
          ),
          Text(
            "Data Alumni",
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                 SizedBox(
                          width: 300,
                          child: TextField(
                            controller: searchController,
                            onChanged: (value) => _onSearchTextChanged(value),
                            decoration: InputDecoration(
                              hintText: "Cari",
                              fillColor: Theme.of(context).canvasColor,
                              filled: true,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              suffixIcon: InkWell(
                                onTap: () {
                                  if (isfiltered == true) {
                                    setState(() {
                                      isfiltered = false;
                                      keyword.clear();
                                      searchResult = '';
                                    });
                                  } else {
                                    setState(() {
                                      isfiltered = true;
                                    });
                                  }
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(
                                      defaultPadding * 0.75),
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: defaultPadding / 2),
                                  decoration: BoxDecoration(
                                    color: isfiltered == false
                                        ? primaryColor
                                        : Colors.red,
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: isfiltered == false
                                      ? SvgPicture.asset(
                                          "assets/icons/Search.svg")
                                      : const Icon(Icons.clear),
                                ),
                              ),
                            ),
                          )),
                Row(
                  children: [
                    userModel!.userRole != 'user' &&
                            userModel!.userRole != 'kepala dinas'
                        ? TextButton(
                            onPressed: () {
                              setState(() {
                                if (userController.iscreated.value == false) {
                                  userController.iscreated.value = true;
                                } else {
                                  userController.iscreated.value = false;
                                }
                              });
                            },
                            child: const Text('Tambah User'),
                          )
                        : const SizedBox.shrink(),
                    // TextButton(
                    //   onPressed: () {
                    //     Get.to(const BookImportScreen());
                    //   },
                    //   child: const Text(
                    //     'Import Buku',
                    //     style: TextStyle(color: Colors.red),
                    //   ),
                    // ),
                    // TextButton(
                    //   onPressed: () {
                    //     setState(() {
                    //       if (bookController.iscreated.value == false) {
                    //         bookController.iscreated.value = true;
                    //       } else {
                    //         bookController.iscreated.value = false;
                    //       }
                    //     });
                    //   },
                    //   child: const Text('Tambah Master Buku'),
                    // ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          // Obx(
          //   () =>  bookController.iscreated.value == true
          //     ? const Padding(
          //         padding: EdgeInsets.all(12.0),
          //         child: UpdateBook(isupdate : true),
          //       )
          //     : const SizedBox.shrink(),),

          SizedBox(
            width: double.infinity,
            child: PaginatedDataTable(
              source: dataSource,
              columnSpacing: 10,
              horizontalMargin: 10,
              rowsPerPage: 10,
              showCheckboxColumn: false,
              //minWidth: 700,
              columns: const [
                DataColumn(
                  label: SizedBox(
                      width: 100,
                      child: Text(
                        "Nama",
                        overflow: TextOverflow.ellipsis,
                      )),
                ),
                DataColumn(
                  label: SizedBox(
                      width: 100,
                      child: Text(
                        "Role",
                        overflow: TextOverflow.ellipsis,
                      )),
                ),
                DataColumn(
                  label: SizedBox(
                      width: 100,
                      child: Text("Email", overflow: TextOverflow.ellipsis)),
                ),
                DataColumn(
                  label: SizedBox(
                      width: 100,
                      child: Text("Bio", overflow: TextOverflow.ellipsis)),
                ),
                DataColumn(
                  label: SizedBox(
                      width: 100,
                      child: Text("Aksi", overflow: TextOverflow.ellipsis)),
                ),
                // DataColumn(
                //   label: SizedBox(
                //       width: 100,
                //       child: Text("Catatan buku",
                //           overflow: TextOverflow.ellipsis)),
                // ),
                // DataColumn(
                //   label: SizedBox(
                //       width: 100,
                //       child:
                //           Text("Aksi", overflow: TextOverflow.ellipsis)),
                // ),
              ],
            ),
          )
        ],
      );
    });
  }
}

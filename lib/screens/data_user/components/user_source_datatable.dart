import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/user/user_model.dart';
import '../../profile_user/components/profile_user_web.dart';
import '../controller/user_controller.dart';

final UserController userController = Get.put(UserController());

class UserDataSource extends DataTableSource {
  List<UserModel> dataList;
  UserDataSource(this.dataList) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.username,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      
        DataCell( Text(
                data.userRole,
                style:
                  const  TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              )
           ),
        DataCell(Text(data.email.toString())),
        DataCell(Text(data.bio.toString())),
        DataCell(TextButton(
            onPressed: () {
              Get.to( ProfileEditView(usermodel: data,));
             
            },
            child: const Text('Lihat Profil'),
            //...
          )),
        // DataCell(SizedBox(
        //   width: 100,
        //   child:
        //       Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        //     IconButton(
        //         icon: const Icon(
        //           Icons.edit,
        //           color: Colors.blue,
        //         ),
        //         tooltip: 'Edit Book',
        //         onPressed: () {
        //           // bookController.updateData(
        //           //     data.bookname, data.pin, data.description, data.id);
        //         }),
        //     TextButton(
        //       onPressed: () => bookController.deletebook(data.id),
        //       child: const Text('Hapus'),
        //       //...
        //     ),
        //   ]),
        // )),
      ],
    );
  }
}

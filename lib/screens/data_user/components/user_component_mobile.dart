import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../services/firestore_services.dart';

class UserComponentMobile extends StatelessWidget {
  const UserComponentMobile({super.key});

  @override
  Widget build(BuildContext context) {
    List dataList = [];
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FutureBuilder(
            future: FireStoreDataBase().getAllBookData(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return const Text(
                  "Something went wrong",
                );
              }
              if (snapshot.connectionState == ConnectionState.done) {
                dataList = snapshot.data as List;
                return Column(
                  children: [
                    Text(
                      "Data Buku Alumni",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    SizedBox(
                      width: double.infinity,
                      
                      child:  SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            DataTable(
                              columnSpacing: 10,
                              //minWidth: 700,
                              columns: const [
                                DataColumn(
                                  label: SizedBox(
                                    width: 100,
                                    child: Text("Nama Buku", overflow: TextOverflow.ellipsis,)),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                     width: 100,
                                    child: Text("Status Buku", overflow: TextOverflow.ellipsis)),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                     width: 100,
                                    child: Text("Total alumni", overflow: TextOverflow.ellipsis)),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                     width: 100,
                                    child: Text("Catatan buku", overflow: TextOverflow.ellipsis)),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                     width: 100,
                                    child: Text("Aksi", overflow: TextOverflow.ellipsis)),
                                ),
                              ],
                              rows: List.generate(
                                dataList.length,
                                (index) => recentFileDataRow(dataList[index]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              }
              return const Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
    );
  }
}

DataRow recentFileDataRow(dataList) {
  List countbookmember = dataList["bookmember"] as List;
  return DataRow(
    cells: [
      DataCell(
        Text(dataList["bookname"], overflow: TextOverflow.ellipsis,),
      ),
      DataCell(dataList["ismasterbook"] == true ? const Text('Master Buku', style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),): const Text('Sub Buku', style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),)),
      DataCell(Text(countbookmember.length.toString())),
      DataCell(Text(dataList["description"].toString())),
       DataCell(SizedBox(
        width: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [Icon(Icons.edit, color: Colors.red,), Icon(Icons.delete, color: Colors.blue,)],
        ),
      )),
    ],
  );
}

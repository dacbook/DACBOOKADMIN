import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/models/user/user_model.dart';
import 'package:dacweb/screens/data_user/components/user_component_web.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../data_buku/components/source_datatable.dart';
import 'components/user_component_mobile.dart';
import 'components/user_source_datatable.dart';

class UserDataScreen extends StatefulWidget {
  const UserDataScreen({super.key});

  @override
  State<UserDataScreen> createState() => _UserDataScreenState();
}

class _UserDataScreenState extends State<UserDataScreen> {


  @override
  void initState() {

   
    super.initState();

    // filteredList = dataList;
  }

 

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
            padding: const EdgeInsets.all(defaultPadding),
            child: Column(
              children: [
                // const Header(),
                const SizedBox(height: defaultPadding),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          if (Responsive.isDesktop(context))
                           const  UserComponentWeb(),
                          if (Responsive.isMobile(context))
                            const UserComponentMobile(),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )));
  }
}

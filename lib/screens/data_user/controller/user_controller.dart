import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../../models/user/user_model.dart';
import '../../../services/firestore_services.dart';

class UserController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  final FirebaseFirestore _db = FirebaseFirestore.instance;

  String collectionName = 'user';

  var txtControllerusername = TextEditingController().obs;
  var txtControllerpin = TextEditingController().obs;
  var txtControllerpassword = TextEditingController().obs;
  var txtControlleremail = TextEditingController().obs;
  var txtControllerphone = TextEditingController().obs;
  var txtmRole = TextEditingController().obs;

  late Map<String, dynamic> data;
  late Map<String, dynamic> dataIlmci;

  var isLoggedinwithgoogleaccount = false.obs;
  var isLoggedin = false.obs;
  var isLogginProcess = false.obs;
  var isPasswordVisible = true.obs;
  var isStatusCodeTrue = false.obs;
  var isUserPinned = false.obs;

  var iscreated = false.obs;
  var isupdated = false.obs;
  var isfiltered = false.obs;
  var ismasterbook = false.obs;

  var uid = ''.obs;
  var fcmtoken = ''.obs;
  var userPin = ''.obs;
  var username = ''.obs;
  var password = ''.obs;
  var photoUrl = ''.obs;
  var email = ''.obs;
  var package = ''.obs;
  var userStatus = ''.obs;
  var userRole = ''.obs;
  var whatssappNumber = ''.obs;
  var telegramNumber = ''.obs;

  Map<String, dynamic> getUserDataIlmcifromMap() {
    package.value = "com.ilmci.digitalbook";
    userRole.value = txtmRole.value.text;
    //userRole.value = "user";
    //userStatus.value = "active";
    // if (userPin.value == '' || password.value == '') {
    //userPin.value = txtControllerpin.value.text;
    //password.value = txtControllerpassword.value.text;
    // }

    dataIlmci = {
      "email": txtControlleremail.value.text,
      "password": txtControllerpassword.value.text,
      "nama": txtControllerusername.value.text,
      "phone": txtControllerphone.value.text,
      "whatsapp": "089999999999",
      "pin": txtControllerpin.value.text,
      "package": package.value
    };
    return dataIlmci;
  }

  hidepassword() {
    isPasswordVisible.value
        ? isPasswordVisible.value = false
        : isPasswordVisible.value = true;
  }

  Stream<List<UserModel>> getUserList() {
    var datasnap =
        FireStoreDataBase().streamData(collectionName: collectionName);
    return datasnap.snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => UserModel.fromJson(doc.data()),
              )
              .toList(),
        );
  }

  Future<void> checkPin(
      {required bool isgoogleaccount,
      required bool iskepsek,
      String? mRole,
      String? masterbookid}) async {
    //  registrasiDacUser(
    //           email: txtControlleremail.value.text,
    //           password: txtControllerpassword.value.text,
    //           phone: txtControllerphone.value.text,
    //           username: txtControllerusername.value.text,
    //           pin: txtControllerpin.value.text);
    try {
      dataIlmci = getUserDataIlmcifromMap();
      debugPrint('data : ${dataIlmci.toString()}');
      String imagecloud =
          "https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo.png?alt=media&token=2375fed1-8208-44f0-84e6-86b8ea8a0525";
      String userstatus = "active";
      String userrole = 'user';
      if (mRole == null) {
        userrole;
      } else {
        userrole = mRole;
      }
      debugPrint('user role : $userrole');
      registrasiDacUser(
          email: txtControlleremail.value.text,
          password: txtControllerpassword.value.text,
          phone: txtControllerphone.value.text,
          username: txtControllerusername.value.text,
          iskepsek: iskepsek,
          bookmasterid: masterbookid,
          // pin: txtControllerpin.value.text,
          userRole: userrole,
          userStatus: userstatus,
          userCoverProfile: imagecloud,
          avatar: imagecloud,
          isgoogleaccount: isgoogleaccount);

      debugPrint('clog => register dac user done');
      isLoggedinwithgoogleaccount.value = false;
      //isRemember.value == true;
      isStatusCodeTrue.value = true;
      isUserPinned.value = true;

      //const fullUrl = "https://admin.digital.ilmci.com/api/auth/register";
      /*
      const fullUrl = "https://admin.digital.ilmci.com/api/auth/v2/register";
      var bodyRequestParam = jsonEncode(dataIlmci);
      final response = await http.Client().post(Uri.parse(fullUrl),
          headers: {
            "Content-Type": "application/json",
          },
          body: bodyRequestParam);
      final result = jsonDecode(response.body);
      debugPrint('clog => ${response.statusCode.toString()}');
      debugPrint('clog => ${response.body.toString()}');
      switch (response.statusCode) {
        case 200:
          
          String imagecloud =
              "https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo.png?alt=media&token=2375fed1-8208-44f0-84e6-86b8ea8a0525";
          String userstatus = "active";
          String userrole = "user";
          debugPrint('clog => register dac user');
          registrasiDacUser(
              email: txtControlleremail.value.text,
              password: txtControllerpassword.value.text,
              phone: txtControllerphone.value.text,
              username: txtControllerusername.value.text,
              pin: txtControllerpin.value.text,
              userRole: userrole,
              userStatus: userstatus,
              userCoverProfile: imagecloud,
              avatar: imagecloud,
              isgoogleaccount: isgoogleaccount);
         
          debugPrint('clog => register dac user done');
          isLoggedinwithgoogleaccount.value = false;
          //isRemember.value == true;
          isStatusCodeTrue.value = true;
          isUserPinned.value = true;

          debugPrint(response.body.toString());
          break;
        case 400:
          debugPrint(response.body.toString());

          if (result['message'] == 'Email already registered.') {
            isStatusCodeTrue.value = false;
            isUserPinned.value = false;
            isLogginProcess.value == false;
            Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
                'Email already registered.');
            debugPrint(
                'is pinned : ${isUserPinned.value} - is status code : ${isUserPinned.value}');
          }
          if (result['message'] == 'PIN tidak terdaftar') {
            isStatusCodeTrue.value = false;
            isUserPinned.value = false;
            isLogginProcess.value == false;
            Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
                'PIN tidak terdaftar.');
          }
          if (result['message'] ==
              '[500] Connection could not be established with host mail.ilmci.com [ #0]') {
            isStatusCodeTrue.value = false;
            isUserPinned.value = false;
            isLogginProcess.value == false;
            Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
                '[500] Connection could not be established with host mail.ilmci.com [ #0]');
          }
          //Email or Password is required.
          Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
              result['message']);

          break;
        case 500:
          Get.snackbar(
              'Login Failed [${response.statusCode}]', 'Data not found');
          isStatusCodeTrue.value = false;
          isUserPinned.value = false;
          isLogginProcess.value == false;
          break;
      }
          */
    } on SocketException {
      isLoggedin.value = false;
      isLogginProcess.value == false;
      Get.snackbar('Login Failed', 'Data not found');
    }
    http.Client().close();
  }

  registrasiDacUser(
      {required String email,
      required String password,
      required String username,
      required String avatar,
      required bool iskepsek,
      //required String pin,
      String? phone,
      String? bookmasterid,
      required String userRole,
      required String userStatus,
      required String userCoverProfile,
      bool? isgoogleaccount}) async {
    debugPrint('is google account status $isgoogleaccount');

    String? result = await registration(
        email: email,
        password: password,
        username: username,
        avatar: avatar,
        iskepsek: iskepsek,
        bookmasterid: bookmasterid,
        // pin: pin,
        phone: phone,
        userRole: userRole,
        userCoverProfile: avatar,
        fcmtoken: '',
        userStatus: userStatus,
        isgoogleaccount: isgoogleaccount);
    debugPrint('result registrasi : $result');
    if (result!.contains('Success')) {
      Future.delayed(const Duration(seconds: 2), () async {
        isLogginProcess.value = false;
        debugPrint('data $email - $username - $avatar');
      });
    } else {
      isLogginProcess.value = false;
    }
    Get.snackbar('Informasi Firebase', result);
    isLogginProcess.value = false;
  }

  Future<String?> registration(
      {required String email,
      required String password,
      required bool iskepsek,
      String? bookmasterid,
      String? username,
      String? avatar,
      // String? pin,
      String? phone,
      String? userRole,
      String? userStatus,
      String? userCoverProfile,
      String? fcmtoken,
      bool? isgoogleaccount}) async {
    try {
      debugPrint('is google account = $isgoogleaccount');
      debugPrint('clog => $email - $password');
      final time = DateFormat('hh:mm');
      final date = DateFormat('dd-MM-yyyy');

      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
        email: email,
        password: password,
      )
          .then((value) {
        Map<String, dynamic> data = {
          'uid': value.user!.uid,
          'email': email,
          'password': password,
          'username': username,
          'avatar': avatar,
          //'pin': pin,
          'phone': phone,
          'userRole': userRole,
          'userStatus': userStatus,
          'userCoverProfile': userCoverProfile,
          'fcmtoken': fcmtoken,
          'followers': [value.user!.uid],
          'bookmasterkepsek': bookmasterid
        };
        _db.collection('user').doc(value.user!.uid).set(data);
        _db.collection('followers').doc(value.user!.uid).collection('follow');
        if (iskepsek == true) {
          _db.collection('book').doc(bookmasterid).update({
            'bookmember': FieldValue.arrayUnion([value.user!.uid]),
            //'bookadmin': FieldValue.arrayUnion([value.user!.uid]),
            'totalmember': FieldValue.increment(1)
          });
          _db
              .collection('book')
              .doc(bookmasterid)
              .collection('bookmember')
              .doc(value.user!.uid)
              .set({
            'bookmember': value.user!.uid,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
            'username': username,
            'avatar': avatar,
            'uid': value.user!.uid,
            'memberlevel': 'kepala sekolah',
          });
        }
      });
      Get.snackbar('Register Success', 'Anda telah berhasil register');
      return 'Success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'The account already exists for that email.';
      } else if (e.code == 'invalid-email.') {
        return 'The email address is badly formatted';
      } else {
        return e.code;
      }
    } catch (e) {
      return e.toString();
    }
  }
}

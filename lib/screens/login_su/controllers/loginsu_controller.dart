// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/data/local/box_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../../../data/local/box_storage_su.dart';
import '../../../models/book/book_model.dart';
import '../../../models/user/user_model.dart';

class LoginSUController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    // as developer
    //onLoginAuto(email: 'developer@dac.com', password: '123456');
    // as kepsek
    // onLoginAuto(email: 'ami@dac.com', password: '123456');
    //onbindingData();
  }

  @override
  void onClose() {
    super.onClose();
  }

  final BoxStorage boxStorage = BoxStorage();
  final BoxStorageSU boxStoragesu = BoxStorageSU();

  final FirebaseFirestore _db = FirebaseFirestore.instance;

  List<BookModel> listbookdata = [];
  List<UserModel> listuserdata = [];

  List<BookModel> bookdataSU = <BookModel>[].obs;
  var userdataSU = <UserModel>[].obs;

  String? email;
  String? password;

  onLoadSu() {
    email = boxStorage.getUserEmail();
    password = boxStorage.getUserPassword();
  }

  Future<List<BookModel>> fetchAllBook() async {
    debugPrint('trying fecth data');
    await _db.collection('book').get().then(
      (value) {
        listbookdata =
            value.docs.map((doc) => BookModel.fromJson(doc.data())).toList();
      },
      onError: (e) => debugPrint("Error completing: $e"),
    );
    debugPrint('trying fecth data is completed');
    return listbookdata;
  }

  Future<List<UserModel>> fetchAllUser() async {
    debugPrint('trying fecth data');
    await _db.collection('user').get().then(
      (value) {
        listuserdata =
            value.docs.map((doc) => UserModel.fromJson(doc.data())).toList();
      },
      onError: (e) => debugPrint("Error completing: $e"),
    );
    debugPrint('trying fecth data is completed');
    return listuserdata;
  }

  onbindingBookData() async {
    print('user data is not empty : ${bookdataSU.isNotEmpty}');
    await onretriveDataBook();
    if (bookdataSU.isNotEmpty) {
      debugPrint('retrive book ${bookdataSU.length}');
    } else {
      debugPrint('receive book $bookdataSU');
      await onreceiverDataBook();
    }
  }

  onbindingUserData() async {
    await onretriveDataUser();
    print('user data is not empty : ${userdataSU.isNotEmpty}');
    if (userdataSU.isNotEmpty) {
      debugPrint('retrive user ${userdataSU.length}');
    } else {
      await onreceiverDataUser();
      debugPrint('receive user ${userdataSU.length}');
    }
    print(
        'populated Book : ${bookdataSU.length} - User : ${userdataSU.length}');
  }

  onreceiverDataUser() async {
    listuserdata = await fetchAllUser();
    debugPrint('trying bundling data');
    var data = listuserdata.map((map) => map.toJson()).toList();
    var datajson = jsonEncode(data);
    // debugPrint('trying saving to box $data');
    boxStoragesu.setUserData(datajson);
    debugPrint('trying saving to box is completed');
    await onretriveDataUser();
  }

  onreceiverDataBook() async {
    listbookdata = await fetchAllBook();
    debugPrint('trying bundling data');
    var data = listbookdata.map((bookdata) => bookdata.toJson()).toList();
    var datajson = jsonEncode(data);
    debugPrint('trying saving to box $data');
    boxStoragesu.setBookData(datajson);
    debugPrint('trying saving to box is completed');
    await onretriveDataBook();
  }

  onretriveDataBook() async {
    debugPrint('trying get bundling data');
    var data = boxStoragesu.getBookData();
    if (data != '') {
      var dataJson = jsonDecode(data);
      List datalist = List.from(dataJson);
      debugPrint('trying get bundling data from map');
      bookdataSU =
          datalist.map((databook) => BookModel.fromJson(databook)).toList().obs;
      debugPrint('trying get bundling data from map is success');
      print(bookdataSU.length);
    }
  }


  onretriveDataUser() async {
    debugPrint('trying get bundling data');
    var data = boxStoragesu.getUserData();
    if (data != '') {
      //print('book data : $data');
      var dataJson = jsonDecode(data);
      List datalist = List.from(dataJson);
      debugPrint('trying get bundling data from map');
      userdataSU = datalist.map((map) => UserModel.fromJson(map)).toList().obs;
      debugPrint('trying get bundling data from map is success');
      print(bookdataSU.length);
    }
  }

  onAddUpdateBook(BookModel listbook)async{
    listbookdata.add(listbook);
    debugPrint('savings book : ${listbookdata.length}');
    var data = listbookdata.map((bookdata) => bookdata.toJson()).toList();
    var datajson = jsonEncode(data);
    debugPrint('trying saving to box $data');
    boxStoragesu.setBookData(datajson);
    debugPrint('trying saving to box is completed');
    await onretriveDataBook();
  }

  onUpdateLocalBook(BookModel databook)async{
    listbookdata[listbookdata.indexWhere((element) => element.id == databook.id)] =
          databook;
      debugPrint('get data $listbookdata');
      var data = listbookdata.map((bookdata) => bookdata.toJson()).toList();
    var datajson = jsonEncode(data);
    debugPrint('trying saving to box $data');
    boxStoragesu.setBookData(datajson);
    debugPrint('trying saving to box is completed');
    await onretriveDataBook();
  }

  Future<List<BookModel>> sugetBooklist() async{
      debugPrint('su get list book');
    onretriveDataBook();
    listbookdata = bookdataSU;
    return listbookdata;
  }

  
}

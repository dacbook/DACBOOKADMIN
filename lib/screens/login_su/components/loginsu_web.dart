// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../gen/assets.gen.dart';
import '../../../main.dart';
import '../../../models/user/user_model.dart';
import '../controllers/loginsu_controller.dart';

class LoginSUWeb extends StatefulWidget {
  const LoginSUWeb({super.key});

  @override
  State<LoginSUWeb> createState() => _LoginSUWebState();
}

class _LoginSUWebState extends State<LoginSUWeb> {
  final LoginSUController loginsucontroller = Get.put(LoginSUController());
  String? message;

  bool isbookReceived = false;
  bool isuserReceived = false;

  String? bookReceivedLabel;
  String? userReceivedLabel;

  double processReceived = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    message = '...';
    bookReceivedLabel = 'on progress';
    userReceivedLabel = 'on progress';
    loadData();
  }

  loadData() async {
    await Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        processReceived = 0.0;
        message = 'bundling user data';
      });
    });
    await Future.delayed(const Duration(seconds: 1), () {
      loginsucontroller.onbindingUserData();
      setState(() {
         userReceivedLabel = 'completed';
        processReceived = 0.4;
        message = 'bundling user data is completed';
      });
    });
    await Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        processReceived = 0.5;
        message = 'bundling book data';
      });
    });
    await Future.delayed(const Duration(seconds: 1), () {
      loginsucontroller.onbindingBookData();
      setState(() {
        bookReceivedLabel = 'completed';
        processReceived = 0.8;
        message = 'bundling book data is completed';
      });
    });
    await Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        isbookReceived = true;
        processReceived = 1.0;
        message = 'all data is completed';
      });
    });
     Get.offAll(() => const MyApp(isloggedin: true,));
    print(
        'Book : ${loginsucontroller.bookdataSU.length} - User : ${loginsucontroller.userdataSU.length}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: Get.width,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox.shrink(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 200,
                    child: Assets.images.logo.image(fit: BoxFit.fitHeight),
                  ),

                  GFProgressBar(
                      percentage: processReceived,
                      backgroundColor: Colors.black26,
                      progressBarColor: GFColors.DANGER),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('$message..', style: const TextStyle(color: Colors.blueAccent),),
                  ),
                  // if (isbookReceived)
                   SizedBox(
                    width: 450,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children:  [
                      const  Icon(
                          Icons.verified_user_outlined,
                          color: Colors.redAccent,
                        ),
                        Text('user data has been downloaded on your local ($userReceivedLabel)', style: const TextStyle(fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 450,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children:  [
                      const  Icon(
                          Icons.book,
                          color: Colors.redAccent,
                        ),
                        Text('book data has been downloaded on your local ($bookReceivedLabel)', style: const TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                 
                ],
              ),
               Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: isbookReceived == true ? const SizedBox.shrink() : const GFLoader(
                    loaderColorOne: Colors.redAccent,
                    loaderColorTwo: Colors.yellowAccent,
                    loaderColorThree: Colors.blueAccent,
                    type: GFLoaderType.circle),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

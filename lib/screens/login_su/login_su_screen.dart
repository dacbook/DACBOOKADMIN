
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:dacweb/controllers/menu_controller.dart' as menucontroller;

import 'components/loginsu_web.dart';


class LoginSUScreen extends StatefulWidget {
  const LoginSUScreen({super.key});

  @override
  State<LoginSUScreen> createState() => _LoginSUScreenState();
}

class _LoginSUScreenState extends State<LoginSUScreen> {
 @override
  Widget build(BuildContext context) {
    return Scaffold(
     //  key: context.read<menucontroller.MenuController>().scaffoldKey,
      body: SafeArea(
        child: LayoutBuilder(builder: (context, constraints){
          if(constraints.maxWidth < 850){
            return const Center(child: Text('the application not support on your devices, please use web browser'));
          }else if(constraints.maxWidth < 1100 && constraints.maxWidth >= 850){
           // return const LoginTablet();
             return const Center(child: Text('the application not support on your devices, please use web browser'));
          }
           else{
            return const LoginSUWeb();
          }
        }),
      ),
    );
  }
}


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

import '../../../data/local/box_storage.dart';
import '../../../models/user/user_model.dart';
import '../../../services/firestore_services.dart';

class ProfileController extends GetxController {
  var isLoading = true.obs;
  var isError = false.obs;
  var errmsg = "".obs;

  var isAvatarSelected = "".obs;

  final _boxStorage = BoxStorage();

  var statusVerifikasi = false.obs;

  // EmailAuth emailAuth = EmailAuth(sessionName: "Test Verifikasi E-Beasiswa");
  // var suksesSend = false.obs;
  // final emailController = TextEditingController();
  String collectionUser = 'user';

  @override
  void onInit() {
    super.onInit();
    //storageVerifikasi();
    // onFetchProfile();
  }

  @override
  void onClose() {
    super.onClose();
    //isAvatarSelected.close();
  }

  updateUserData({
    required String uid,
    required UserModel usermodel,
    String? username,
    String? noktp,
    String? address,
    String? biodata,
    String? nohandphone,
    String? noline,
    String? nowhatsapp,
    String? nosignal,
    String? nokakaotalk,
    String? tgllahir,
    String? nonisn,
    String? noijazah,
    String? nowechat,
    String? karir,
    String? jabatan,
    String? status,
    String? pendidikan,
    String? penghargaan,
    bool? ishideKtp,
  }) async {
    String uusername = username != '' ? username! : usermodel.username;

    String unoktp = noktp != '' ? noktp! : usermodel.noKtp;
    String uaddress = address != '' ? address! : usermodel.address;
    String ubiodata = biodata != '' ? biodata! : usermodel.bio;
    String unohandphone = nohandphone != '' ? nohandphone! : usermodel.noHp;
    String unoline = noline != '' ? noline! : usermodel.noLine;
    String unowhatsapp = nowhatsapp != '' ? nowhatsapp! : usermodel.noWhatsapp;
    String unosignal = nosignal != '' ? nosignal! : usermodel.noSignal;
    String unokakaotalk =
        nokakaotalk != '' ? nokakaotalk! : usermodel.noKakaoTalk;
    bool uishidektp = ishideKtp!;

    String utglahir = tgllahir != '' ? tgllahir! : usermodel.birthday;
    String unonisn = nonisn != '' ? nonisn! : usermodel.nonisn;
    String unoijazah = noijazah != '' ? noijazah! : usermodel.noijazah;
    String unowechat = nowechat != '' ? nowechat! : usermodel.noWechat;
    String ukarir = karir != '' ? karir! : usermodel.karir;
    String ujabatan = jabatan != '' ? jabatan! : usermodel.jabatan;
    String ustatus = status != '' ? status! : usermodel.statusMariage;
    String upendidikan = pendidikan != '' ? pendidikan! : usermodel.pendidikan;
    String upenghargaan =
        penghargaan != '' ? penghargaan! : usermodel.penghargaan;

    Map<String, Object> userData = {
      'username': uusername,
      'noKtp': unoktp,
      'address': uaddress,
      'bio': ubiodata,
      'noHp': unohandphone,
      'noLine': unoline,
      'noWhatsapp': unowhatsapp,
      'noSignal': unosignal,
      'noKakaoTalk': unokakaotalk,
      'ishideNoKtp': uishidektp,
      'penghargaan': upenghargaan,
      'pendidikan': upendidikan,
      'birthday': utglahir,
      'statusMariage': ustatus,
      'jabatan': ujabatan,
      'karir': ukarir,
      'noijazah': unoijazah,
      'nonisn': unonisn,
      'nowechat': unowechat,
    };

    await updateUser(data: userData, uid: usermodel.uid);
    var collectionstory = FirebaseFirestore.instance
        .collection('story')
        .where('author', isEqualTo: usermodel.uid);
    var collectionfollowerdata = FirebaseFirestore.instance
        .collectionGroup('followersdata')
            .where('uid', isEqualTo: usermodel.uid)
        .where('username', isEqualTo: usermodel.username);
    var collectionbookmember = FirebaseFirestore.instance
        .collectionGroup('bookmember')
        .where('uid', isEqualTo: usermodel.uid)
        .where('username', isEqualTo: usermodel.username);
    var qsStory = await collectionstory.get();
    for (var doc in qsStory.docs) {
      await doc.reference.update({
        'username': uusername,
      });
    }
    var qsBookmember = await collectionbookmember.get();
    for (var doc in qsBookmember.docs) {
      await doc.reference.update({
        'username': uusername,
      });
    }
    var qsFollowerData = await collectionfollowerdata.get();
    for (var doc in qsFollowerData.docs) {
      await doc.reference.update({
        'username': uusername,
      });
    }
    _boxStorage.deleteUserAvatarSelectedCache();
  }

   Future<String?> updateUser(
      {required String uid, required Map<String, Object> data}) async {
        final db = FirebaseFirestore.instance;
    try {
      await db.collection('user').doc(uid).update(data);
      return 'Success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Wrong password provided for that user.';
      } else {
        return e.message;
      }
    } catch (e) {
      return e.toString();
    }
  }
}

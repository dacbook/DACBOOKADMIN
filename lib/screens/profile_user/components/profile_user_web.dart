import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../data/local/box_storage.dart';
import '../../../gen/assets.gen.dart';
import '../../../gen/colors.gen.dart';
import '../../../models/user/user_model.dart';
import '../controller/profile_controller.dart';

class ProfileEditView extends StatefulWidget {
  const ProfileEditView({super.key, required this.usermodel});
  final UserModel usermodel;
  @override
  State<ProfileEditView> createState() => _ProfileEditViewState();
}

class _ProfileEditViewState extends State<ProfileEditView> {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _noktp = TextEditingController();
  final TextEditingController _address = TextEditingController();
  final TextEditingController _statusMariage = TextEditingController();
  final TextEditingController _biodata = TextEditingController();
  final TextEditingController _nohandphone = TextEditingController();
  final TextEditingController _nowhatsapp = TextEditingController();
  final TextEditingController _noline = TextEditingController();
  final TextEditingController _nokakaotalk = TextEditingController();
  final TextEditingController _nosignal = TextEditingController();

  final TextEditingController _tgllahir = TextEditingController();
  final TextEditingController _nonisn = TextEditingController();
  final TextEditingController _noijazah = TextEditingController();
  final TextEditingController _nowechat = TextEditingController();
  final TextEditingController _karir = TextEditingController();
  final TextEditingController _jabatan = TextEditingController();
  final TextEditingController _status = TextEditingController();
  final TextEditingController _pendidikan = TextEditingController();
  final TextEditingController _penghargaan = TextEditingController();

  final _boxStorage = BoxStorage();

  bool isloading = false;

  bool isHideKTP = false;

  String isAvatar = '';

  @override
  void initState() {
    super.initState();
    isHideKTP = widget.usermodel.ishideNoKtp;
    debugPrint('is hide KTP : $isAvatar');
  }

  final ProfileController profileController = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: ColorName.redprimary,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(Icons.close)),
            const Text('Profile'),
            GestureDetector(
                onTap: () async {
                  if (isloading == false) {
                    setState(() {
                      isloading = true;
                    });

                    debugPrint(isloading.toString());
                    profileController.updateUserData(
                        uid: widget.usermodel.uid,
                        usermodel: widget.usermodel,
                        username: _username.text,
                        noktp: _noktp.text,
                        address: _address.text,
                        biodata: _biodata.text,
                        nohandphone: _nohandphone.text,
                        noline: _noline.text,
                        nowhatsapp: _nowhatsapp.text,
                        nosignal: _nosignal.text,
                        nokakaotalk: _nokakaotalk.text,
                        noijazah: _noijazah.text,
                        nonisn: _nonisn.text,
                        tgllahir: _tgllahir.text,
                        nowechat: _nowechat.text,
                        karir: _karir.text,
                        penghargaan: _penghargaan.text,
                        pendidikan: _pendidikan.text,
                        status: _status.text,
                        jabatan: _jabatan.text,
                        ishideKtp: isHideKTP);

                    setState(() {
                      isloading = true;
                    });
                  }
                  debugPrint(isloading.toString());

                  showAlertDialog(context);
                },
                child: const Icon(Icons.check)),
          ],
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            ListView(
              children: [
                const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Center(
                    child: Text(
                      'Formulir Perubahan Profil',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: ColorName.blueprimary),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 110,
                        width: 110,
                        child: CachedNetworkImage(
                          imageUrl: widget.usermodel.avatar,
                          fit: BoxFit.fitHeight,
                          placeholder: (context, url) {
                            return Image.network(
                              widget.usermodel.avatar,
                              fit: BoxFit.fitHeight,
                            );
                          },
                          errorWidget: (context, url, error) {
                            return const Icon(Icons.error);
                          },
                        ),
                      ),
                      SizedBox(
                        width: Get.width * .5,
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Username',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _username,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _username,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.username,
                                      //labelText: 'Username',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: Get.width * 0.3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Nomor KTP',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: ColorName.blackprimary
                                                  .withOpacity(0.7)),
                                        ),
                                        TextFormField(
                                          controller: _noktp,
                                          keyboardType: TextInputType.number,
                                          onSaved: (String? value) {},
                                          onChanged: (value) => _noktp,
                                          validator: (value) {
                                            if (value!.isEmpty) {
                                              return "username cannot be empty";
                                            }
                                            return null;
                                          },
                                          decoration: InputDecoration(
                                            border:
                                                const UnderlineInputBorder(),
                                            hintText: widget.usermodel.noKtp,
                                            //labelText: 'No KTP',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Switch(
                                    // thumb color (round icon)
                                    activeColor: ColorName.redprimary,
                                    activeTrackColor: Colors.grey,
                                    inactiveThumbColor:
                                        Colors.blueGrey.shade600,
                                    inactiveTrackColor: Colors.grey.shade400,
                                    splashRadius: 50.0,
                                    // boolean variable value
                                    value: isHideKTP,
                                    // changes the state of the switch
                                    onChanged: (value) =>
                                        setState(() => isHideKTP = value),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: _dropdownIsMariageStatus(),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Alamat',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _address,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _address,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.address,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Tanggal lahir',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14,
                                            color: ColorName.blackprimary
                                                .withOpacity(0.7)),
                                      ),
                                      GestureDetector(
                                          onTap: () {
                                            showDatePicker(
                                              context: context,
                                              initialDate: DateTime.now(),
                                              firstDate: DateTime(1900),
                                              lastDate: DateTime(2099),
                                            ).then((date) {
                                              //tambahkan setState dan panggil variabel _dateTime.
                                              setState(() {
                                                _tgllahir.text =
                                                    DateFormat('yyyy-MM-dd')
                                                        .format(date!);
                                              });
                                            });
                                          },
                                          child:
                                              const Icon(Icons.calendar_month)),
                                    ],
                                  ),
                                  // ignore: unnecessary_null_comparison
                                  _tgllahir != null || _tgllahir.text != ''
                                      ? Text(
                                          _tgllahir.text,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: ColorName.blackprimary
                                                  .withOpacity(0.7)),
                                        )
                                      : Text(
                                          widget.usermodel.birthday,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: ColorName.blackprimary
                                                  .withOpacity(0.7)),
                                        ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'No NISN',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _nonisn,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _nonisn,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.nonisn,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'No Ijazah',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _noijazah,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _noijazah,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noijazah,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Jabatan',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _jabatan,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _jabatan,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.jabatan,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Karir',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _karir,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _karir,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.karir,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Pendidikan',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _pendidikan,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _pendidikan,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.pendidikan,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Penghargaan',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _penghargaan,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _penghargaan,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.penghargaan,
                                      // labelText: 'Address',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Bio',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _biodata,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _biodata,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.bio,
                                      //labelText: 'Biodata',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Nomor Handphone',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _nohandphone,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _nohandphone,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noHp,
                                      // labelText: 'No Handphone',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Nomor We chat',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _nowechat,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _nowechat,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noWechat,
                                      // labelText: 'No Handphone',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Kontak Line',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _noline,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _noline,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noLine,
                                      // labelText: 'No Line',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Kontak Whatsapp',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _nowhatsapp,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _nowhatsapp,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noWhatsapp,
                                      // labelText: 'No Whatsapp',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Kontak Signal',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _nosignal,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _nosignal,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noSignal,
                                      // labelText: 'No Signal',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20, right: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Kontak Kakao Talk',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary
                                            .withOpacity(0.7)),
                                  ),
                                  TextFormField(
                                    controller: _nokakaotalk,
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) {},
                                    onChanged: (value) => _nokakaotalk,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "username cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: widget.usermodel.noKakaoTalk,
                                      // labelText: 'No Kakao Talk',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _dropdownIsMariageStatus() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
          child: SizedBox(
            width: Get.width,
            child: CustomDropdown(
              hintText: 'Status Perkawinan',
              items: const ['Lajang', 'Sudah Menikah', 'Duda', 'Janda'],
              controller: _status,
              excludeSelected: false,
              onChanged: (value) {
                setState(() {
                  _status.text = value;
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: ()  => Navigator.pop(context),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Informasi Update"),
      content: const Text("Data profil berhasil di update"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () async => false, child: alert);
      },
    );
  }
}

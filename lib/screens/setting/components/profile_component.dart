import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../gen/colors.gen.dart';
import '../../../models/user/user_model.dart';
import '../controller/setting_controller.dart';

class ProfileComponent extends StatefulWidget {
  const ProfileComponent({super.key, required this.usermodel});
  final UserModel usermodel;
  @override
  State<ProfileComponent> createState() => _ProfileComponentState();
}

class _ProfileComponentState extends State<ProfileComponent> {

   final SettingController settingController = Get.put(SettingController());

  final TextEditingController _username = TextEditingController();
  final TextEditingController _noktp = TextEditingController();
  final TextEditingController _address = TextEditingController();
  final TextEditingController _statusMariage = TextEditingController();
  final TextEditingController _biodata = TextEditingController();
  final TextEditingController _nohandphone = TextEditingController();
  final TextEditingController _nowhatsapp = TextEditingController();
  final TextEditingController _noline = TextEditingController();
  final TextEditingController _nokakaotalk = TextEditingController();
  final TextEditingController _nosignal = TextEditingController();

  final TextEditingController _tgllahir = TextEditingController();
  final TextEditingController _nonisn = TextEditingController();
  final TextEditingController _noijazah = TextEditingController();
  final TextEditingController _nowechat = TextEditingController();
  final TextEditingController _karir = TextEditingController();
  final TextEditingController _jabatan = TextEditingController();
  final TextEditingController _status = TextEditingController();
  final TextEditingController _pendidikan = TextEditingController();
  final TextEditingController _penghargaan = TextEditingController();

  bool isHideKTP = false;
  bool iseditable = true;
  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    return  Container(
      color: Theme.of(context).canvasColor,
      // constraints: const BoxConstraints(maxWidth: 21),
      height: 520,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  IconButton(
                      tooltip: 'Tekan untuk edit profil',
                      onPressed: () {
                         if (iseditable == true) {
                              setState(() {
                                iseditable = false;
                              });
                            } else {
                              setState(() {
                                iseditable = true;
                              });
                            }
                       
                      },
                      icon: Icon(
                        Icons.edit,
                        color: iseditable == false ? Colors.red : Colors.blue,
                      )), iseditable == true ? const  Text('Edit profile', style: TextStyle(fontWeight: FontWeight.bold),) : const Text('Kamu sedang melakukan edit profil',  style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
             iseditable == false ? Row(
               children: [
                 const Text('Simpan',  style: TextStyle(fontWeight: FontWeight.bold)),
                isloading == true ? const CircularProgressIndicator(): IconButton(
                          tooltip: 'Tekan save profile',
                          onPressed: () {
                            if(isloading == false){
                          setState(() {
                            isloading = true;
                            debugPrint('is loading = $isloading');
                          });
                           debugPrint(isloading.toString());
                      settingController.updateUserData(
                          uid: widget.usermodel.uid,
                          usermodel: widget.usermodel,
                          username: _username.text,
                          noktp: _noktp.text,
                          address: _address.text,
                          biodata: _biodata.text,
                          nohandphone: _nohandphone.text,
                          noline: _noline.text,
                          nowhatsapp: _nowhatsapp.text,
                          nosignal: _nosignal.text,
                          nokakaotalk: _nokakaotalk.text,
                          noijazah: _noijazah.text,
                          nonisn: _nonisn.text,
                          tgllahir: _tgllahir.text,
                          nowechat: _nowechat.text,
                          karir: _karir.text,
                          penghargaan: _penghargaan.text,
                          pendidikan: _pendidikan.text,
                          status: _status.text,
                          jabatan: _jabatan.text,
                          ishideKtp: isHideKTP);
                          setState(() {
                            isloading = false;
                            iseditable = true;
                          });
                          debugPrint('is loading = $isloading');
                        } else{
                          
                        }
                          },
                          icon: const Icon(
                            Icons.send,
                            color: Colors.blue,
                          )),
                         
               ],
             ) : const SizedBox.shrink(),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Username',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                  readOnly: iseditable,
                  controller: _username,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _username,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.username,
                    //labelText: 'Username',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Row(
              children: [
                SizedBox(
                  width: Get.width * 0.6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Nomor KTP',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        readOnly: iseditable,
                        controller: _noktp,
                        keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _noktp,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noKtp,

                          //labelText: 'No KTP',
                        ),
                      ),
                    ],
                  ),
                ),
                Switch(
                  // thumb color (round icon)
                  activeColor: ColorName.redprimary,
                  activeTrackColor: Colors.grey,
                  inactiveThumbColor: Colors.blueGrey.shade600,
                  inactiveTrackColor: Colors.grey.shade400,
                  splashRadius: 50.0,
                  // boolean variable value
                  value: isHideKTP,
                  // changes the state of the switch
                  onChanged: (value) => setState(() => isHideKTP = value),
                ),
              ],
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
          //   child: _dropdownIsMariageStatus(),
          // ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Alamat',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                  readOnly: iseditable,
                  controller: _address,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _address,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.address,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Tanggal lahir',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: ColorName.blackprimary.withOpacity(0.7)),
                    ),
                    GestureDetector(
                        onTap: () {
                          showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2099),
                          ).then((date) {
                            //tambahkan setState dan panggil variabel _dateTime.
                            setState(() {
                              _tgllahir.text =
                                  DateFormat('yyyy-MM-dd').format(date!);
                            });
                          });
                        },
                        child: const Icon(Icons.calendar_month)),
                  ],
                ),
                // ignore: unnecessary_null_comparison
                _tgllahir != null || _tgllahir.text != ''
                    ? Text(
                        _tgllahir.text,
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      )
                    : Text(
                        widget.usermodel.birthday,
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'No NISN',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                  readOnly: iseditable,
                  controller: _nonisn,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _nonisn,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.nonisn,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'No Ijazah',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _noijazah,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _noijazah,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noijazah,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Jabatan',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _jabatan,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _jabatan,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.jabatan,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Karir',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _karir,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _karir,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.karir,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Pendidikan',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _pendidikan,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _pendidikan,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.pendidikan,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Penghargaan',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _penghargaan,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _penghargaan,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.penghargaan,
                    // labelText: 'Address',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Bio',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _biodata,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _biodata,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.bio,
                    //labelText: 'Biodata',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Nomor Handphone',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _nohandphone,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _nohandphone,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noHp,
                    // labelText: 'No Handphone',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Nomor We chat',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _nowechat,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _nowechat,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noWechat,
                    // labelText: 'No Handphone',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Kontak Line',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _noline,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _noline,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noLine,
                    // labelText: 'No Line',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Kontak Whatsapp',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _nowhatsapp,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _nowhatsapp,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noWhatsapp,
                    // labelText: 'No Whatsapp',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Kontak Signal',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _nosignal,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _nosignal,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noSignal,
                    // labelText: 'No Signal',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Kontak Kakao Talk',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorName.blackprimary.withOpacity(0.7)),
                ),
                TextFormField(
                    readOnly: iseditable,
                  controller: _nokakaotalk,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => _nokakaotalk,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "username cannot be empty";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: widget.usermodel.noKakaoTalk,
                    // labelText: 'No Kakao Talk',
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import '../../models/user/user_model.dart';
import '../../responsive.dart';
import '../main/controller/main_controller.dart';
import 'components/profile_component.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {

  final MainController mainController = Get.put(MainController());
  UserModel? usermodel;

  @override
  void initState() {
    super.initState();
    usermodel = mainController.usermodel.value;
    // filteredList = dataList;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
            padding: const EdgeInsets.all(defaultPadding),
            child: Column(
              children: [
                // const Header(),
                const SizedBox(height: defaultPadding),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          if (Responsive.isDesktop(context))
                            usermodel == null ? const CircularProgressIndicator() : ProfileComponent(usermodel: usermodel!,),
                         // if (Responsive.isMobile(context))
                        //    const UserComponentMobile(),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )));
  }
}

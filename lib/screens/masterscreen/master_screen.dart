import 'package:flutter/material.dart';

class MasterScreen extends StatefulWidget {
  const MasterScreen({super.key});

  @override
  State<MasterScreen> createState() => _MasterScreenState();
}

class _MasterScreenState extends State<MasterScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        //  future: firebaseAuthServices.loginUser(),
        builder: (context, snapshot) {
      if (snapshot.hasError) {
        return const Center(
          child: Text(
            "Something went wrong",
          ),
        );
      }
      if (snapshot.connectionState == ConnectionState.done) {}
      return const Center(child: CircularProgressIndicator());
    });
  }
}

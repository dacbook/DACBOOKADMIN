// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';

import '../../services/firestore_services.dart';
import '../dashboard/components/storage_info_card.dart';

class BookScreen extends StatefulWidget {
  const BookScreen({Key? key}) : super(key: key);

  @override
  _BookScreenState createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {
  List dataList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Data Buku Alumni"),
      ),
      body: FutureBuilder(
        future: FireStoreDataBase().getAllBookData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Text(
              "Something went wrong",
            );
          }
          if (snapshot.connectionState == ConnectionState.done) {
            dataList = snapshot.data as List;
            return buildItems(dataList);
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildItems(dataList) => ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: dataList.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemBuilder: (BuildContext context, int index) {
        List countbookmember = dataList[index]["bookmember"] as List;
        return StorageInfoCard(
          svgSrc: "assets/icons/Documents.svg",
          title: dataList[index]["bookname"],
          ismasterbook: dataList[index]["ismasterbook"],
          amountOfFiles: "",
          numOfFiles: countbookmember.length,
          isimage: false,
           isbook: true,
        );
      });
}

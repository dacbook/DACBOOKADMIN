import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/local/box_storage.dart';
import '../../../models/book/book_model.dart';
import '../../../models/book_organization/book_organization_model.dart';
import '../../../models/book_position/book_position_rank_model.dart';
import '../../../models/feed/feed_model.dart';
import '../../../models/user/user_model.dart';
import '../../../services/firebase_auth.dart';
import '../../../services/firestore_services.dart';

class DashboardController extends GetxController {
  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
  }

  final FirebaseAuthServices firebaseAuthServices = FirebaseAuthServices();
   final FirebaseFirestore db = FirebaseFirestore.instance;
  final BoxStorage boxStorage = BoxStorage();
  final usermodel = Rxn<UserModel>();
  var txtcontrollerpositionlevel = TextEditingController().obs;
  var txtcontrollerpositionname = TextEditingController().obs;
  var docid = ''.obs;
  var isupdateorganization = false.obs;
  var isopenupdateorganization = false.obs;

  getUser() async {
    String uid = boxStorage.getUserId();
    UserModel userdatamodel = await firebaseAuthServices.loginCurrentUser(uid);
    debugPrint('clog model user => $userdatamodel');
    return usermodel;
  }

  String collectionNameBook = 'book';
  String collectionBookForum = 'bookforum';
  String collectionBookGaleri = 'bookgallery';
  String collectionBookAgenda = 'bookagenda';

  String subcollectionNameBook = 'bookmember';

  Stream<List<BookModel>> getBookListDashboard(String bookmasterid) {
    debugPrint('is data book id : $bookmasterid');
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionNameBook);
    return databook
        // .orderBy('date', descending: true)
        .where('bookmasterid', isEqualTo: bookmasterid)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data()),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getBookForum(String bookmasterid) {
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionBookForum);
    return databook
        // .orderBy('date', descending: true)
        .where('bookmasterid', isEqualTo: bookmasterid)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getBookAgenda(String bookmasterid) {
    debugPrint('is data book agenda id : $bookmasterid');
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionBookAgenda);

    return databook
        .where('bookid', isEqualTo: bookmasterid)
        //  .where('categoryKegiatan', isEqualTo: categoryKegiatan)
        // .orderBy('timestamp', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getBookAgendaFilter(
      String bookmasterid, String filter) {
    debugPrint('is data book agenda id : $bookmasterid - $filter');
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionBookAgenda);

    return databook
        .where('bookid', isEqualTo: bookmasterid)
        .where('category', arrayContainsAny: [filter])
        // .orderBy('timestamp', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<UserModel>> getBookMember(String bookmasterid) {
    debugPrint(
        'is data book id : $bookmasterid - $collectionNameBook - $subcollectionNameBook');
    return FireStoreDataBase()
        .streamCollectionGroup(
          collectionName: collectionNameBook,
        )
        .doc(bookmasterid)
        .collection(subcollectionNameBook)
        //.where('bookid', isEqualTo: bookmasterid)
        //  .where('categoryKegiatan', isEqualTo: categoryKegiatan)
        // .orderBy('timestamp', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (DocumentSnapshot doc) => UserModel.fromJson(
                    doc.data() as Map<String, dynamic>),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getBookGalery(String bookmasterid) {
    debugPrint('is data galeri book id : $bookmasterid');
    var databook =
        FireStoreDataBase().streamData(collectionName: 'bookgallery');
    return databook
       // .orderBy('date', descending: true)
        .where('bookid', isEqualTo: bookmasterid)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getBookGaleryFilter(
      {required String bookmasterid,
      required String bookcategoryMedia,
      required String bookcategoryGalery}) {
    debugPrint(
        'is data book id : $bookmasterid - $bookcategoryMedia - $bookcategoryGalery');
    var databook =
        FireStoreDataBase().streamData(collectionName: collectionBookGaleri);
    return databook
        // .orderBy('date', descending: true)
        .where('bookid', isEqualTo: bookmasterid)
        .orderBy('date', descending: true)
        .where('subscription', isEqualTo: bookcategoryGalery)
        .where('category', arrayContainsAny: [bookcategoryMedia])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<BookOrganizationModel>> getBookOrganization(String bookmasterid) {
    debugPrint('is data book id : $bookmasterid');
    var databook = FireStoreDataBase()
        .streamData(collectionName: collectionNameBook)
        .doc(bookmasterid)
        .collection('bookmember');
    return databook.where('positionlevel', isNull: false).snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookOrganizationModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<BookPositionRank>> getPositionRank(String bookid) {
    debugPrint('book id : $bookid');
    var datapositionrank = FireStoreDataBase()
        .streamData(collectionName: collectionNameBook)
        .doc(bookid)
        .collection('positionrank');
    return datapositionrank
        .orderBy('positionLevel', descending: false)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookPositionRank.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Future<void> updateBookmemberlevel(
      {required String? bookid,
      required String? documentid,
      required String? levelname}) async {
    await FireStoreDataBase().updateDocumentBookmember(
        documentId: documentid!,
        collectionName: collectionNameBook,
        subcollectionName: subcollectionNameBook,
        bookid: bookid!,
        level: levelname!);
    debugPrint('success to delete');
  }

  Future<void> deleteBookForum(String bookid) async {
    await FireStoreDataBase().deleteDocument(
        documentId: bookid, collectionName: collectionBookForum);
    debugPrint('success to delete');
  }

  Future<void> deleteBookAgenda(String bookid) async {
    await FireStoreDataBase().deleteDocument(
        documentId: bookid, collectionName: collectionBookAgenda);
    debugPrint('success to delete');
  }

  Future<void> deleteBookGalery(String bookid) async {
    await FireStoreDataBase().deleteDocument(
        documentId: bookid, collectionName: collectionBookGaleri);
    debugPrint('success to delete');
  }

  Future<void> deleteBookMember(String bookid, String subdocumentid) async {
    await FireStoreDataBase().deleteDocumentOnSubCollection(
        documentId: bookid,
        collectionName: collectionNameBook,
        subcollectionname: subcollectionNameBook,
        subdocumentid: subdocumentid);
    debugPrint('success to delete');
  }

  Future<void> deleteBookOrganization(String bookid, String documentid) async {
    await FireStoreDataBase().deleteDocumentOnSubCollection(
        documentId: bookid,
        collectionName: collectionBookGaleri,
        subcollectionname: 'bookmember',
        subdocumentid: documentid);
    debugPrint('success to delete');
  }

   updatepositionmember(String bookid, String uid, String positionName, String positionLevel) {
    final collRef = db.collection('book').doc(bookid).collection('bookmember');
    DocumentReference docReference = collRef.doc(uid);
    Map<String, dynamic> data = {
      'positionlevel': positionLevel,
      'positionname': positionName,
    };
    docReference.update(data);
  }

  var isdataforumonly = false.obs;
  var isdatabookonly = false.obs;
  var isdatagaleryonly = false.obs;
  var isdatastrukturonly = false.obs;
  var isdataagendaonly = false.obs;
  var isdatauseronly = false.obs;
  var isactived = false.obs;

  var populatealumni = 0.obs;

  selectedOnBookMenus(int index) {
    switch (index) {
      //book
      case 0:
        isdatabookonly.value = true;

        isdatastrukturonly.value = true;
        isdataforumonly.value = false;
        isdatagaleryonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //struktur
      case 1:
        isdatastrukturonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatagaleryonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //galeri
      case 2:
        isdatagaleryonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //forum
      case 3:
        isdataforumonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //agenda
      case 4:
        isdataagendaonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
      case 5:
        isdatauseronly.value = true;

        isdataagendaonly.value = false;
        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
    }
  }
  selectedOnSubBookMenus(int index) {
    switch (index) {
      
      //struktur
      case 0:
        isdatastrukturonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatagaleryonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //galeri
      case 1:
        isdatagaleryonly.value = true;

        isdatabookonly.value = false;
        isdataforumonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //forum
      case 2:
        isdataforumonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataagendaonly.value = false;
        isdatauseronly.value = false;
        break;
      //agenda
      case 3:
        isdataagendaonly.value = true;

        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
      case 4:
        isdatauseronly.value = true;

        isdataagendaonly.value = false;
        isdatabookonly.value = false;
        isdatagaleryonly.value = false;
        isdatastrukturonly.value = false;
        isdataforumonly.value = false;
        isdatauseronly.value = false;
        break;
    }
  }

  createPositionRank(
      {required String positionLevel,
      required String positionName,
      required String bookid,
      bool? isupdate,
      String? id}) {
    final db = FirebaseFirestore.instance;
    debugPrint(
        'position data : $positionLevel $positionName , $bookid , $isupdate, $id');
    final collRef =
        db.collection('book').doc(bookid).collection('positionrank');

    if (isupdate == true) {
      DocumentReference docReference = collRef.doc(id);
      Map<String, dynamic> data = {
        'positionName': positionName,
        'positionLevel': positionLevel,
      };
      docReference.update(data);
    } else {
      DocumentReference docReference = collRef.doc();
      Map<String, dynamic> data = {
        'id': docReference.id,
        'bookmasterid': id,
        'positionName': positionName,
        'positionLevel': positionLevel,
      };
      docReference.set(data);
    }

    debugPrint('success create position rank');
  }

 
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../models/feed/feed_model.dart';
import '../../main/controller/main_controller.dart';
import '../controllers/dashboard_controller.dart';
import 'datatable_agenda.dart';

final db = FirebaseFirestore.instance;

class DashboardDataagendaTable extends StatefulWidget {
  const DashboardDataagendaTable({super.key, required this.bookmasterid});
  final String bookmasterid;
  @override
  State<DashboardDataagendaTable> createState() =>
      _DashboardDataagendaTableState();
}

class _DashboardDataagendaTableState extends State<DashboardDataagendaTable> {
  final MainController mainController = Get.put(MainController());
  String? userrole;
  String? seletectedCategory;
  bool isfilteractived = false;
  int count = 0;

  @override
  Widget build(BuildContext context) {
    List<FeedModel> listData = [];
    final DashboardController dashboardController =
        Get.put(DashboardController());
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder(
            stream: isfilteractived == false ? dashboardController.getBookAgenda(widget.bookmasterid) : dashboardController.getBookAgendaFilter(widget.bookmasterid, seletectedCategory!),
            builder: (context, snapshot) {
              debugPrint('is data book ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasError ||
                  !snapshot.hasData ||
                  snapshot.data == [] ||
                  snapshot.data == null) {
                return SizedBox(
                  height: Get.height * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Tidak ada data buku',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.grey[600]),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                );
              } else {
                listData = snapshot.data!;
                userrole = mainController.usermodel.value!.userRole;

                AgendaDataTable dataSource =
                    AgendaDataTable(listData, userrole!);

                return Column(
                  children: [
                    Text(
                      "Data Kegiatan Buku",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            height: 60,
                            width: Get.width * .3,
                            padding: const EdgeInsets.all(5),
                            child: StreamBuilder<QuerySnapshot>(
                                stream: db.collection('mCategoryEvent').snapshots(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }

                                  return Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          'Kategori Agenda',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          child: DropdownButton(
                                            value: seletectedCategory,
                                            onTap: (){
                                                debugPrint('is category : $seletectedCategory');
                                                isfilteractived = true;
                                            },
                                            isDense: true,
                                            items: snapshot.data!.docs
                                                .map((DocumentSnapshot doc) {
                                              return DropdownMenuItem<String>(
                                                  value: doc['name'],
                                                  child: Text(doc['name']));
                                            }).toList(),
                                            hint: const Text("Tentukan kategori"),
                                            onChanged: (value) {
                                              setState(() {
                                                seletectedCategory = value;
                                              });
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                })),
                               const SizedBox.shrink()
                      ],
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: PaginatedDataTable(
                        source: dataSource,
                        onPageChanged:(value) => count,
                        columnSpacing: 10,
                        horizontalMargin: 10,
                        rowsPerPage: 10,
                        showCheckboxColumn: false,
                        //minWidth: 700,
                        columns: [
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Title",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Deskripsi",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Tanggal",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Media",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          DataColumn(
                            label:
                                userrole != 'user' && userrole != 'kepala dinas'
                                    ? const SizedBox(
                                        width: 100,
                                        child: Text("Aksi",
                                            overflow: TextOverflow.ellipsis))
                                    : const SizedBox.shrink(),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }
            },
          ),
        ],
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dacweb/controllers/menu_controller.dart' as menucontroller;
import 'package:dacweb/models/user/user_model.dart';
import 'package:dacweb/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';
import '../../../services/firebase_auth.dart';
import '../../main/controller/main_controller.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      final FirebaseAuthServices firebaseAuthServices = FirebaseAuthServices();
      final MainController mainController = Get.put(MainController());
    return FutureBuilder(
         future: firebaseAuthServices.loginUser(),
        builder: (context, snapshot) {
      if (snapshot.hasError) {
        return const Center(
          child: Text(
            "Something went wrong",
          ),
        );
      }
      if (snapshot.connectionState == ConnectionState.done) {
         UserModel usermodel = snapshot.data!;
        return Row(
          children: [
            if (!Responsive.isDesktop(context))
              IconButton(
                icon: const Icon(Icons.menu),
                onPressed:
                    context.read<menucontroller.MenuController>().controlMenu,
              ),
            if (!Responsive.isMobile(context))
              Obx(
               () =>Text(
                  mainController.pagename.value == ''? 'Dashboard' : mainController.pagename.value,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            if (!Responsive.isMobile(context))
              Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
           // const Expanded(child: SearchField()),
           // ProfileCard(userdata: usermodel)
          ],
        );
      }
      return const Center(child: CircularProgressIndicator());
    });
  }
}

class ProfileCard extends StatelessWidget {
  const ProfileCard({Key? key, required this.userdata}) : super(key: key);
  final UserModel userdata;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: defaultPadding),
      padding: const EdgeInsets.symmetric(
        horizontal: defaultPadding,
        vertical: defaultPadding / 2,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.white10),
      ),
      child: Row(
        children: [
          Image(
            height: 38,
            width: 38,
            image: CachedNetworkImageProvider(
              userdata.avatar,
            ),
          ),
          if (!Responsive.isMobile(context))
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
              child: Text(userdata.username),
            ),
          const Icon(Icons.keyboard_arrow_down),
        ],
      ),
    );
  }
}

class SearchField extends StatelessWidget {
  const SearchField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: "Cari",
        fillColor: Theme.of(context).canvasColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        suffixIcon: InkWell(
          onTap: () {},
          child: Container(
            padding: const EdgeInsets.all(defaultPadding * 0.75),
            margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
            decoration: const BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: SvgPicture.asset("assets/icons/Search.svg"),
          ),
        ),
      ),
    );
  }
}

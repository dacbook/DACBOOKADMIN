import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../models/feed/feed_model.dart';
import '../../main/controller/main_controller.dart';
import '../controllers/dashboard_controller.dart';
import 'datatable_galery.dart';

final db = FirebaseFirestore.instance;

class DashboardDatagaleryTable extends StatefulWidget {
  const DashboardDatagaleryTable({super.key, required this.bookmasterid});
  final String bookmasterid;
  @override
  State<DashboardDatagaleryTable> createState() =>
      _DashboardDatagaleryTableState();
}

class _DashboardDatagaleryTableState extends State<DashboardDatagaleryTable> {
  final MainController mainController = Get.put(MainController());
  String? userrole;
  String? seletectedCategoryMedia;
  String? seletectedCategory;
  bool isfilterCategoryActive = false;

  @override
  Widget build(BuildContext context) {
    List<FeedModel> listData = [];
    final DashboardController dashboardController =
        Get.put(DashboardController());
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder(
            stream: isfilterCategoryActive == true
                ? dashboardController.getBookGaleryFilter(
                    bookmasterid: widget.bookmasterid,
                    bookcategoryMedia: seletectedCategoryMedia!,
                    bookcategoryGalery: seletectedCategory!)
                : dashboardController.getBookGalery(widget.bookmasterid),
            builder: (context, snapshot) {
              debugPrint('is data galeri book ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasError ||
                  !snapshot.hasData ||
                  snapshot.data == [] ||
                  snapshot.data == null) {
                return SizedBox(
                  height: Get.height * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Tidak ada data galeri',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.grey[600]),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                );
              } else {
                listData = snapshot.data!;

                userrole = mainController.usermodel.value!.userRole;

                GaleryDataTable dataSource =
                    GaleryDataTable(listData, userrole!, context);

                return Column(
                  children: [
                    Text(
                      "Data Galeri",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    Row(
                      children: [
                        Container(
                            height: 60,
                            width: Get.width * .3,
                            padding: const EdgeInsets.all(5),
                            child: StreamBuilder<QuerySnapshot>(
                                stream: db.collection('mGalery').snapshots(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }

                                  return Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          'Kategori Media',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          child: DropdownButton(
                                            value: seletectedCategoryMedia,
                                            isDense: true,
                                            items: snapshot.data!.docs
                                                .map((DocumentSnapshot doc) {
                                              return DropdownMenuItem<String>(
                                                  value: doc['categoryName'],
                                                  child: Text(
                                                      doc['categoryName']));
                                            }).toList(),
                                            hint:
                                                const Text("Tentukan kategori"),
                                            onChanged: (value) {
                                              setState(() {
                                                seletectedCategoryMedia = value;
                                              });
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                })),
                        seletectedCategoryMedia == null
                            ? const SizedBox.shrink()
                            : Container(
                                height: 60,
                                width: Get.width * .3,
                                padding: const EdgeInsets.all(5),
                                child: StreamBuilder<QuerySnapshot>(
                                    stream: db
                                        .collection('book')
                                        .doc(widget.bookmasterid)
                                        .collection('galerycategory')
                                        .snapshots(),
                                    builder: (context, snapshot) {
                                      if (!snapshot.hasData) {
                                        return const Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }

                                      return Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Kategori $seletectedCategoryMedia',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.all(5),
                                              child: DropdownButton(
                                                value: seletectedCategory,
                                                isDense: true,
                                                items: snapshot.data!.docs.map(
                                                    (DocumentSnapshot doc) {
                                                  return DropdownMenuItem<
                                                          String>(
                                                      value: doc['category'],
                                                      child: Text(
                                                          doc['category']));
                                                }).toList(),
                                                hint: const Text(
                                                    "Tentukan kategori"),
                                                onChanged: (value) {
                                                  setState(() {
                                                    seletectedCategory = value;
                                                  });
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    })),
                        seletectedCategoryMedia == null
                            ? const SizedBox.shrink() : SizedBox(
                          // width: 100,
                          // height: 90,
                          child: IconButton(
                            color: Colors.white,
                            iconSize: 30,
                            onPressed: () {
                              if (isfilterCategoryActive == false) {
                                setState(() {
                                  isfilterCategoryActive = true;
                                });
                              } else {
                                setState(() {
                                  seletectedCategoryMedia = null;
                                  seletectedCategory = null;
                                  isfilterCategoryActive = false;
                                });
                              }
                            },
                            icon: Container(
                             
                              decoration: BoxDecoration(
                                color: isfilterCategoryActive == false
                                    ? primaryColor
                                    : Colors.red,
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                              ),
                              child: isfilterCategoryActive == false
                                  ?  const Icon(Icons.search)
                                  : const Icon(Icons.clear),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: PaginatedDataTable(
                        source: dataSource,
                        columnSpacing: 10,
                        horizontalMargin: 10,
                        rowsPerPage: 10,
                        showCheckboxColumn: false,
                        //minWidth: 700,
                        columns: [
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Title",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Deskripsi forum",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text("Tanggal",
                                    overflow: TextOverflow.ellipsis)),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text("Jenis Media",
                                    overflow: TextOverflow.ellipsis)),
                          ),
                          // const DataColumn(
                          //   label: SizedBox(
                          //       width: 100,
                          //       child: Text("Media",
                          //           overflow: TextOverflow.ellipsis)),
                          // ),
                          DataColumn(
                            label:
                                userrole != 'user' && userrole != 'kepala dinas'
                                    ? const SizedBox(
                                        width: 100,
                                        child: Text("Aksi",
                                            overflow: TextOverflow.ellipsis))
                                    : const SizedBox.shrink(),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }
            },
          ),
        ],
      ),
    );
  }
}

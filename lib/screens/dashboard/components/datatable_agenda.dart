import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:js' as js;

import '../../../models/feed/feed_model.dart';
import '../../../utils/video_player_utils/media_player_preview.dart';
import '../controllers/dashboard_controller.dart';

final DashboardController dashboardController = Get.put(DashboardController());

class AgendaDataTable extends DataTableSource {
  List<FeedModel> dataList;
  String userrole;
  AgendaDataTable(this.dataList, this.userrole) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.title,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.description,
            overflow: TextOverflow.ellipsis,
          ),
        ),
       
        DataCell(Text(data.date)),
         DataCell(SizedBox(
          width: 100,
          child: TextButton(
            onPressed: () {
             js.context.callMethod('open', [data.mediaFeedList[0]]);
            },
            child: const Text('Lihat Media'),
            //...
          ),
        )),
      
        DataCell(
           userrole != 'user' && userrole != 'kepala dinas' ?
          SizedBox(
          width: 100,
          child:
              TextButton(
                onPressed: () => dashboardController.deleteBookForum(data.id),
                child: const Text('Hapus'),
                //...
              ),
        ):  const SizedBox.shrink()),
      ],
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/models/book_organization/book_organization_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../gen/colors.gen.dart';
import '../../../models/book_position/book_position_rank_model.dart';
import '../../../responsive.dart';
import '../controllers/dashboard_controller.dart';
import 'create_organization_position.dart';
import 'datatable_organization.dart';

final db = FirebaseFirestore.instance;

class DashboardDataorganizationTable extends StatefulWidget {
  const DashboardDataorganizationTable({super.key, required this.bookmasterid});
  final String bookmasterid;
  @override
  State<DashboardDataorganizationTable> createState() =>
      _DashboardDataorganizationTableState();
}

class _DashboardDataorganizationTableState
    extends State<DashboardDataorganizationTable> {
  bool positionrank = false;
  bool iscreatepositionrank = false;

  @override
  Widget build(BuildContext context) {
    List<BookOrganizationModel> listData = [];
    final DashboardController dashboardController =
        Get.put(DashboardController());
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 200,
                child: ElevatedButton.icon(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.green,
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultPadding * 1.5,
                      vertical: defaultPadding /
                          (Responsive.isMobile(context) ? 2 : 1),
                    ),
                  ),
                  onPressed: () {
                    if (positionrank == false || iscreatepositionrank == false) {
                      setState(() {
                        positionrank = true;
                        iscreatepositionrank = true;
                      });
                    } else {
                      setState(() {
                        positionrank = false;
                        iscreatepositionrank = false;
                      });
                    }
                  },
                  icon: const Icon(Icons.change_circle),
                  label: positionrank == false ?  const Text("Ubah Struktur"): const Text("Lihat Organisasi"),
                ),
              ),
       iscreatepositionrank == false ? const SizedBox.shrink() :      SizedBox(
                width: 200,
                child: ElevatedButton.icon(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.green,
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultPadding * 1.5,
                      vertical: defaultPadding /
                          (Responsive.isMobile(context) ? 2 : 1),
                    ),
                  ),
                  onPressed: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return showBottomSheet(context, false, null);
                      },
                    );
                  },
                  icon: const Icon(Icons.change_circle),
                  label: const Text("Tambah Posisi"),
                ),
              ),
            ],
          ),
          positionrank == true
              ? Column(
                children: [
                  positionRankOrganization(),
                const  SizedBox(height: 50,)
                ],
              )
              : StreamBuilder(
                  stream: dashboardController
                      .getBookOrganization(widget.bookmasterid),
                  builder: (context, snapshot) {
                    debugPrint('is data book ${snapshot.data}');
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    if (snapshot.connectionState == ConnectionState.done ||
                        snapshot.hasError ||
                        !snapshot.hasData ||
                        snapshot.data == [] ||
                        snapshot.data == null) {
                      return SizedBox(
                        height: Get.height * 0.4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Tidak ada data buku',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: Colors.grey[600]),
                            ),
                            const SizedBox(
                              height: 24,
                            ),
                          ],
                        ),
                      );
                    } else {
                      listData = snapshot.data!;

                      OrganizationDataTable dataSource =
                          OrganizationDataTable(listData, widget.bookmasterid, context);

                      return Column(
                        children: [
                          Text(
                            "Data Struktur Organisasi",
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                         
                          SizedBox(
                            width: double.infinity,
                            child: PaginatedDataTable(
                              source: dataSource,
                              columnSpacing: 10,
                              horizontalMargin: 10,
                              rowsPerPage: 10,
                              showCheckboxColumn: false,
                              //minWidth: 700,
                              columns: const [
                                DataColumn(
                                  label: SizedBox(
                                      width: 100,
                                      child: Text(
                                        "Username",
                                        overflow: TextOverflow.ellipsis,
                                      )),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                      width: 100,
                                      child: Text("Posisi Level",
                                          overflow: TextOverflow.ellipsis)),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                      width: 100,
                                      child: Text("Posisi Name",
                                          overflow: TextOverflow.ellipsis)),
                                ),
                                DataColumn(
                                  label: SizedBox(
                                      width: 100,
                                      child: Text("Aksi",
                                          overflow: TextOverflow.ellipsis)),
                                ),
                              ],
                            ),
                          )
                        ],
                      );
                    }
                  },
                ),
        ],
      ),
    );
  }

  int positioncount = 0;
  List<BookPositionRank> bookPositionRank = [];

  Widget positionRankOrganization() {
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: dashboardController.getPositionRank(widget.bookmasterid),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //  debugPrint('data snap : ${snapshot.data}');
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.data.length == 0 || snapshot.data == null) {
          return SizedBox(
              height: Get.height * 0.6,
              width: Get.width * 0.6,
              child: const Center(
                  child: Text(
                'Posisi organisasi buku masih kosong \n\n',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: ColorName.purplelow),
              )));
        } else {
          return ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data?.length,
            itemBuilder: (context, int index) {
              BookPositionRank positionRank = snapshot.data[index];
              bookPositionRank = snapshot.data;
              return ListTile(
                leading: Text('Lv. ${positionRank.positionLevel}'),
                title: Text(positionRank.positionName),
                onTap: () {
                  // Here We Will Add The Update Feature and passed the value 'true' to the is update
                  // feature.

                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return showBottomSheet(context, true, positionRank);
                    },
                  );
                },
                trailing: IconButton(
                  icon: const Icon(
                    Icons.delete_outline,
                  ),
                  onPressed: () {
                    // Here We Will Add The Delete Feature
                    db
                        .collection('book')
                        .doc(widget.bookmasterid)
                        .collection('positionrank')
                        .doc(positionRank.id)
                        .delete();
                  },
                ),
              );
            },
          );
        }
      },
    );
  }

  String? positionName;
  String? positionLevel;
  showBottomSheet(
      BuildContext context, bool isUpdate, BookPositionRank? documentSnapshot) {
    // Added the isUpdate argument to check if our item has been updated
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter mystate) {
      return Padding(
        padding: const EdgeInsets.only(top: 20),
        child: SizedBox(
          // height: Get.height * 0.3,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // SizedBox(
              //   height: 30,
              //   child: Text('${documentSnapshot!['positionName']} Menjadi $positionName'),
              // ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.9,
                child: TextField(
                  keyboardType: TextInputType.number,
                  maxLength: 1,
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      // Used a ternary operator to check if isUpdate is true then display
                      // Update Todo.
                      labelText: isUpdate ? 'Update level' : 'Tambah level',
                      hintText: 'Masukan level'),
                  onChanged: (String val) {
                    // Storing the value of the text entered in the variable value.

                    positionLevel = val;
                  },
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              // SizedBox(
              //   height: 30,
              //   child: Text('${documentSnapshot['positionLevel']} Menjadi $positionLevel'),
              // ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.9,
                child: TextField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    // Used a ternary operator to check if isUpdate is true then display
                    // Update Todo.
                    labelText: isUpdate ? 'Update posisi' : 'Add posisi',
                    hintText: 'tambahkan posisi',
                  ),
                  onChanged: (String val) {
                    // Storing the value of the text entered in the variable value.
                    positionName = val;
                  },
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(ColorName.purplelow),
                  ),
                  onPressed: () {
                    // Check to see if isUpdate is true then update the value else add the value
                    if (isUpdate) {
                      dashboardController.createPositionRank(
                          positionLevel: positionLevel == null
                              ? documentSnapshot!.positionLevel
                              : positionLevel!,
                          positionName: positionName == null
                              ? documentSnapshot!.positionName
                              : positionName!,
                          bookid: widget.bookmasterid,
                          id: documentSnapshot!.id,
                          isupdate: isUpdate);
                    } else {
                      dashboardController.createPositionRank(
                          positionLevel: positionLevel!,
                          positionName: positionName!,
                          bookid: widget.bookmasterid,
                          id: widget.bookmasterid,
                          isupdate: isUpdate);
                    }
                    Navigator.pop(context);
                  },
                  child: isUpdate
                      ? const Text(
                          'Edit',
                          style: TextStyle(color: Colors.white),
                        )
                      : const Text('Tambahkan',
                          style: TextStyle(color: Colors.white))),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      );
    });
  }



}

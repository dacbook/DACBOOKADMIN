import 'package:flutter/material.dart';

import '../../../constants.dart';
import 'chart.dart';
import 'storage_info_card.dart';

class StorageDetails extends StatelessWidget {
  const StorageDetails({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          Text(
            "Populasi Buku",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(height: defaultPadding),
        //  Chart(),
          StorageInfoCard(
            svgSrc: "assets/icons/Documents.svg",
            title: "Buku Kelas 6",
            amountOfFiles: "",
            ismasterbook: false,
            numOfFiles: 2000,
            isimage: false,
            isbook: false,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/Documents.svg",
            title: "Buku Kelas 1",
            amountOfFiles: "",
            ismasterbook: false,
            numOfFiles: 2000,
            isimage: false,
            isbook: false,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/Documents.svg",
            title: "Buku Kelas 2",
            amountOfFiles: "",
            ismasterbook: false,
            numOfFiles: 2000,
            isimage: false,
            isbook: false,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/Documents.svg",
            title: "Buku Kelas 3",
            amountOfFiles: "",
            ismasterbook: false,
            numOfFiles: 2000,
            isimage: false,
            isbook: false,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/Documents.svg",
            title: "Buku Kelas 4",
            amountOfFiles: "",
            ismasterbook: false,
            numOfFiles: 2000,
            isimage: false,
            isbook: false,
          ),
        ],
      ),
    );
  }
}

//import 'package:data_table_2/data_table_2.dart';
import 'package:dacweb/screens/dashboard/controllers/dashboard_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../models/book/book_model.dart';
import '../../../services/firestore_services.dart';
import '../../data_buku/components/source_datatable.dart';
import '../../main/controller/main_controller.dart';
import 'datatable_book.dart';

class DatabookDashboard extends StatefulWidget {
  const DatabookDashboard({super.key, required this.bookmasterid});
  final String bookmasterid;
  @override
  State<DatabookDashboard> createState() => _DatabookDashboardState();
}

class _DatabookDashboardState extends State<DatabookDashboard> {

     final MainController mainController =
        Get.put(MainController());
        String? userrole;

  @override
  Widget build(BuildContext context) {
    List<BookModel> listData = [];
    final DashboardController dashboardController =
        Get.put(DashboardController());
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder(
            stream:
                dashboardController.getBookListDashboard(widget.bookmasterid),
            builder: (context, snapshot) {
              debugPrint('is data book ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasError ||
                  !snapshot.hasData ||
                  snapshot.data == [] ||
                  snapshot.data == null) {
                return SizedBox(
                  height: Get.height * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Tidak ada data buku',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.grey[600]),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                );
              } else {
                listData = snapshot.data!;
                 userrole = mainController.usermodel.value!.userRole;

                BookDataTable dataSource = BookDataTable(listData, userrole!);

                return Column(
                  children: [
                    Text(
                      "Data Buku Alumni",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: PaginatedDataTable(
                        source: dataSource,
                        columnSpacing: 10,
                        horizontalMargin: 10,
                        rowsPerPage: 10,
                        showCheckboxColumn: false,
                        //minWidth: 700,
                        columns:  const [
                       DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Nama Buku",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                       DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "PIN Buku",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                       DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text("Status Buku",
                                    overflow: TextOverflow.ellipsis)),
                          ),
                        DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text("Total alumni",
                                    overflow: TextOverflow.ellipsis)),
                          ),
                       DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text("Catatan buku",
                                    overflow: TextOverflow.ellipsis)),
                          ),
                          // DataColumn(
                          //   label:  userrole != 'user' || userrole != 'kepala dinas' ? const SizedBox(
                          //       width: 100,
                          //       child: Text("Aksi",
                          //           overflow: TextOverflow.ellipsis)): const SizedBox.shrink(),
                          // ),
                        ],
                      ),
                    )
                  ],
                );
              }
            },
          ),
        ],
      ),
    );
  }
}

DataRow recentFileDataRow(dataList) {
  List countbookmember = dataList["bookmember"] as List;
  return DataRow(
    cells: [
      DataCell(
        Text(
          dataList["bookname"],
          overflow: TextOverflow.ellipsis,
        ),
      ),
      DataCell(Text(countbookmember.length.toString())),
      DataCell(Text(dataList["description"].toString())),
    ],
  );
}

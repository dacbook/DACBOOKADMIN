import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/book_member/book_member_model.dart';
import '../../../models/book_organization/book_organization_model.dart';
import '../../../models/feed/feed_model.dart';
import '../../../models/user/user_model.dart';
import '../controllers/dashboard_controller.dart';

final DashboardController dashboardController = Get.put(DashboardController());

class UserDataTable extends DataTableSource {
  List<BookMemberModel> dataList;
  String bookid;
  String userrole;
  UserDataTable(this.dataList, this.bookid, this.userrole) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.username,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.memberlevel,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.date,
            overflow: TextOverflow.ellipsis,
          ),
        ),
       
      
        DataCell(
           userrole != 'user' && userrole != 'kepala dinas' ?
          SizedBox(
          width: 200,
          child:
             Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              data.memberlevel == 'admin' ? TextButton(
                onPressed: () => dashboardController.updateBookmemberlevel(bookid: bookid, documentid: data.uid, levelname: 'anggota'),
                child: const Text('Hapus Admin', style: TextStyle(color: Colors.blue),),
                //...
              ): TextButton(
                onPressed: () => dashboardController.updateBookmemberlevel(bookid: bookid, documentid: data.uid, levelname: 'admin'),
                child: const Text('Tambah Admin', style: TextStyle(color: Colors.green),),
                //...
              ),
               TextButton(
                onPressed: () => dashboardController.deleteBookMember(bookid, data.uid),
                child: const Text('Keluarkan', style: TextStyle(color: Colors.red),),
                //...
              ),
             ],)
        ): const SizedBox.shrink()) ,
      ],
    );
  }
}

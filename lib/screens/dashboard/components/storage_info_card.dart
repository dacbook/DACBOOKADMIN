import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../constants.dart';

class StorageInfoCard extends StatelessWidget {
  const StorageInfoCard({
    Key? key,
    required this.title,
    required this.svgSrc,
    required this.amountOfFiles,
    required this.numOfFiles,
    required this.isimage,
    required this.isbook,
    required this.ismasterbook,
  }) : super(key: key);

  final String title, svgSrc, amountOfFiles;
  final int numOfFiles;
  final bool isimage;
  final bool isbook;
  final bool ismasterbook;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.only(top: defaultPadding),
        padding: const EdgeInsets.all(defaultPadding),
        decoration: BoxDecoration(
          border: Border.all(width: 2, color: primaryColor.withOpacity(0.15)),
          borderRadius: const BorderRadius.all(
            Radius.circular(defaultPadding),
          ),
        ),
        child: Row(
          children: [
            SizedBox(
              height: 20,
              width: 20,
              child: isimage == false ? SvgPicture.asset(svgSrc) : SvgPicture.asset(svgSrc),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                       ismasterbook ? const Text(
                          'Master Book',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                        ) :  const Text(
                          'Sub Book',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                       const SizedBox(width: 3,),
                        Text(
                          title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                   isbook == false ? Text(
                      "$numOfFiles Alumni",
                      style: Theme.of(context)
                          .textTheme
                          .caption,
                    ) :  Text(
                      "$numOfFiles Buku Alumni",
                      style: Theme.of(context)
                          .textTheme
                          .caption,
                    ),
                  ],
                ),
              ),
            ),
            Text(amountOfFiles,style: Theme.of(context).textTheme.bodyText1,)
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../models/dialog/dialog_utils.dart';
import '../../../models/feed/feed_model.dart';
import '../../../services/firestore_services.dart';
import '../controllers/dashboard_controller.dart';

final DashboardController dashboardController = Get.put(DashboardController());
final formKey = GlobalKey<FormState>();
TextEditingController txtcontrollerbtitle = TextEditingController();

TextEditingController txtcontrollerdeskripsi = TextEditingController();


class ForumDataTable extends DataTableSource {
  List<FeedModel> dataList;
    String userrole;
      BuildContext context;
  ForumDataTable(this.dataList, this.userrole, this.context) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.title,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.description,
            overflow: TextOverflow.ellipsis,
          ),
        ),
       
        DataCell(Text(data.date)),
        DataCell(userrole != 'user' && userrole != 'kepala dinas'
            ? SizedBox(
                width: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                   
                    IconButton(
                        splashRadius: 1.0,
                        onPressed: () {
                          debugPrint('edited');
                           // bookController.updateData(data);
                           txtcontrollerbtitle.text = data.title;
                           txtcontrollerdeskripsi.text = data.description;
                          
                             List<DialogUtilsModel>
                                                    dialogCreateMasterBuku = [
                                                  DialogUtilsModel(
                                                      icon: const Icon(
                                                        Icons.book_outlined,
                                                      ),
                                                      label: 'Title',
                                                      hint: 'update title',
                                                      textEditingController:
                                                          txtcontrollerbtitle),
                                                 
                                                  DialogUtilsModel(
                                                      icon: const Icon(
                                                        Icons.description,
                                                      ),
                                                      label: 'Deskripsi',
                                                      hint:
                                                          'update deskripsi',
                                                      textEditingController:
                                                          txtcontrollerdeskripsi),
                                                ];
                             updateGaleryDialog(
                                                  context: context,
                                                  formKey: formKey,
                                                  buttonLabel: 'Update',
                                                  title: 'Edit Master Buku',
                                                  dialogUtilsModel:
                                                      dialogCreateMasterBuku,
                                                      feeddata: data
                                                    
                                                );
                        },
                        icon: const Icon(
                          Icons.edit,
                          color: Colors.blue,
                        )),
                    IconButton(
                        splashRadius: 1.0,
                        onPressed: () {
                          dashboardController.deleteBookForum(data.id);
                        },
                        icon: const Icon(
                          Icons.delete,
                          color: Colors.blue,
                        ))
                  ],
                )
                // TextButton(
                //   onPressed: () =>
                //       dashboardController.deleteBookGalery(data.id),
                //   child: const Text('Hapus'),
                //   //...
                // ),
                )
            : const SizedBox.shrink()),
      
       
      ],
    );
  }
}


updateGaleryDialog({
  required BuildContext context,
  required GlobalKey<FormState> formKey,
  required String title,
  required String buttonLabel,
  required List<DialogUtilsModel> dialogUtilsModel,
  required FeedModel feeddata
}) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          content: Stack(
            clipBehavior: Clip.none,
            children: <Widget>[
              Positioned(
                right: -10.0,
                top: -10.0,
                child: InkResponse(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: const Icon(Icons.close)),
              ),
              SizedBox(
                width: 450,
                child: Form(
                  key: formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        title,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      for (var dialogModel in dialogUtilsModel)
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                              controller: dialogModel.textEditingController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "cannot be empty";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  isDense: false,
                                  labelText: dialogModel.label,
                                  hintText: dialogModel.hint,
                                  prefixIcon: dialogModel.icon)),
                        ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GFButton(
                            size: GFSize.SMALL,
                            text: buttonLabel,
                            textStyle: const TextStyle(
                                fontSize: 16, color: GFColors.WHITE),
                            // icon: const Icon(
                            //   Icons.check,
                            // ),
                            color: Colors.red,
                            blockButton: true,
                            onPressed: ()
                                //=>onPressed
                                {
                              if (formKey.currentState!.validate()) {
                                formKey.currentState!.save();
                                print('on pressed validation');

                                _onUpdateMasterBook(feeddata);
                              } else {
                                print('on pressed no validation');
                              }
                              Navigator.of(context).pop();
                            }),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      });
}

_onUpdateMasterBook(FeedModel feeddata) async {
  if (txtcontrollerbtitle.value.text.isNotEmpty &&
      txtcontrollerdeskripsi.value.text.isNotEmpty) {
    print('update masterbook proses..');
    Map<String, dynamic> datamap = {
      'title': txtcontrollerbtitle.text,
      'description': txtcontrollerdeskripsi.text,
    };

    txtcontrollerdeskripsi.clear();
    txtcontrollerbtitle.clear();

    await FireStoreDataBase()
        .updateBookForum(documentId: feeddata.id, object: datamap);
    debugPrint('success to update');
  } else {
    //null;
    print('create masterbook invalid..');
  }
}


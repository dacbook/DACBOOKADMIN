import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/models/user/user_model.dart';
import 'package:dacweb/screens/main/controller/main_controller.dart';
import 'package:dacweb/screens/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../../models/book_member/book_member_model.dart';
import '../controllers/dashboard_controller.dart';
import 'datatable_user.dart';

class DashboardDatauserTable extends StatefulWidget {
  const DashboardDatauserTable({super.key, required this.bookmasterid});
  final String bookmasterid;
  @override
  State<DashboardDatauserTable> createState() => _DashboardDatauserTableState();
}

class _DashboardDatauserTableState extends State<DashboardDatauserTable> {
  final db = FirebaseFirestore.instance;
  final MainController mainController = Get.put(MainController());
  String? userrole;
  @override
  void initState() {
    super.initState();
    userrole = mainController.usermodel.value!.userRole;
    debugPrint('is user role data $userrole');
    // filteredList = dataList;
  }


  @override
  Widget build(BuildContext context) {
    List<BookMemberModel> listData = [];
    final DashboardController dashboardController =
        Get.put(DashboardController());
    final MainController mainController =
        Get.put(MainController());
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder<QuerySnapshot>(
            stream: db
                .collection('book')
                .doc(widget.bookmasterid)
                .collection('bookmember')
                .snapshots(),
            builder: (context, snapshot) {
              debugPrint(
                  'is data book user : ${widget.bookmasterid} - ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasError ||
                  !snapshot.hasData ||
                  snapshot.data == [] ||
                  snapshot.data == null) {
                return SizedBox(
                  height: Get.height * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Tidak ada data alumni buku',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.grey[600]),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                );
              } else {
                listData = snapshot.data!.docs
                    .map(
                      (DocumentSnapshot doc) => BookMemberModel.fromJson(
                          doc.data() as Map<String, dynamic>, doc.id),
                    )
                    .toList();
                debugPrint('data : $listData');

                 userrole = mainController.usermodel.value!.userRole;

                UserDataTable dataSource =
                    UserDataTable(listData, widget.bookmasterid,userrole! );

                return
                Column(
                  children: [
                    Text(
                      "Data Alumni",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: PaginatedDataTable(
                        source: dataSource,
                        columnSpacing: 10,
                        horizontalMargin: 10,
                        rowsPerPage: 10,
                        showCheckboxColumn: false,
                        //minWidth: 700,
                        columns:  [
                        const  DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Nama",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                        const  DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Role",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                        const  DataColumn(
                            label: SizedBox(
                                width: 180,
                                child: Text(
                                  "Tanggal bergabung",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                        DataColumn(
                            label: userrole != 'user' && userrole != 'kepala dinas' ? const  SizedBox(
                                width: 100,
                                child: Text("Aksi",
                                    overflow: TextOverflow.ellipsis)) : const SizedBox.shrink(),
                          ),
                        ],
                      ),
                    )
                  ],
                )
               ;
              }
            },
          ),
        ],
      ),
    );
  }
}

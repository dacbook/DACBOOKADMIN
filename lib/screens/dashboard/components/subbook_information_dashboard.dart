import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dacweb/responsive.dart';
import 'package:dacweb/screens/dashboard/components/datatable_forum.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';
import '../../../data/local/box_storage.dart';
import '../../../models/book/book_model.dart';
import '../../../models/myfiles.dart';
import '../../../models/user/user_model.dart';
import '../../data_buku/controller/book_controller.dart';
import '../controllers/dashboard_controller.dart';
import 'chart.dart';
import 'dashboard_dataagenda_table.dart';
import 'dashboard_dataforum_table.dart';
import 'dashboard_datagalery_table.dart';
import 'dashboard_dataorganization_table.dart';
import 'dashboard_datauser_table.dart';
import 'file_info_card.dart';
import 'databook_dashboard.dart';

final db = FirebaseFirestore.instance;

class SubBookInformationDashboard extends StatefulWidget {
  const SubBookInformationDashboard({super.key,});
  @override
  State<SubBookInformationDashboard> createState() =>
      _SubBookInformationDashboardState();
}

class _SubBookInformationDashboardState
    extends State<SubBookInformationDashboard> {
  //String? bookmasterid;
  String? kepsekusername;
  String? kepsekavatar;
  String? kepsekuid;
  bool isdatabookonly = false;
  bool isdatastrukturonly = false;
  bool isdatagaleryonly = false;
  bool isdataforumonly = false;
  bool isdataagendaonly = false;
  bool iskepsekmode = false;

  List bookmembers = [];

  String? selectedValue;
  String? kepsekselectedValue;
  final TextEditingController textEditingController = TextEditingController();
  final TextEditingController kepsekEditingController = TextEditingController();

  final BookController bookController = Get.put(BookController());

  final BoxStorage boxStorage = BoxStorage();
  String? localid;
  String? bookmastername;

  @override
  void initState() {
    super.initState();
    setState(() {
      localid = boxStorage.getUserId();
    //  bookmasterid = widget.bookid;
      dashboardController.isactived.value = true;
      dashboardController.isdatauseronly.value = true;
      dashboardController.isdatabookonly.value = false;
      dashboardController.isdataagendaonly.value = false;
      dashboardController.isdataforumonly.value = false;
      dashboardController.isdatagaleryonly.value = false;
      dashboardController.isdatastrukturonly.value = false;
      debugPrint(
          'is databook only ${dashboardController.isdatabookonly.value}');
    });
  }

  bool isactived = false;
  bool iskepsekActived = false;

  refreshData() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return StreamBuilder(
        stream: db.collection('user').doc(localid).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          var doc = snapshot.data!.data();
          UserModel userModel = UserModel.fromJson(doc!);
          debugPrint('book master id : ${userModel.bookmasterkepsek}');
          if (userModel.bookmasterkepsek != '') {
          //  bookmasterid = userModel.bookmasterkepsek;
            iskepsekmode = true;
          //  selectedValue = bookmasterid;
            dashboardController.isactived.value = true;
            dashboardController.isdatauseronly.value = true;
            dashboardController.isdatabookonly.value = false;
            dashboardController.isdataagendaonly.value = false;
            dashboardController.isdataforumonly.value = false;
            dashboardController.isdatagaleryonly.value = false;
            dashboardController.isdatastrukturonly.value = false;
            debugPrint(
                'is databook only  $iskepsekmode ${dashboardController.isdatabookonly.value}');
          }

          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  bookController.isdetailbook.value == true
                      ? const SizedBox(
                          width: 100,
                        )
                      : StreamBuilder(
                          stream: iskepsekmode == true
                              ? db
                                  .collection('book')
                                  .where('bookmasterid',
                                      isEqualTo: bookController.bookmasteridlocal.value)
                                  .snapshots()
                              : db
                                  .collection('book')
                                  .where('ismasterbook', isEqualTo: true)
                                  .snapshots(),
                          builder: (context, datasnap) {
                            if (!datasnap.hasData) {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            }

                            return Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    children: [
                                      const Text(
                                        'Buku',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      const SizedBox(
                                        width: 4,
                                      ),
                                      if (iskepsekmode == true)
                                        Obx(
                                          ()=> StreamBuilder(
                                              stream: db
                                                  .collection('book')
                                                  .doc(bookController.bookmasteridlocal.value)
                                                  .snapshots(),
                                              builder: (context, streambook) {
                                                if (!streambook.hasData) {
                                                  return const Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  );
                                                }
                                                var doc = streambook.data!.data();
                                                bookmastername = doc!['bookname'];
                                                bookmembers = doc['bookmember'];
                                                int count = bookmembers.length;
                                        
                                                debugPrint(
                                                    'book master name : $bookmastername - ${bookmembers.length}');
                                        
                                                return SizedBox(
                                                    child: Text(
                                                  bookmastername!,
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ));
                                              }),
                                        )
                                    ],
                                  ),
                                  
                                ],
                              ),
                            );
                          }),
                  Row(
                    children: [
                      bookController.bookmasteridlocal.value == ''
                          ? const SizedBox.shrink()
                          : iskepsekmode == true
                              ? const SizedBox.shrink()
                              : ElevatedButton.icon(
                                  style: TextButton.styleFrom(
                                    backgroundColor: iskepsekActived == false
                                        ? Colors.green
                                        : Colors.red,
                                    padding: EdgeInsets.symmetric(
                                      horizontal: defaultPadding * 1.5,
                                      vertical: defaultPadding /
                                          (Responsive.isMobile(context)
                                              ? 2
                                              : 1),
                                    ),
                                  ),
                                  onPressed: () {
                                    if (iskepsekActived == false) {
                                      setState(() {
                                        iskepsekActived = true;
                                      });
                                    } else {
                                      setState(() {
                                        iskepsekActived = false;
                                      });
                                    }
                                  },
                                  icon: iskepsekActived == false
                                      ? const Icon(Icons.add)
                                      : const Icon(Icons.change_circle),
                                  label: const Text("Tambah Kepala Sekolah"),
                                ),
                      const SizedBox(
                        width: 20,
                      ),
                     
                    ],
                  ),
                ],
              ),
              const SizedBox(height: defaultPadding),
              iskepsekActived == false
                  ? const SizedBox(
                      width: 100,
                    )
                  : StreamBuilder<QuerySnapshot>(
                      stream: db
                          .collection('user')
                          .where('userRole', isEqualTo: 'kepala sekolah')
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }

                        return Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Text(
                                'Kepala Sekolah',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              DropdownButtonHideUnderline(
                                child: DropdownButton2<String>(
                                  isExpanded: true,
                                  hint: Text(
                                    'Tentukan Kepala Sekolah',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Theme.of(context).hintColor,
                                    ),
                                  ),
                                  items: snapshot.data!.docs
                                      .map((DocumentSnapshot doc) {
                                    return DropdownMenuItem<String>(
                                        onTap: () {
                                          // List bookmembers = doc['bookmember'];
                                          // dashboardController.populatealumni
                                          //     .value = bookmembers.length;
                                          kepsekusername = doc['username'];
                                          kepsekuid = doc['uid'];
                                          kepsekavatar = doc['avatar'];
                                          debugPrint(
                                              'kepsek : $kepsekusername');
                                        },
                                        value: doc['username'],
                                        child: Text(doc['username']));
                                  }).toList(),
                                  value: kepsekselectedValue,
                                  onChanged: (value) {
                                    setState(() {
                                      kepsekselectedValue = value as String;
                                    });
                                  },
                                  buttonStyleData: const ButtonStyleData(
                                    height: 40,
                                    width: 200,
                                  ),
                                  dropdownStyleData: const DropdownStyleData(
                                    maxHeight: 200,
                                  ),
                                  menuItemStyleData: const MenuItemStyleData(
                                    height: 40,
                                  ),
                                  dropdownSearchData: DropdownSearchData(
                                    searchController: kepsekEditingController,
                                    searchInnerWidgetHeight: 50,
                                    searchInnerWidget: Container(
                                      height: 50,
                                      padding: const EdgeInsets.only(
                                        top: 8,
                                        bottom: 4,
                                        right: 8,
                                        left: 8,
                                      ),
                                      child: TextFormField(
                                        expands: true,
                                        maxLines: null,
                                        controller: kepsekEditingController,
                                        decoration: InputDecoration(
                                          isDense: true,
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                            horizontal: 10,
                                            vertical: 8,
                                          ),
                                          hintText: 'Search for an  kepsek...',
                                          hintStyle:
                                              const TextStyle(fontSize: 12),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                        ),
                                      ),
                                    ),
                                    searchMatchFn: (item, searchValue) {
                                      return (item.value
                                          .toString()
                                          .toLowerCase()
                                          .contains(searchValue));
                                    },
                                  ),
                                  //This to clear the search value when you close the menu
                                  onMenuStateChange: (isOpen) {
                                    if (!isOpen) {
                                      kepsekEditingController.clear();
                                    }
                                  },
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              ElevatedButton.icon(
                                style: TextButton.styleFrom(
                                  backgroundColor: Colors.green,
                                  padding: EdgeInsets.symmetric(
                                    horizontal: defaultPadding * 1.5,
                                    vertical: defaultPadding /
                                        (Responsive.isMobile(context) ? 2 : 1),
                                  ),
                                ),
                                onPressed: () {
                                  if (kepsekuid != null &&
                                      bookController.bookmasteridlocal.value != '') {
                                    String localuid = kepsekuid!;
                                    String username = kepsekusername!;
                                    String avatar = kepsekavatar!;
                                    final time = DateFormat('hh:mm');
                                    final date = DateFormat('dd-MM-yyyy');
                                    db
                                        .collection('book')
                                        .doc(bookController.bookmasteridlocal.value)
                                        .update({
                                      'bookmember':
                                          FieldValue.arrayUnion([localuid]),
                                      'totalmember': FieldValue.increment(1)
                                    });
                                    db
                                        .collection('book')
                                        .doc(bookController.bookmasteridlocal.value)
                                        .collection('bookmember')
                                        .doc(localuid)
                                        .set({
                                      'bookmember': localuid,
                                      'date': date.format(DateTime.now()),
                                      'time': time.format(DateTime.now()),
                                      'username': username,
                                      'avatar': avatar,
                                      'uid': localuid,
                                      'memberlevel': 'kepala sekolah',
                                    });
                                    Get.snackbar('Informasi',
                                        "Selamat berhasil ditambahkan");
                                    setState(() {
                                      kepsekuid = null;
                                      iskepsekActived = false;
                                      dashboardController.isactived.value =
                                          true;
                                      dashboardController.isdatauseronly.value =
                                          true;
                                      dashboardController.isdatabookonly.value =
                                          false;
                                      dashboardController
                                          .isdataagendaonly.value = false;
                                      dashboardController
                                          .isdataforumonly.value = false;
                                      dashboardController
                                          .isdatagaleryonly.value = false;
                                      dashboardController
                                          .isdatastrukturonly.value = false;
                                      debugPrint(
                                          'is databook only ${dashboardController.isdatabookonly.value}');
                                    });
                                  } else {
                                    Get.snackbar('Informasi',
                                        "Gagal menambahkan kepala sekolah, tentukan masterbook / kepala sekolah");
                                  }
                                },
                                icon: const Icon(Icons.check),
                                label: const Text("Simpan"),
                              ),
                            ],
                          ),
                        );
                      }),
              const SizedBox(height: defaultPadding),
              const Text(
                "Populasi Buku",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: defaultPadding),
              if (iskepsekmode == true)
                Obx(
                  ()=> Chart(
                    bookmasterid:bookController.bookmasteridlocal.value,
                  ),
                ),
              if (iskepsekmode == false)
                bookController.bookmasteridlocal.value == ''
                    ? const SizedBox.shrink()
                    : Chart(
                        bookmasterid: bookController.bookmasteridlocal.value,
                      ),
                       const SizedBox(height: defaultPadding),
                bookController.bookmasteridlocal.value == ''
                  ? const SizedBox.shrink():  ElevatedButton.icon(
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: defaultPadding * 1.5,
                            vertical: defaultPadding /
                                (Responsive.isMobile(context) ? 2 : 1),
                          ),
                        ),
                        onPressed: () {
                          dashboardController.selectedOnSubBookMenus(4);
                        },
                        icon: const Icon(Icons.verified_user),
                        label: const Text("Alumni"),
                      ),
              const SizedBox(height: defaultPadding),
              
              Responsive(
                mobile: FileInfoCardGridView(
                  crossAxisCount: size.width < 650 ? 2 : 4,
                  childAspectRatio:
                      size.width < 650 && size.width > 350 ? 1.3 : 1,
                ),
                tablet: FileInfoCardGridView(
                  isactivedcard: isactived,
                ),
                desktop: FileInfoCardGridView(
                  isactivedcard: isactived,
                  crossAxisCount: 5,
                  childAspectRatio: size.width < 1400 ? 4 : 3.5,
                ),
              ),
              bookController.bookmasteridlocal.value == ''
                  ? SizedBox(
                      height: Get.height * 0.4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Tidak ada data buku',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.grey[600]),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                        ],
                      ),
                    )
                  : Obx(
                      () => Column(
                        children: [
                          if (dashboardController.isdatauseronly.value == true)
                            DashboardDatauserTable(
                              bookmasterid: bookController.bookmasteridlocal.value,
                            ),
                          // if (dashboardController.isdatabookonly.value == true)
                          //   DatabookDashboard(
                          //     bookmasterid: bookmasterid!,
                          //   ),
                          if (dashboardController.isdataforumonly.value == true)
                            DashboardDataforumTable(
                              bookmasterid: bookController.bookmasteridlocal.value,
                            ),
                          if (dashboardController.isdatagaleryonly.value ==
                              true)
                            DashboardDatagaleryTable(
                              bookmasterid: bookController.bookmasteridlocal.value,
                            ),
                          if (dashboardController.isdatastrukturonly.value ==
                              true)
                            DashboardDataorganizationTable(
                              bookmasterid: bookController.bookmasteridlocal.value,
                            ),
                          if (dashboardController.isdataagendaonly.value ==
                              true)
                            DashboardDataagendaTable(
                              bookmasterid: bookController.bookmasteridlocal.value,
                            ),
                        ],
                      ),
                    )
            ],
          );
        });
  }
}

class FileInfoCardGridView extends StatelessWidget {
  const FileInfoCardGridView(
      {Key? key,
      this.crossAxisCount = 4,
      this.childAspectRatio = 1,
      this.isactivedcard = false})
      : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;
  final bool isactivedcard;

  @override
  Widget build(BuildContext context) {
    final DashboardController dashboardController =
        Get.put(DashboardController());
    return GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: isactivedcard == true
            ? subbookfiles.length
            : subbookInActiveCard.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          crossAxisSpacing: defaultPadding,
          mainAxisSpacing: defaultPadding,
          childAspectRatio: childAspectRatio,
        ),
        itemBuilder: (context, index) {
          return Obx(() => dashboardController.isactived.value == true
              ? GestureDetector(
                  onTap: () => dashboardController.selectedOnSubBookMenus(index),
                  child: FileInfoCard(info: subbookfiles[index]))
              : FileInfoCard(info: subbookInActiveCard[index]));
        });
  }
}

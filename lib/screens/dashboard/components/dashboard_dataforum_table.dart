import 'package:dacweb/models/feed/feed_model.dart';
import 'package:dacweb/screens/dashboard/components/datatable_forum.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants.dart';
import '../../main/controller/main_controller.dart';
import '../controllers/dashboard_controller.dart';

class DashboardDataforumTable extends StatefulWidget {
  const DashboardDataforumTable({super.key, required this.bookmasterid});
  final String bookmasterid;
  @override
  State<DashboardDataforumTable> createState() =>
      _DashboardDataforumTableState();
}

class _DashboardDataforumTableState extends State<DashboardDataforumTable> {
  final MainController mainController = Get.put(MainController());
    String? userrole;
  @override
  Widget build(BuildContext context) {
    List<FeedModel> listData = [];
    final DashboardController dashboardController =
        Get.put(DashboardController());
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder(
            stream: dashboardController.getBookForum(widget.bookmasterid),
            builder: (context, snapshot) {
              debugPrint('is data book ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasError ||
                  !snapshot.hasData ||
                  snapshot.data == [] ||
                  snapshot.data == null) {
                return SizedBox(
                  height: Get.height * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Tidak ada data buku',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.grey[600]),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                );
              } else {
                listData = snapshot.data!;
                 userrole = mainController.usermodel.value!.userRole;

                ForumDataTable dataSource = ForumDataTable(listData, userrole!, context);

                return Column(
                  children: [
                    Text(
                      "Data Forum",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: PaginatedDataTable(
                        source: dataSource,
                        columnSpacing: 10,
                        horizontalMargin: 10,
                        rowsPerPage: 10,
                        showCheckboxColumn: false,
                        //minWidth: 700,
                        columns:  [
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Title",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text(
                                  "Deskripsi forum",
                                  overflow: TextOverflow.ellipsis,
                                )),
                          ),
                          const DataColumn(
                            label: SizedBox(
                                width: 100,
                                child: Text("Tanggal",
                                    overflow: TextOverflow.ellipsis)),
                          ),
                          DataColumn(
                            label:  userrole != 'user' && userrole != 'kepala dinas'? const SizedBox(
                                width: 100,
                                child: Text("Aksi",
                                    overflow: TextOverflow.ellipsis)): const SizedBox.shrink(),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }
            },
          ),
        ],
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/book/book_model.dart';
import '../../data_buku/controller/book_controller.dart';

final BookController bookController = Get.put(BookController());

class BookDataTable extends DataTableSource {
  List<BookModel> dataList;
  String userrole;
  BookDataTable(this.dataList, this.userrole) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    List countbookmember = data.bookmember;
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.bookname,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.pin,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(data.ismasterbook == true
            ? const Text(
                'Master Buku',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              )
            : const Text(
                'Sub Buku',
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              )),
        DataCell(Text(countbookmember.length.toString())),
        DataCell(Text(data.description.toString())),
        // DataCell(userrole != 'user' && userrole != 'kepala dinas'
        //     ? SizedBox(
        //         width: 150,
        //         child: Row(
        //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //             children: [
        //               // IconButton(
        //               //     icon: const Icon(
        //               //       Icons.book,
        //               //       color: Colors.blue,
        //               //     ),
        //               //     tooltip: 'Detail',
        //               //     onPressed: () {
        //               //      bookController.isdetailbook.value = true;
        //               //      bookController.bookmasteridlocal.value = data.id;
        //               //     }),
        //               IconButton(
        //                   icon: const Icon(
        //                     Icons.edit,
        //                     color: Colors.blue,
        //                   ),
        //                   tooltip: 'Edit Book',
        //                   onPressed: () {
        //                     bookController.updateData(data.bookname, data.pin,
        //                         data.description, data.id);
        //                   }),
        //               TextButton(
        //                 onPressed: () => bookController.deletebook(data.id),
        //                 child: const Text('Hapus'),
        //                 //...
        //               ),
        //             ]),
        //       )
        //     : const SizedBox.shrink()),
      ],
    );
  }
}

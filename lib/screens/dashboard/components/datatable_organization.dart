import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../gen/colors.gen.dart';
import '../../../models/book_organization/book_organization_model.dart';
import '../../../models/book_position/book_position_rank_model.dart';
import '../controllers/dashboard_controller.dart';

final DashboardController dashboardController = Get.put(DashboardController());

class OrganizationDataTable extends DataTableSource {
  List<BookOrganizationModel> dataList;
  String bookid;
  BuildContext context;
  OrganizationDataTable(this.dataList, this.bookid, this.context) {
    //print(dataList);
  }
  // Generate some made-up data

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => dataList.length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    var data = dataList[index];
    return DataRow(
      cells: [
        DataCell(
          Text(
            data.username,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.positionlevel,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(
          Text(
            data.positionname,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        DataCell(SizedBox(
          width: 100,
          child: TextButton(
            onPressed: () {
              changePositionUser(context, data );
            },
            child: const Text('Edit'),
            //...
          ),
        )),
      ],
    );
  }
  String positionSelected = '';

  void changePositionUser(context, BookOrganizationModel data) {
    showModalBottomSheet(
        //  isScrollControlled: true,
         constraints: BoxConstraints(
     maxWidth: Get.width * .3,              
  ),
        context: context,
        builder: (context) {
          return StreamBuilder(
              stream: dashboardController.getPositionRank(bookid),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (snapshot.data.length == 0 || snapshot.data == null) {
                  return SizedBox(
                      height: Get.height * 0.6,
                      width: Get.width * 0.6,
                      child: const Center(
                          child: Text(
                        'Posisi organisasi buku masih kosong \n\n',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: ColorName.redprimary),
                      )));
                } else {
                  return Container(
                    padding: const EdgeInsets.all(8),
                    height: 200,
                    //  height: MediaQuery.of(context).viewInsets.bottom,
                    alignment: Alignment.center,
                    child: ListView.builder(
                        itemCount:
                            snapshot.data.isNotEmpty ? snapshot.data.length : 0,
                        itemBuilder: (context, index) {
                          BookPositionRank positionRanks = snapshot.data[index];
                          debugPrint('rank : ${snapshot.data.isNotEmpty}');
                          return SizedBox(
                            height: 50,
                            child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Lv. ${positionRanks.positionLevel} - ${positionRanks.positionName}'
                                          .toUpperCase(),
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          dashboardController
                                              .updatepositionmember(
                                                  bookid,
                                                  data.uid,
                                                  positionRanks.positionName, positionRanks.positionLevel);
                                          Navigator.pop(context);
                                        },
                                        child: const Text('Pilih posisi'))
                                  ],
                                ),
                                onTap: () {
                                 positionSelected =
                                        positionRanks.positionName;
                                    debugPrint('posisi : $positionSelected');
                               //   Navigator.of(context).pop();
                                }),
                          );
                        }),
                  );
                }
              });
        });
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/dashboard_controller.dart';

class CreatePositionOrganization extends StatefulWidget {
  const CreatePositionOrganization({super.key, required this.isupdate, required this.bookid});
  final bool isupdate;
  final String bookid;
  @override
  State<CreatePositionOrganization> createState() =>
      _CreatePositionOrganizationState();
}

class _CreatePositionOrganizationState
    extends State<CreatePositionOrganization> {
  final _formKey = GlobalKey<FormState>();
  final DashboardController dashboardController = Get.put(DashboardController());
  bool isloading = false;

   String? positionName;
  String? positionLevel;
  @override
  Widget build(BuildContext context) {
    return Container(
      // constraints: const BoxConstraints(maxWidth: 21),
      height: 380,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          TextFormField(
            controller: dashboardController.txtcontrollerpositionlevel.value,
            keyboardType: TextInputType.number,
            maxLength: 1,
            //  autofillHints: const [AutofillHints.email],
            textInputAction: TextInputAction.next,
            onSaved: (String? value) {},
            onChanged: (value) =>
                dashboardController.txtcontrollerpositionlevel.value,
            validator: (value) {
              if (value!.isEmpty) {
                return "poisition lv cannot be empty";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'masukan level posisi',
              labelText: '1,2,3,4,5',
            ),
          ),
          TextFormField(
            controller: dashboardController.txtcontrollerpositionname.value,
            keyboardType: TextInputType.text,
            //  autofillHints: const [AutofillHints.email],
            textInputAction: TextInputAction.next,
            onSaved: (String? value) {},
            onChanged: (value) =>
                dashboardController.txtcontrollerpositionname.value,
            validator: (value) {
              if (value!.isEmpty) {
                return "position name cannot be empty";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'masukan nama posisi',
              labelText: 'cto',
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          isloading == false
              ? TextButton(
                  onPressed: () {
                    Future.delayed(const Duration(seconds: 1), () {
                      // <-- Delay here
                      if (_formKey.currentState!.validate()) {
                        setState(() {
                          isloading = true;
                        });
                      

                        if (widget.isupdate) {
                        dashboardController.createPositionRank(
                            positionLevel: positionLevel == null
                                ? dashboardController.txtcontrollerpositionlevel.value.text
                                : positionLevel!,
                            positionName: positionName == null
                                ? dashboardController.txtcontrollerpositionname.value.text
                                : positionName!,
                            bookid: widget.bookid,
                            id: dashboardController.docid.value,
                            isupdate: widget.isupdate);
                      } else {
                        dashboardController.createPositionRank(
                            positionLevel: positionLevel!,
                            positionName: positionName!,
                            bookid: widget.bookid,
                             id: widget.bookid,
                            isupdate: widget.isupdate);
                      }

                        setState(() {
                          dashboardController.txtcontrollerpositionname.value.clear();
                          dashboardController.txtcontrollerpositionlevel.value.clear();
                          dashboardController.docid.close();
                          isloading == false;
                        });
                      } else {
                        Get.snackbar('Login Proses', 'login in progress');
                        debugPrint('login in progress..');
                        setState(() {
                          isloading = false;
                          dashboardController.isupdateorganization.value = false;
                        });
                      }
                    });
                  },
                  child:widget.isupdate == true
                      ? const Text('ubah')
                      : const Text('simpan'),
                  //...
                )
              : const Center(
                  child: SizedBox(
                      height: 30,
                      width: 30,
                      child: CircularProgressIndicator()),
                ),
          const SizedBox(
            height: 30,
          )
        ]),
      ),
    );
  }
}

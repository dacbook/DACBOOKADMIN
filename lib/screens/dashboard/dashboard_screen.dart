import 'package:dacweb/models/user/user_model.dart';
import 'package:dacweb/responsive.dart';
import 'package:dacweb/screens/dashboard/components/masterbook_information_dashboard.dart';
import 'package:dacweb/screens/dashboard/controllers/dashboard_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import '../../services/firebase_auth.dart';
import '../data_buku/controller/book_controller.dart';
import 'components/header.dart';

import 'components/databook_dashboard.dart';
import 'components/storage_details.dart';

// ignore: must_be_immutable
class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final FirebaseAuthServices firebaseAuthServices = FirebaseAuthServices();
  final BookController bookController = Get.put(BookController());
  String dropdownValue = 'Dog';
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: firebaseAuthServices.loginUser(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Center(
            child: Text(
              "Something went wrong",
            ),
          );
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return SafeArea(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(defaultPadding),
              child: StreamBuilder<Object>(
                  stream: null,
                  builder: (context, snapshot) {
                    return Column(
                      children: [
                        //  const Header(
                        //   ),
                        const SizedBox(height: defaultPadding),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 5,
                              child: Column(
                                children: [
                                  const MasterBookInformationDashboard(),
                                  const SizedBox(height: defaultPadding),
                                  if (Responsive.isMobile(context))
                                    const SizedBox(height: defaultPadding),
                                  if (Responsive.isMobile(context))
                                    const StorageDetails(),
                                ],
                              ),
                            ),
                            if (!Responsive.isMobile(context))
                              const SizedBox(width: defaultPadding),
                            // On Mobile means if the screen is less than 850 we dont want to show it
                            // if (!Responsive.isMobile(context))
                            //   const Expanded(
                            //     flex: 2,
                            //     child: StorageDetails(),
                            //   ),
                          ],
                        )
                      ],
                    );
                  }),
            ),
          );
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}

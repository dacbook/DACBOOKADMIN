import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../models/dialog/dialog_utils.dart';

class AlertDialogDACController extends GetxController {
  onDialogOpened({
    required BuildContext context,
    required String title,
    required String description,
  }) {
    Alert(
      context: context,
      style: alertStyle,
      type: AlertType.success,
      title: title,
      desc: description,
      buttons: [],
    ).show();
  }

  onDialogForm({
    required BuildContext screenContext,
    required BuildContext context,
  }) {
    Alert(
        context: context,
        title: "LOGIN",
        content: Column(
          children: const <Widget>[
            TextField(
              decoration: InputDecoration(
                icon: Icon(Icons.account_circle),
                labelText: 'Username',
              ),
            ),
            TextField(
              obscureText: true,
              decoration: InputDecoration(
                icon: Icon(Icons.lock),
                labelText: 'Password',
              ),
            ),
          ],
        ),
        buttons: [
          DialogButton(
            onPressed: () {
              Navigator.pop(screenContext);
            },
            child: const Text(
              "LOGIN",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
        ]).show();
  }

  var alertStyle = AlertStyle(
      isButtonVisible: false,
      animationType: AnimationType.fromTop,
      isCloseButton: true,
      isOverlayTapDismiss: true,
      descStyle: const TextStyle(fontWeight: FontWeight.w300, fontSize: 14),
      animationDuration: const Duration(milliseconds: 800),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: const BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: const TextStyle(
        color: Colors.red,
      ),
      constraints: const BoxConstraints.expand(width: 500),
      //First to chars "55" represents transparency of color
      overlayColor: const Color(0x55000000),
      alertElevation: 0,
      alertAlignment: Alignment.topCenter);

  defaultDialog(
      {required BuildContext context,
      required GlobalKey<FormState> formKey,
      required String title,
      required String buttonLabel,
      required List<DialogUtilsModel> dialogUtilsModel,
      required void Function()? onPressed
      }) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            content: Stack(
              clipBehavior: Clip.none,
              children: <Widget>[
                Positioned(
                  right: -15.0,
                  top: -15.0,
                  child: InkResponse(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: const Icon(Icons.close)),
                ),
                SizedBox(
                  width: 300,
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          title,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        for (var dialogModel in dialogUtilsModel)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                                controller: dialogModel.textEditingController,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "cannot be empty";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    isDense: false,
                                    labelText: dialogModel.label,
                                    hintText: dialogModel.hint,
                                    prefixIcon: dialogModel.icon)),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GFButton(
                              size: GFSize.SMALL,
                              text: buttonLabel,
                              textStyle: const TextStyle(
                                  fontSize: 16, color: GFColors.WHITE),
                              // icon: const Icon(
                              //   Icons.check,
                              // ),
                              color: Colors.red,
                              blockButton: true,
                              onPressed:() 
                              //=>onPressed
                             {
                                if (formKey.currentState!.validate()) {
                                  formKey.currentState!.save();
                                  print('on pressed validation');
                                 
                                 onPressed;
                             
                                }else{

                                  print('on pressed no validation');
                                }
                               Navigator.of(context).pop();
                             }
                              ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}

import 'package:get/get.dart';
import 'package:pod_player/pod_player.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CustomVideoControlls extends StatefulWidget {
  const CustomVideoControlls({Key? key, required this.videoUrl})
      : super(key: key);
  final String videoUrl;

  @override
  State<CustomVideoControlls> createState() => _CustomVideoControllsState();
}

class _CustomVideoControllsState extends State<CustomVideoControlls> {
  late PodPlayerController controller;
  bool? isVideoPlaying;
  // final videoTextFieldCtr = TextEditingController(
  //   text:
  //       'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4',
  // );
  // final vimeoTextFieldCtr = TextEditingController(
  //   text: '518228118',
  // );
  // final youtubeTextFieldCtr = TextEditingController(
  //   text: 'https://youtu.be/A3ltMaM6noM',
  // );

  bool alwaysShowProgressBar = true;
  @override
  void initState() {
    super.initState();
    controller = PodPlayerController(
      playVideoFrom: PlayVideoFrom.network(widget.videoUrl),
      // fromAssets: 'assets/long_video.mkv',
      // fromAssets: 'assets/SampleVideo_720x480_20mb.mp4',
      // fromNetworkUrl:
      // 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4',
      // 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4',
      // 'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4',
      // 'https://user-images.githubusercontent.com/85326522/140480457-ab21345a-76e2-4b0e-b4ec-027c89f0e712.mp4',
      // 'http://techslides.com/demos/sample-videos/small.mp4',
      // fromVimeoVideoId: '518228118',
    )..initialise().then((value) {
        setState(() {
          isVideoPlaying = controller.isVideoPlaying;
        });
      });
    controller.addListener(_listner);
  }

  ///Listnes to changes in video
  void _listner() {
    if (controller.isVideoPlaying != isVideoPlaying) {
      isVideoPlaying = controller.isVideoPlaying;
    }
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    controller.removeListener(_listner);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ///
    const sizeH20 = SizedBox(height: 20);
    final totalHour = controller.currentVideoPosition.inHours == 0
        ? '0'
        : '${controller.currentVideoPosition.inHours}:';
    final totalMinute =
        controller.currentVideoPosition.toString().split(':')[1];
    final totalSeconds = (controller.currentVideoPosition -
            Duration(minutes: controller.currentVideoPosition.inMinutes))
        .inSeconds
        .toString()
        .padLeft(2, '0');

    ///
    const videoTitle = Padding(
      padding: kIsWeb
          ? EdgeInsets.symmetric(vertical: 25, horizontal: 15)
          : EdgeInsets.only(left: 15),
      child: Text(
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        style: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
    const textStyle = TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
    );
    return Scaffold(
      backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        ),
        body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: Get.height,
                width: Get.width,
                child: PodVideoPlayer(
                  alwaysShowProgressBar: alwaysShowProgressBar,
                  controller: controller,
                  matchFrameAspectRatioToVideo: true,
                  matchVideoAspectRatioToFrame: true,
                  videoTitle: videoTitle,
                ),
              ),
            )));
  }

  void snackBar(String text) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(text),
        ),
      );
  }

  ElevatedButton _iconButton(String text, IconData icon,
      {void Function()? onPressed}) {
    return ElevatedButton.icon(
        onPressed: onPressed ?? () {},
        style: ElevatedButton.styleFrom(
            fixedSize: const Size.fromWidth(double.maxFinite)),
        icon: Icon(icon),
        label: Text(text));
  }
}

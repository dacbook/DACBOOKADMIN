import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_model.freezed.dart';
part 'book_model.g.dart';

@freezed
class BookModel with _$BookModel {
  //const HomeMenuModel._();
  const factory BookModel({
    required String id,
    required String bookmasterid,
    required bool ismasterbook,
    required String bookname,
    required String description,
    required String bookimage,
    required String date,
    required String time,
    required String isofficial,
    required String ispublic,
    required String istype,
    required String ownerid,
    required String pin,
    required String sharelink,
    required String status,
    required int totalmember,
    @Default([]) List<String> bookmember,
    @Default([]) List<String> bookadmin,
    @Default([]) List<String> bookmemberrequest,
  }) = _BookModel;

  factory BookModel.fromJson(Map<String, dynamic> json) =>
      _$BookModelFromJson(json);
}

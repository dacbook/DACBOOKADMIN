import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
class UserModel with _$UserModel {
  //const HomeMenuModel._();
  const factory UserModel({
    required String uid,
    required String email,
    required String password,
    @Default('') String avatar,
    @Default('') String userRole,
    @Default('') String fcmtoken,
    @Default('') String userStatus,
    @Default('') String userCoverProfile,
    @Default('') String pin,
    @Default('') String username,
    @Default('') String statusMariage,
    @Default('') String address,
    @Default('') String package,
    @Default('') String bio,
    @Default('') String noKtp,
    @Default('') String noHp,
    @Default('') String noWhatsapp,
    @Default('') String noSignal,
    @Default('') String noLine,
    @Default('') String noKakaoTalk,
    @Default('') String noWechat,
    @Default('') String birthday,
    @Default('') String noijazah,
    @Default('') String nonisn,
    @Default('') String karir,
    @Default('') String jabatan,
    @Default('') String pendidikan,
    @Default('') String penghargaan,
    @Default('') String bookmasterkepsek,
    @Default(0) int postlength,
    @Default(false) bool ishideTanggalLahir,
    @Default(false) bool ishideEmail,
    @Default(false) bool ishideNoHandphone,
    @Default(false) bool ishideNoKtp,
    @Default(false) bool ishideNoIjazah,
    @Default([]) List<String> followers,
    @Default([]) List<String> following,
    // required String useravatar,
    // @Default([]) List<String> followers,
  }) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return _UserModel.fromJson(json);
}

/// @nodoc
mixin _$UserModel {
  String get uid => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get password => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  String get userRole => throw _privateConstructorUsedError;
  String get fcmtoken => throw _privateConstructorUsedError;
  String get userStatus => throw _privateConstructorUsedError;
  String get userCoverProfile => throw _privateConstructorUsedError;
  String get pin => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get statusMariage => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;
  String get package => throw _privateConstructorUsedError;
  String get bio => throw _privateConstructorUsedError;
  String get noKtp => throw _privateConstructorUsedError;
  String get noHp => throw _privateConstructorUsedError;
  String get noWhatsapp => throw _privateConstructorUsedError;
  String get noSignal => throw _privateConstructorUsedError;
  String get noLine => throw _privateConstructorUsedError;
  String get noKakaoTalk => throw _privateConstructorUsedError;
  String get noWechat => throw _privateConstructorUsedError;
  String get birthday => throw _privateConstructorUsedError;
  String get noijazah => throw _privateConstructorUsedError;
  String get nonisn => throw _privateConstructorUsedError;
  String get karir => throw _privateConstructorUsedError;
  String get jabatan => throw _privateConstructorUsedError;
  String get pendidikan => throw _privateConstructorUsedError;
  String get penghargaan => throw _privateConstructorUsedError;
  String get bookmasterkepsek => throw _privateConstructorUsedError;
  int get postlength => throw _privateConstructorUsedError;
  bool get ishideTanggalLahir => throw _privateConstructorUsedError;
  bool get ishideEmail => throw _privateConstructorUsedError;
  bool get ishideNoHandphone => throw _privateConstructorUsedError;
  bool get ishideNoKtp => throw _privateConstructorUsedError;
  bool get ishideNoIjazah => throw _privateConstructorUsedError;
  List<String> get followers => throw _privateConstructorUsedError;
  List<String> get following => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserModelCopyWith<UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserModelCopyWith<$Res> {
  factory $UserModelCopyWith(UserModel value, $Res Function(UserModel) then) =
      _$UserModelCopyWithImpl<$Res, UserModel>;
  @useResult
  $Res call(
      {String uid,
      String email,
      String password,
      String avatar,
      String userRole,
      String fcmtoken,
      String userStatus,
      String userCoverProfile,
      String pin,
      String username,
      String statusMariage,
      String address,
      String package,
      String bio,
      String noKtp,
      String noHp,
      String noWhatsapp,
      String noSignal,
      String noLine,
      String noKakaoTalk,
      String noWechat,
      String birthday,
      String noijazah,
      String nonisn,
      String karir,
      String jabatan,
      String pendidikan,
      String penghargaan,
      String bookmasterkepsek,
      int postlength,
      bool ishideTanggalLahir,
      bool ishideEmail,
      bool ishideNoHandphone,
      bool ishideNoKtp,
      bool ishideNoIjazah,
      List<String> followers,
      List<String> following});
}

/// @nodoc
class _$UserModelCopyWithImpl<$Res, $Val extends UserModel>
    implements $UserModelCopyWith<$Res> {
  _$UserModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? email = null,
    Object? password = null,
    Object? avatar = null,
    Object? userRole = null,
    Object? fcmtoken = null,
    Object? userStatus = null,
    Object? userCoverProfile = null,
    Object? pin = null,
    Object? username = null,
    Object? statusMariage = null,
    Object? address = null,
    Object? package = null,
    Object? bio = null,
    Object? noKtp = null,
    Object? noHp = null,
    Object? noWhatsapp = null,
    Object? noSignal = null,
    Object? noLine = null,
    Object? noKakaoTalk = null,
    Object? noWechat = null,
    Object? birthday = null,
    Object? noijazah = null,
    Object? nonisn = null,
    Object? karir = null,
    Object? jabatan = null,
    Object? pendidikan = null,
    Object? penghargaan = null,
    Object? bookmasterkepsek = null,
    Object? postlength = null,
    Object? ishideTanggalLahir = null,
    Object? ishideEmail = null,
    Object? ishideNoHandphone = null,
    Object? ishideNoKtp = null,
    Object? ishideNoIjazah = null,
    Object? followers = null,
    Object? following = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      userRole: null == userRole
          ? _value.userRole
          : userRole // ignore: cast_nullable_to_non_nullable
              as String,
      fcmtoken: null == fcmtoken
          ? _value.fcmtoken
          : fcmtoken // ignore: cast_nullable_to_non_nullable
              as String,
      userStatus: null == userStatus
          ? _value.userStatus
          : userStatus // ignore: cast_nullable_to_non_nullable
              as String,
      userCoverProfile: null == userCoverProfile
          ? _value.userCoverProfile
          : userCoverProfile // ignore: cast_nullable_to_non_nullable
              as String,
      pin: null == pin
          ? _value.pin
          : pin // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      statusMariage: null == statusMariage
          ? _value.statusMariage
          : statusMariage // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      package: null == package
          ? _value.package
          : package // ignore: cast_nullable_to_non_nullable
              as String,
      bio: null == bio
          ? _value.bio
          : bio // ignore: cast_nullable_to_non_nullable
              as String,
      noKtp: null == noKtp
          ? _value.noKtp
          : noKtp // ignore: cast_nullable_to_non_nullable
              as String,
      noHp: null == noHp
          ? _value.noHp
          : noHp // ignore: cast_nullable_to_non_nullable
              as String,
      noWhatsapp: null == noWhatsapp
          ? _value.noWhatsapp
          : noWhatsapp // ignore: cast_nullable_to_non_nullable
              as String,
      noSignal: null == noSignal
          ? _value.noSignal
          : noSignal // ignore: cast_nullable_to_non_nullable
              as String,
      noLine: null == noLine
          ? _value.noLine
          : noLine // ignore: cast_nullable_to_non_nullable
              as String,
      noKakaoTalk: null == noKakaoTalk
          ? _value.noKakaoTalk
          : noKakaoTalk // ignore: cast_nullable_to_non_nullable
              as String,
      noWechat: null == noWechat
          ? _value.noWechat
          : noWechat // ignore: cast_nullable_to_non_nullable
              as String,
      birthday: null == birthday
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as String,
      noijazah: null == noijazah
          ? _value.noijazah
          : noijazah // ignore: cast_nullable_to_non_nullable
              as String,
      nonisn: null == nonisn
          ? _value.nonisn
          : nonisn // ignore: cast_nullable_to_non_nullable
              as String,
      karir: null == karir
          ? _value.karir
          : karir // ignore: cast_nullable_to_non_nullable
              as String,
      jabatan: null == jabatan
          ? _value.jabatan
          : jabatan // ignore: cast_nullable_to_non_nullable
              as String,
      pendidikan: null == pendidikan
          ? _value.pendidikan
          : pendidikan // ignore: cast_nullable_to_non_nullable
              as String,
      penghargaan: null == penghargaan
          ? _value.penghargaan
          : penghargaan // ignore: cast_nullable_to_non_nullable
              as String,
      bookmasterkepsek: null == bookmasterkepsek
          ? _value.bookmasterkepsek
          : bookmasterkepsek // ignore: cast_nullable_to_non_nullable
              as String,
      postlength: null == postlength
          ? _value.postlength
          : postlength // ignore: cast_nullable_to_non_nullable
              as int,
      ishideTanggalLahir: null == ishideTanggalLahir
          ? _value.ishideTanggalLahir
          : ishideTanggalLahir // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideEmail: null == ishideEmail
          ? _value.ishideEmail
          : ishideEmail // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideNoHandphone: null == ishideNoHandphone
          ? _value.ishideNoHandphone
          : ishideNoHandphone // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideNoKtp: null == ishideNoKtp
          ? _value.ishideNoKtp
          : ishideNoKtp // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideNoIjazah: null == ishideNoIjazah
          ? _value.ishideNoIjazah
          : ishideNoIjazah // ignore: cast_nullable_to_non_nullable
              as bool,
      followers: null == followers
          ? _value.followers
          : followers // ignore: cast_nullable_to_non_nullable
              as List<String>,
      following: null == following
          ? _value.following
          : following // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserModelCopyWith<$Res> implements $UserModelCopyWith<$Res> {
  factory _$$_UserModelCopyWith(
          _$_UserModel value, $Res Function(_$_UserModel) then) =
      __$$_UserModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String uid,
      String email,
      String password,
      String avatar,
      String userRole,
      String fcmtoken,
      String userStatus,
      String userCoverProfile,
      String pin,
      String username,
      String statusMariage,
      String address,
      String package,
      String bio,
      String noKtp,
      String noHp,
      String noWhatsapp,
      String noSignal,
      String noLine,
      String noKakaoTalk,
      String noWechat,
      String birthday,
      String noijazah,
      String nonisn,
      String karir,
      String jabatan,
      String pendidikan,
      String penghargaan,
      String bookmasterkepsek,
      int postlength,
      bool ishideTanggalLahir,
      bool ishideEmail,
      bool ishideNoHandphone,
      bool ishideNoKtp,
      bool ishideNoIjazah,
      List<String> followers,
      List<String> following});
}

/// @nodoc
class __$$_UserModelCopyWithImpl<$Res>
    extends _$UserModelCopyWithImpl<$Res, _$_UserModel>
    implements _$$_UserModelCopyWith<$Res> {
  __$$_UserModelCopyWithImpl(
      _$_UserModel _value, $Res Function(_$_UserModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? email = null,
    Object? password = null,
    Object? avatar = null,
    Object? userRole = null,
    Object? fcmtoken = null,
    Object? userStatus = null,
    Object? userCoverProfile = null,
    Object? pin = null,
    Object? username = null,
    Object? statusMariage = null,
    Object? address = null,
    Object? package = null,
    Object? bio = null,
    Object? noKtp = null,
    Object? noHp = null,
    Object? noWhatsapp = null,
    Object? noSignal = null,
    Object? noLine = null,
    Object? noKakaoTalk = null,
    Object? noWechat = null,
    Object? birthday = null,
    Object? noijazah = null,
    Object? nonisn = null,
    Object? karir = null,
    Object? jabatan = null,
    Object? pendidikan = null,
    Object? penghargaan = null,
    Object? bookmasterkepsek = null,
    Object? postlength = null,
    Object? ishideTanggalLahir = null,
    Object? ishideEmail = null,
    Object? ishideNoHandphone = null,
    Object? ishideNoKtp = null,
    Object? ishideNoIjazah = null,
    Object? followers = null,
    Object? following = null,
  }) {
    return _then(_$_UserModel(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      userRole: null == userRole
          ? _value.userRole
          : userRole // ignore: cast_nullable_to_non_nullable
              as String,
      fcmtoken: null == fcmtoken
          ? _value.fcmtoken
          : fcmtoken // ignore: cast_nullable_to_non_nullable
              as String,
      userStatus: null == userStatus
          ? _value.userStatus
          : userStatus // ignore: cast_nullable_to_non_nullable
              as String,
      userCoverProfile: null == userCoverProfile
          ? _value.userCoverProfile
          : userCoverProfile // ignore: cast_nullable_to_non_nullable
              as String,
      pin: null == pin
          ? _value.pin
          : pin // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      statusMariage: null == statusMariage
          ? _value.statusMariage
          : statusMariage // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      package: null == package
          ? _value.package
          : package // ignore: cast_nullable_to_non_nullable
              as String,
      bio: null == bio
          ? _value.bio
          : bio // ignore: cast_nullable_to_non_nullable
              as String,
      noKtp: null == noKtp
          ? _value.noKtp
          : noKtp // ignore: cast_nullable_to_non_nullable
              as String,
      noHp: null == noHp
          ? _value.noHp
          : noHp // ignore: cast_nullable_to_non_nullable
              as String,
      noWhatsapp: null == noWhatsapp
          ? _value.noWhatsapp
          : noWhatsapp // ignore: cast_nullable_to_non_nullable
              as String,
      noSignal: null == noSignal
          ? _value.noSignal
          : noSignal // ignore: cast_nullable_to_non_nullable
              as String,
      noLine: null == noLine
          ? _value.noLine
          : noLine // ignore: cast_nullable_to_non_nullable
              as String,
      noKakaoTalk: null == noKakaoTalk
          ? _value.noKakaoTalk
          : noKakaoTalk // ignore: cast_nullable_to_non_nullable
              as String,
      noWechat: null == noWechat
          ? _value.noWechat
          : noWechat // ignore: cast_nullable_to_non_nullable
              as String,
      birthday: null == birthday
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as String,
      noijazah: null == noijazah
          ? _value.noijazah
          : noijazah // ignore: cast_nullable_to_non_nullable
              as String,
      nonisn: null == nonisn
          ? _value.nonisn
          : nonisn // ignore: cast_nullable_to_non_nullable
              as String,
      karir: null == karir
          ? _value.karir
          : karir // ignore: cast_nullable_to_non_nullable
              as String,
      jabatan: null == jabatan
          ? _value.jabatan
          : jabatan // ignore: cast_nullable_to_non_nullable
              as String,
      pendidikan: null == pendidikan
          ? _value.pendidikan
          : pendidikan // ignore: cast_nullable_to_non_nullable
              as String,
      penghargaan: null == penghargaan
          ? _value.penghargaan
          : penghargaan // ignore: cast_nullable_to_non_nullable
              as String,
      bookmasterkepsek: null == bookmasterkepsek
          ? _value.bookmasterkepsek
          : bookmasterkepsek // ignore: cast_nullable_to_non_nullable
              as String,
      postlength: null == postlength
          ? _value.postlength
          : postlength // ignore: cast_nullable_to_non_nullable
              as int,
      ishideTanggalLahir: null == ishideTanggalLahir
          ? _value.ishideTanggalLahir
          : ishideTanggalLahir // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideEmail: null == ishideEmail
          ? _value.ishideEmail
          : ishideEmail // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideNoHandphone: null == ishideNoHandphone
          ? _value.ishideNoHandphone
          : ishideNoHandphone // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideNoKtp: null == ishideNoKtp
          ? _value.ishideNoKtp
          : ishideNoKtp // ignore: cast_nullable_to_non_nullable
              as bool,
      ishideNoIjazah: null == ishideNoIjazah
          ? _value.ishideNoIjazah
          : ishideNoIjazah // ignore: cast_nullable_to_non_nullable
              as bool,
      followers: null == followers
          ? _value._followers
          : followers // ignore: cast_nullable_to_non_nullable
              as List<String>,
      following: null == following
          ? _value._following
          : following // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserModel implements _UserModel {
  const _$_UserModel(
      {required this.uid,
      required this.email,
      required this.password,
      this.avatar = '',
      this.userRole = '',
      this.fcmtoken = '',
      this.userStatus = '',
      this.userCoverProfile = '',
      this.pin = '',
      this.username = '',
      this.statusMariage = '',
      this.address = '',
      this.package = '',
      this.bio = '',
      this.noKtp = '',
      this.noHp = '',
      this.noWhatsapp = '',
      this.noSignal = '',
      this.noLine = '',
      this.noKakaoTalk = '',
      this.noWechat = '',
      this.birthday = '',
      this.noijazah = '',
      this.nonisn = '',
      this.karir = '',
      this.jabatan = '',
      this.pendidikan = '',
      this.penghargaan = '',
      this.bookmasterkepsek = '',
      this.postlength = 0,
      this.ishideTanggalLahir = false,
      this.ishideEmail = false,
      this.ishideNoHandphone = false,
      this.ishideNoKtp = false,
      this.ishideNoIjazah = false,
      final List<String> followers = const [],
      final List<String> following = const []})
      : _followers = followers,
        _following = following;

  factory _$_UserModel.fromJson(Map<String, dynamic> json) =>
      _$$_UserModelFromJson(json);

  @override
  final String uid;
  @override
  final String email;
  @override
  final String password;
  @override
  @JsonKey()
  final String avatar;
  @override
  @JsonKey()
  final String userRole;
  @override
  @JsonKey()
  final String fcmtoken;
  @override
  @JsonKey()
  final String userStatus;
  @override
  @JsonKey()
  final String userCoverProfile;
  @override
  @JsonKey()
  final String pin;
  @override
  @JsonKey()
  final String username;
  @override
  @JsonKey()
  final String statusMariage;
  @override
  @JsonKey()
  final String address;
  @override
  @JsonKey()
  final String package;
  @override
  @JsonKey()
  final String bio;
  @override
  @JsonKey()
  final String noKtp;
  @override
  @JsonKey()
  final String noHp;
  @override
  @JsonKey()
  final String noWhatsapp;
  @override
  @JsonKey()
  final String noSignal;
  @override
  @JsonKey()
  final String noLine;
  @override
  @JsonKey()
  final String noKakaoTalk;
  @override
  @JsonKey()
  final String noWechat;
  @override
  @JsonKey()
  final String birthday;
  @override
  @JsonKey()
  final String noijazah;
  @override
  @JsonKey()
  final String nonisn;
  @override
  @JsonKey()
  final String karir;
  @override
  @JsonKey()
  final String jabatan;
  @override
  @JsonKey()
  final String pendidikan;
  @override
  @JsonKey()
  final String penghargaan;
  @override
  @JsonKey()
  final String bookmasterkepsek;
  @override
  @JsonKey()
  final int postlength;
  @override
  @JsonKey()
  final bool ishideTanggalLahir;
  @override
  @JsonKey()
  final bool ishideEmail;
  @override
  @JsonKey()
  final bool ishideNoHandphone;
  @override
  @JsonKey()
  final bool ishideNoKtp;
  @override
  @JsonKey()
  final bool ishideNoIjazah;
  final List<String> _followers;
  @override
  @JsonKey()
  List<String> get followers {
    if (_followers is EqualUnmodifiableListView) return _followers;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_followers);
  }

  final List<String> _following;
  @override
  @JsonKey()
  List<String> get following {
    if (_following is EqualUnmodifiableListView) return _following;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_following);
  }

  @override
  String toString() {
    return 'UserModel(uid: $uid, email: $email, password: $password, avatar: $avatar, userRole: $userRole, fcmtoken: $fcmtoken, userStatus: $userStatus, userCoverProfile: $userCoverProfile, pin: $pin, username: $username, statusMariage: $statusMariage, address: $address, package: $package, bio: $bio, noKtp: $noKtp, noHp: $noHp, noWhatsapp: $noWhatsapp, noSignal: $noSignal, noLine: $noLine, noKakaoTalk: $noKakaoTalk, noWechat: $noWechat, birthday: $birthday, noijazah: $noijazah, nonisn: $nonisn, karir: $karir, jabatan: $jabatan, pendidikan: $pendidikan, penghargaan: $penghargaan, bookmasterkepsek: $bookmasterkepsek, postlength: $postlength, ishideTanggalLahir: $ishideTanggalLahir, ishideEmail: $ishideEmail, ishideNoHandphone: $ishideNoHandphone, ishideNoKtp: $ishideNoKtp, ishideNoIjazah: $ishideNoIjazah, followers: $followers, following: $following)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserModel &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.avatar, avatar) || other.avatar == avatar) &&
            (identical(other.userRole, userRole) ||
                other.userRole == userRole) &&
            (identical(other.fcmtoken, fcmtoken) ||
                other.fcmtoken == fcmtoken) &&
            (identical(other.userStatus, userStatus) ||
                other.userStatus == userStatus) &&
            (identical(other.userCoverProfile, userCoverProfile) ||
                other.userCoverProfile == userCoverProfile) &&
            (identical(other.pin, pin) || other.pin == pin) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.statusMariage, statusMariage) ||
                other.statusMariage == statusMariage) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.package, package) || other.package == package) &&
            (identical(other.bio, bio) || other.bio == bio) &&
            (identical(other.noKtp, noKtp) || other.noKtp == noKtp) &&
            (identical(other.noHp, noHp) || other.noHp == noHp) &&
            (identical(other.noWhatsapp, noWhatsapp) ||
                other.noWhatsapp == noWhatsapp) &&
            (identical(other.noSignal, noSignal) ||
                other.noSignal == noSignal) &&
            (identical(other.noLine, noLine) || other.noLine == noLine) &&
            (identical(other.noKakaoTalk, noKakaoTalk) ||
                other.noKakaoTalk == noKakaoTalk) &&
            (identical(other.noWechat, noWechat) ||
                other.noWechat == noWechat) &&
            (identical(other.birthday, birthday) ||
                other.birthday == birthday) &&
            (identical(other.noijazah, noijazah) ||
                other.noijazah == noijazah) &&
            (identical(other.nonisn, nonisn) || other.nonisn == nonisn) &&
            (identical(other.karir, karir) || other.karir == karir) &&
            (identical(other.jabatan, jabatan) || other.jabatan == jabatan) &&
            (identical(other.pendidikan, pendidikan) ||
                other.pendidikan == pendidikan) &&
            (identical(other.penghargaan, penghargaan) ||
                other.penghargaan == penghargaan) &&
            (identical(other.bookmasterkepsek, bookmasterkepsek) ||
                other.bookmasterkepsek == bookmasterkepsek) &&
            (identical(other.postlength, postlength) ||
                other.postlength == postlength) &&
            (identical(other.ishideTanggalLahir, ishideTanggalLahir) ||
                other.ishideTanggalLahir == ishideTanggalLahir) &&
            (identical(other.ishideEmail, ishideEmail) ||
                other.ishideEmail == ishideEmail) &&
            (identical(other.ishideNoHandphone, ishideNoHandphone) ||
                other.ishideNoHandphone == ishideNoHandphone) &&
            (identical(other.ishideNoKtp, ishideNoKtp) ||
                other.ishideNoKtp == ishideNoKtp) &&
            (identical(other.ishideNoIjazah, ishideNoIjazah) ||
                other.ishideNoIjazah == ishideNoIjazah) &&
            const DeepCollectionEquality()
                .equals(other._followers, _followers) &&
            const DeepCollectionEquality()
                .equals(other._following, _following));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        uid,
        email,
        password,
        avatar,
        userRole,
        fcmtoken,
        userStatus,
        userCoverProfile,
        pin,
        username,
        statusMariage,
        address,
        package,
        bio,
        noKtp,
        noHp,
        noWhatsapp,
        noSignal,
        noLine,
        noKakaoTalk,
        noWechat,
        birthday,
        noijazah,
        nonisn,
        karir,
        jabatan,
        pendidikan,
        penghargaan,
        bookmasterkepsek,
        postlength,
        ishideTanggalLahir,
        ishideEmail,
        ishideNoHandphone,
        ishideNoKtp,
        ishideNoIjazah,
        const DeepCollectionEquality().hash(_followers),
        const DeepCollectionEquality().hash(_following)
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      __$$_UserModelCopyWithImpl<_$_UserModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserModelToJson(
      this,
    );
  }
}

abstract class _UserModel implements UserModel {
  const factory _UserModel(
      {required final String uid,
      required final String email,
      required final String password,
      final String avatar,
      final String userRole,
      final String fcmtoken,
      final String userStatus,
      final String userCoverProfile,
      final String pin,
      final String username,
      final String statusMariage,
      final String address,
      final String package,
      final String bio,
      final String noKtp,
      final String noHp,
      final String noWhatsapp,
      final String noSignal,
      final String noLine,
      final String noKakaoTalk,
      final String noWechat,
      final String birthday,
      final String noijazah,
      final String nonisn,
      final String karir,
      final String jabatan,
      final String pendidikan,
      final String penghargaan,
      final String bookmasterkepsek,
      final int postlength,
      final bool ishideTanggalLahir,
      final bool ishideEmail,
      final bool ishideNoHandphone,
      final bool ishideNoKtp,
      final bool ishideNoIjazah,
      final List<String> followers,
      final List<String> following}) = _$_UserModel;

  factory _UserModel.fromJson(Map<String, dynamic> json) =
      _$_UserModel.fromJson;

  @override
  String get uid;
  @override
  String get email;
  @override
  String get password;
  @override
  String get avatar;
  @override
  String get userRole;
  @override
  String get fcmtoken;
  @override
  String get userStatus;
  @override
  String get userCoverProfile;
  @override
  String get pin;
  @override
  String get username;
  @override
  String get statusMariage;
  @override
  String get address;
  @override
  String get package;
  @override
  String get bio;
  @override
  String get noKtp;
  @override
  String get noHp;
  @override
  String get noWhatsapp;
  @override
  String get noSignal;
  @override
  String get noLine;
  @override
  String get noKakaoTalk;
  @override
  String get noWechat;
  @override
  String get birthday;
  @override
  String get noijazah;
  @override
  String get nonisn;
  @override
  String get karir;
  @override
  String get jabatan;
  @override
  String get pendidikan;
  @override
  String get penghargaan;
  @override
  String get bookmasterkepsek;
  @override
  int get postlength;
  @override
  bool get ishideTanggalLahir;
  @override
  bool get ishideEmail;
  @override
  bool get ishideNoHandphone;
  @override
  bool get ishideNoKtp;
  @override
  bool get ishideNoIjazah;
  @override
  List<String> get followers;
  @override
  List<String> get following;
  @override
  @JsonKey(ignore: true)
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

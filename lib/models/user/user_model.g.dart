// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserModel _$$_UserModelFromJson(Map<String, dynamic> json) => _$_UserModel(
      uid: json['uid'] as String,
      email: json['email'] as String,
      password: json['password'] as String,
      avatar: json['avatar'] as String? ?? '',
      userRole: json['userRole'] as String? ?? '',
      fcmtoken: json['fcmtoken'] as String? ?? '',
      userStatus: json['userStatus'] as String? ?? '',
      userCoverProfile: json['userCoverProfile'] as String? ?? '',
      pin: json['pin'] as String? ?? '',
      username: json['username'] as String? ?? '',
      statusMariage: json['statusMariage'] as String? ?? '',
      address: json['address'] as String? ?? '',
      package: json['package'] as String? ?? '',
      bio: json['bio'] as String? ?? '',
      noKtp: json['noKtp'] as String? ?? '',
      noHp: json['noHp'] as String? ?? '',
      noWhatsapp: json['noWhatsapp'] as String? ?? '',
      noSignal: json['noSignal'] as String? ?? '',
      noLine: json['noLine'] as String? ?? '',
      noKakaoTalk: json['noKakaoTalk'] as String? ?? '',
      noWechat: json['noWechat'] as String? ?? '',
      birthday: json['birthday'] as String? ?? '',
      noijazah: json['noijazah'] as String? ?? '',
      nonisn: json['nonisn'] as String? ?? '',
      karir: json['karir'] as String? ?? '',
      jabatan: json['jabatan'] as String? ?? '',
      pendidikan: json['pendidikan'] as String? ?? '',
      penghargaan: json['penghargaan'] as String? ?? '',
      bookmasterkepsek: json['bookmasterkepsek'] as String? ?? '',
      postlength: json['postlength'] as int? ?? 0,
      ishideTanggalLahir: json['ishideTanggalLahir'] as bool? ?? false,
      ishideEmail: json['ishideEmail'] as bool? ?? false,
      ishideNoHandphone: json['ishideNoHandphone'] as bool? ?? false,
      ishideNoKtp: json['ishideNoKtp'] as bool? ?? false,
      ishideNoIjazah: json['ishideNoIjazah'] as bool? ?? false,
      followers: (json['followers'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      following: (json['following'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$$_UserModelToJson(_$_UserModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'email': instance.email,
      'password': instance.password,
      'avatar': instance.avatar,
      'userRole': instance.userRole,
      'fcmtoken': instance.fcmtoken,
      'userStatus': instance.userStatus,
      'userCoverProfile': instance.userCoverProfile,
      'pin': instance.pin,
      'username': instance.username,
      'statusMariage': instance.statusMariage,
      'address': instance.address,
      'package': instance.package,
      'bio': instance.bio,
      'noKtp': instance.noKtp,
      'noHp': instance.noHp,
      'noWhatsapp': instance.noWhatsapp,
      'noSignal': instance.noSignal,
      'noLine': instance.noLine,
      'noKakaoTalk': instance.noKakaoTalk,
      'noWechat': instance.noWechat,
      'birthday': instance.birthday,
      'noijazah': instance.noijazah,
      'nonisn': instance.nonisn,
      'karir': instance.karir,
      'jabatan': instance.jabatan,
      'pendidikan': instance.pendidikan,
      'penghargaan': instance.penghargaan,
      'bookmasterkepsek': instance.bookmasterkepsek,
      'postlength': instance.postlength,
      'ishideTanggalLahir': instance.ishideTanggalLahir,
      'ishideEmail': instance.ishideEmail,
      'ishideNoHandphone': instance.ishideNoHandphone,
      'ishideNoKtp': instance.ishideNoKtp,
      'ishideNoIjazah': instance.ishideNoIjazah,
      'followers': instance.followers,
      'following': instance.following,
    };

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DialogUtilsModel {
  final String label;
  final String hint;
  final TextEditingController textEditingController;
  final Icon icon;

  DialogUtilsModel({
    required this.label,
    required this.hint,
    required this.textEditingController,
    required this.icon,
   
  });
}


import 'package:freezed_annotation/freezed_annotation.dart';

part 'feed_model.freezed.dart';
part 'feed_model.g.dart';

@freezed
class FeedModel with _$FeedModel {
  //const HomeMenuModel._();
  const factory FeedModel({
    required String id,
    required String author,
    required String description,
    required String time,
    required String username,
    required String userimage,
    @Default({}) Map mediagallery,
    @Default('') String date,
    @Default('') String title,
    @Default('') String bookid,
    @Default('') String booklogo,
    //@Default('') String timestamp,
    @Default('') String categoryKegiatan,
    @Default('') String tanggalkegiatan,
    @Default('') String keterangankegiatan,
    @Default('') String deskripsikegiatan,
    @Default('') String thumbnail,
    @Default('') String mediacategory,
    @Default('') String bookmasterid,
    @Default('') String pdfdownload,
    @Default([]) List<String> mediaFeedList,
    @Default(false) bool isphoto,
    @Default(false) bool istext,
    @Default(false) bool isaudio,
    @Default(0) int favorites,
    @Default(0) int commentcount,
    @Default([]) List<String> followers,
    @Default([]) List<String> category,
    @Default([]) List<String> followersfavorites,
   // required String useravatar,
   // @Default([]) List<String> followers,
  }) = _FeedModel;

   factory FeedModel.fromJson(Map<String, dynamic> json, String id) => _$FeedModelFromJson(json);
}
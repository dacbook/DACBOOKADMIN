// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FeedModel _$$_FeedModelFromJson(Map<String, dynamic> json) => _$_FeedModel(
      id: json['id'] as String,
      author: json['author'] as String,
      description: json['description'] as String,
      time: json['time'] as String,
      username: json['username'] as String,
      userimage: json['userimage'] as String,
      mediagallery: json['mediagallery'] as Map<String, dynamic>? ?? const {},
      date: json['date'] as String? ?? '',
      title: json['title'] as String? ?? '',
      bookid: json['bookid'] as String? ?? '',
      booklogo: json['booklogo'] as String? ?? '',
      categoryKegiatan: json['categoryKegiatan'] as String? ?? '',
      tanggalkegiatan: json['tanggalkegiatan'] as String? ?? '',
      keterangankegiatan: json['keterangankegiatan'] as String? ?? '',
      deskripsikegiatan: json['deskripsikegiatan'] as String? ?? '',
      thumbnail: json['thumbnail'] as String? ?? '',
      mediacategory: json['mediacategory'] as String? ?? '',
      bookmasterid: json['bookmasterid'] as String? ?? '',
      pdfdownload: json['pdfdownload'] as String? ?? '',
      mediaFeedList: (json['mediaFeedList'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      isphoto: json['isphoto'] as bool? ?? false,
      istext: json['istext'] as bool? ?? false,
      isaudio: json['isaudio'] as bool? ?? false,
      favorites: json['favorites'] as int? ?? 0,
      commentcount: json['commentcount'] as int? ?? 0,
      followers: (json['followers'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      category: (json['category'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      followersfavorites: (json['followersfavorites'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$$_FeedModelToJson(_$_FeedModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'author': instance.author,
      'description': instance.description,
      'time': instance.time,
      'username': instance.username,
      'userimage': instance.userimage,
      'mediagallery': instance.mediagallery,
      'date': instance.date,
      'title': instance.title,
      'bookid': instance.bookid,
      'booklogo': instance.booklogo,
      'categoryKegiatan': instance.categoryKegiatan,
      'tanggalkegiatan': instance.tanggalkegiatan,
      'keterangankegiatan': instance.keterangankegiatan,
      'deskripsikegiatan': instance.deskripsikegiatan,
      'thumbnail': instance.thumbnail,
      'mediacategory': instance.mediacategory,
      'bookmasterid': instance.bookmasterid,
      'pdfdownload': instance.pdfdownload,
      'mediaFeedList': instance.mediaFeedList,
      'isphoto': instance.isphoto,
      'istext': instance.istext,
      'isaudio': instance.isaudio,
      'favorites': instance.favorites,
      'commentcount': instance.commentcount,
      'followers': instance.followers,
      'category': instance.category,
      'followersfavorites': instance.followersfavorites,
    };

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'feed_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FeedModel _$FeedModelFromJson(Map<String, dynamic> json) {
  return _FeedModel.fromJson(json);
}

/// @nodoc
mixin _$FeedModel {
  String get id => throw _privateConstructorUsedError;
  String get author => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get userimage => throw _privateConstructorUsedError;
  Map<dynamic, dynamic> get mediagallery => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get bookid => throw _privateConstructorUsedError;
  String get booklogo =>
      throw _privateConstructorUsedError; //@Default('') String timestamp,
  String get categoryKegiatan => throw _privateConstructorUsedError;
  String get tanggalkegiatan => throw _privateConstructorUsedError;
  String get keterangankegiatan => throw _privateConstructorUsedError;
  String get deskripsikegiatan => throw _privateConstructorUsedError;
  String get thumbnail => throw _privateConstructorUsedError;
  String get mediacategory => throw _privateConstructorUsedError;
  String get bookmasterid => throw _privateConstructorUsedError;
  String get pdfdownload => throw _privateConstructorUsedError;
  List<String> get mediaFeedList => throw _privateConstructorUsedError;
  bool get isphoto => throw _privateConstructorUsedError;
  bool get istext => throw _privateConstructorUsedError;
  bool get isaudio => throw _privateConstructorUsedError;
  int get favorites => throw _privateConstructorUsedError;
  int get commentcount => throw _privateConstructorUsedError;
  List<String> get followers => throw _privateConstructorUsedError;
  List<String> get category => throw _privateConstructorUsedError;
  List<String> get followersfavorites => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FeedModelCopyWith<FeedModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeedModelCopyWith<$Res> {
  factory $FeedModelCopyWith(FeedModel value, $Res Function(FeedModel) then) =
      _$FeedModelCopyWithImpl<$Res, FeedModel>;
  @useResult
  $Res call(
      {String id,
      String author,
      String description,
      String time,
      String username,
      String userimage,
      Map<dynamic, dynamic> mediagallery,
      String date,
      String title,
      String bookid,
      String booklogo,
      String categoryKegiatan,
      String tanggalkegiatan,
      String keterangankegiatan,
      String deskripsikegiatan,
      String thumbnail,
      String mediacategory,
      String bookmasterid,
      String pdfdownload,
      List<String> mediaFeedList,
      bool isphoto,
      bool istext,
      bool isaudio,
      int favorites,
      int commentcount,
      List<String> followers,
      List<String> category,
      List<String> followersfavorites});
}

/// @nodoc
class _$FeedModelCopyWithImpl<$Res, $Val extends FeedModel>
    implements $FeedModelCopyWith<$Res> {
  _$FeedModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? author = null,
    Object? description = null,
    Object? time = null,
    Object? username = null,
    Object? userimage = null,
    Object? mediagallery = null,
    Object? date = null,
    Object? title = null,
    Object? bookid = null,
    Object? booklogo = null,
    Object? categoryKegiatan = null,
    Object? tanggalkegiatan = null,
    Object? keterangankegiatan = null,
    Object? deskripsikegiatan = null,
    Object? thumbnail = null,
    Object? mediacategory = null,
    Object? bookmasterid = null,
    Object? pdfdownload = null,
    Object? mediaFeedList = null,
    Object? isphoto = null,
    Object? istext = null,
    Object? isaudio = null,
    Object? favorites = null,
    Object? commentcount = null,
    Object? followers = null,
    Object? category = null,
    Object? followersfavorites = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      author: null == author
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      userimage: null == userimage
          ? _value.userimage
          : userimage // ignore: cast_nullable_to_non_nullable
              as String,
      mediagallery: null == mediagallery
          ? _value.mediagallery
          : mediagallery // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      bookid: null == bookid
          ? _value.bookid
          : bookid // ignore: cast_nullable_to_non_nullable
              as String,
      booklogo: null == booklogo
          ? _value.booklogo
          : booklogo // ignore: cast_nullable_to_non_nullable
              as String,
      categoryKegiatan: null == categoryKegiatan
          ? _value.categoryKegiatan
          : categoryKegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      tanggalkegiatan: null == tanggalkegiatan
          ? _value.tanggalkegiatan
          : tanggalkegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      keterangankegiatan: null == keterangankegiatan
          ? _value.keterangankegiatan
          : keterangankegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      deskripsikegiatan: null == deskripsikegiatan
          ? _value.deskripsikegiatan
          : deskripsikegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      thumbnail: null == thumbnail
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as String,
      mediacategory: null == mediacategory
          ? _value.mediacategory
          : mediacategory // ignore: cast_nullable_to_non_nullable
              as String,
      bookmasterid: null == bookmasterid
          ? _value.bookmasterid
          : bookmasterid // ignore: cast_nullable_to_non_nullable
              as String,
      pdfdownload: null == pdfdownload
          ? _value.pdfdownload
          : pdfdownload // ignore: cast_nullable_to_non_nullable
              as String,
      mediaFeedList: null == mediaFeedList
          ? _value.mediaFeedList
          : mediaFeedList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      isphoto: null == isphoto
          ? _value.isphoto
          : isphoto // ignore: cast_nullable_to_non_nullable
              as bool,
      istext: null == istext
          ? _value.istext
          : istext // ignore: cast_nullable_to_non_nullable
              as bool,
      isaudio: null == isaudio
          ? _value.isaudio
          : isaudio // ignore: cast_nullable_to_non_nullable
              as bool,
      favorites: null == favorites
          ? _value.favorites
          : favorites // ignore: cast_nullable_to_non_nullable
              as int,
      commentcount: null == commentcount
          ? _value.commentcount
          : commentcount // ignore: cast_nullable_to_non_nullable
              as int,
      followers: null == followers
          ? _value.followers
          : followers // ignore: cast_nullable_to_non_nullable
              as List<String>,
      category: null == category
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as List<String>,
      followersfavorites: null == followersfavorites
          ? _value.followersfavorites
          : followersfavorites // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FeedModelCopyWith<$Res> implements $FeedModelCopyWith<$Res> {
  factory _$$_FeedModelCopyWith(
          _$_FeedModel value, $Res Function(_$_FeedModel) then) =
      __$$_FeedModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String author,
      String description,
      String time,
      String username,
      String userimage,
      Map<dynamic, dynamic> mediagallery,
      String date,
      String title,
      String bookid,
      String booklogo,
      String categoryKegiatan,
      String tanggalkegiatan,
      String keterangankegiatan,
      String deskripsikegiatan,
      String thumbnail,
      String mediacategory,
      String bookmasterid,
      String pdfdownload,
      List<String> mediaFeedList,
      bool isphoto,
      bool istext,
      bool isaudio,
      int favorites,
      int commentcount,
      List<String> followers,
      List<String> category,
      List<String> followersfavorites});
}

/// @nodoc
class __$$_FeedModelCopyWithImpl<$Res>
    extends _$FeedModelCopyWithImpl<$Res, _$_FeedModel>
    implements _$$_FeedModelCopyWith<$Res> {
  __$$_FeedModelCopyWithImpl(
      _$_FeedModel _value, $Res Function(_$_FeedModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? author = null,
    Object? description = null,
    Object? time = null,
    Object? username = null,
    Object? userimage = null,
    Object? mediagallery = null,
    Object? date = null,
    Object? title = null,
    Object? bookid = null,
    Object? booklogo = null,
    Object? categoryKegiatan = null,
    Object? tanggalkegiatan = null,
    Object? keterangankegiatan = null,
    Object? deskripsikegiatan = null,
    Object? thumbnail = null,
    Object? mediacategory = null,
    Object? bookmasterid = null,
    Object? pdfdownload = null,
    Object? mediaFeedList = null,
    Object? isphoto = null,
    Object? istext = null,
    Object? isaudio = null,
    Object? favorites = null,
    Object? commentcount = null,
    Object? followers = null,
    Object? category = null,
    Object? followersfavorites = null,
  }) {
    return _then(_$_FeedModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      author: null == author
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      userimage: null == userimage
          ? _value.userimage
          : userimage // ignore: cast_nullable_to_non_nullable
              as String,
      mediagallery: null == mediagallery
          ? _value._mediagallery
          : mediagallery // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      bookid: null == bookid
          ? _value.bookid
          : bookid // ignore: cast_nullable_to_non_nullable
              as String,
      booklogo: null == booklogo
          ? _value.booklogo
          : booklogo // ignore: cast_nullable_to_non_nullable
              as String,
      categoryKegiatan: null == categoryKegiatan
          ? _value.categoryKegiatan
          : categoryKegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      tanggalkegiatan: null == tanggalkegiatan
          ? _value.tanggalkegiatan
          : tanggalkegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      keterangankegiatan: null == keterangankegiatan
          ? _value.keterangankegiatan
          : keterangankegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      deskripsikegiatan: null == deskripsikegiatan
          ? _value.deskripsikegiatan
          : deskripsikegiatan // ignore: cast_nullable_to_non_nullable
              as String,
      thumbnail: null == thumbnail
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as String,
      mediacategory: null == mediacategory
          ? _value.mediacategory
          : mediacategory // ignore: cast_nullable_to_non_nullable
              as String,
      bookmasterid: null == bookmasterid
          ? _value.bookmasterid
          : bookmasterid // ignore: cast_nullable_to_non_nullable
              as String,
      pdfdownload: null == pdfdownload
          ? _value.pdfdownload
          : pdfdownload // ignore: cast_nullable_to_non_nullable
              as String,
      mediaFeedList: null == mediaFeedList
          ? _value._mediaFeedList
          : mediaFeedList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      isphoto: null == isphoto
          ? _value.isphoto
          : isphoto // ignore: cast_nullable_to_non_nullable
              as bool,
      istext: null == istext
          ? _value.istext
          : istext // ignore: cast_nullable_to_non_nullable
              as bool,
      isaudio: null == isaudio
          ? _value.isaudio
          : isaudio // ignore: cast_nullable_to_non_nullable
              as bool,
      favorites: null == favorites
          ? _value.favorites
          : favorites // ignore: cast_nullable_to_non_nullable
              as int,
      commentcount: null == commentcount
          ? _value.commentcount
          : commentcount // ignore: cast_nullable_to_non_nullable
              as int,
      followers: null == followers
          ? _value._followers
          : followers // ignore: cast_nullable_to_non_nullable
              as List<String>,
      category: null == category
          ? _value._category
          : category // ignore: cast_nullable_to_non_nullable
              as List<String>,
      followersfavorites: null == followersfavorites
          ? _value._followersfavorites
          : followersfavorites // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FeedModel implements _FeedModel {
  const _$_FeedModel(
      {required this.id,
      required this.author,
      required this.description,
      required this.time,
      required this.username,
      required this.userimage,
      final Map<dynamic, dynamic> mediagallery = const {},
      this.date = '',
      this.title = '',
      this.bookid = '',
      this.booklogo = '',
      this.categoryKegiatan = '',
      this.tanggalkegiatan = '',
      this.keterangankegiatan = '',
      this.deskripsikegiatan = '',
      this.thumbnail = '',
      this.mediacategory = '',
      this.bookmasterid = '',
      this.pdfdownload = '',
      final List<String> mediaFeedList = const [],
      this.isphoto = false,
      this.istext = false,
      this.isaudio = false,
      this.favorites = 0,
      this.commentcount = 0,
      final List<String> followers = const [],
      final List<String> category = const [],
      final List<String> followersfavorites = const []})
      : _mediagallery = mediagallery,
        _mediaFeedList = mediaFeedList,
        _followers = followers,
        _category = category,
        _followersfavorites = followersfavorites;

  factory _$_FeedModel.fromJson(Map<String, dynamic> json) =>
      _$$_FeedModelFromJson(json);

  @override
  final String id;
  @override
  final String author;
  @override
  final String description;
  @override
  final String time;
  @override
  final String username;
  @override
  final String userimage;
  final Map<dynamic, dynamic> _mediagallery;
  @override
  @JsonKey()
  Map<dynamic, dynamic> get mediagallery {
    if (_mediagallery is EqualUnmodifiableMapView) return _mediagallery;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_mediagallery);
  }

  @override
  @JsonKey()
  final String date;
  @override
  @JsonKey()
  final String title;
  @override
  @JsonKey()
  final String bookid;
  @override
  @JsonKey()
  final String booklogo;
//@Default('') String timestamp,
  @override
  @JsonKey()
  final String categoryKegiatan;
  @override
  @JsonKey()
  final String tanggalkegiatan;
  @override
  @JsonKey()
  final String keterangankegiatan;
  @override
  @JsonKey()
  final String deskripsikegiatan;
  @override
  @JsonKey()
  final String thumbnail;
  @override
  @JsonKey()
  final String mediacategory;
  @override
  @JsonKey()
  final String bookmasterid;
  @override
  @JsonKey()
  final String pdfdownload;
  final List<String> _mediaFeedList;
  @override
  @JsonKey()
  List<String> get mediaFeedList {
    if (_mediaFeedList is EqualUnmodifiableListView) return _mediaFeedList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_mediaFeedList);
  }

  @override
  @JsonKey()
  final bool isphoto;
  @override
  @JsonKey()
  final bool istext;
  @override
  @JsonKey()
  final bool isaudio;
  @override
  @JsonKey()
  final int favorites;
  @override
  @JsonKey()
  final int commentcount;
  final List<String> _followers;
  @override
  @JsonKey()
  List<String> get followers {
    if (_followers is EqualUnmodifiableListView) return _followers;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_followers);
  }

  final List<String> _category;
  @override
  @JsonKey()
  List<String> get category {
    if (_category is EqualUnmodifiableListView) return _category;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_category);
  }

  final List<String> _followersfavorites;
  @override
  @JsonKey()
  List<String> get followersfavorites {
    if (_followersfavorites is EqualUnmodifiableListView)
      return _followersfavorites;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_followersfavorites);
  }

  @override
  String toString() {
    return 'FeedModel(id: $id, author: $author, description: $description, time: $time, username: $username, userimage: $userimage, mediagallery: $mediagallery, date: $date, title: $title, bookid: $bookid, booklogo: $booklogo, categoryKegiatan: $categoryKegiatan, tanggalkegiatan: $tanggalkegiatan, keterangankegiatan: $keterangankegiatan, deskripsikegiatan: $deskripsikegiatan, thumbnail: $thumbnail, mediacategory: $mediacategory, bookmasterid: $bookmasterid, pdfdownload: $pdfdownload, mediaFeedList: $mediaFeedList, isphoto: $isphoto, istext: $istext, isaudio: $isaudio, favorites: $favorites, commentcount: $commentcount, followers: $followers, category: $category, followersfavorites: $followersfavorites)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FeedModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.author, author) || other.author == author) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.userimage, userimage) ||
                other.userimage == userimage) &&
            const DeepCollectionEquality()
                .equals(other._mediagallery, _mediagallery) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.bookid, bookid) || other.bookid == bookid) &&
            (identical(other.booklogo, booklogo) ||
                other.booklogo == booklogo) &&
            (identical(other.categoryKegiatan, categoryKegiatan) ||
                other.categoryKegiatan == categoryKegiatan) &&
            (identical(other.tanggalkegiatan, tanggalkegiatan) ||
                other.tanggalkegiatan == tanggalkegiatan) &&
            (identical(other.keterangankegiatan, keterangankegiatan) ||
                other.keterangankegiatan == keterangankegiatan) &&
            (identical(other.deskripsikegiatan, deskripsikegiatan) ||
                other.deskripsikegiatan == deskripsikegiatan) &&
            (identical(other.thumbnail, thumbnail) ||
                other.thumbnail == thumbnail) &&
            (identical(other.mediacategory, mediacategory) ||
                other.mediacategory == mediacategory) &&
            (identical(other.bookmasterid, bookmasterid) ||
                other.bookmasterid == bookmasterid) &&
            (identical(other.pdfdownload, pdfdownload) ||
                other.pdfdownload == pdfdownload) &&
            const DeepCollectionEquality()
                .equals(other._mediaFeedList, _mediaFeedList) &&
            (identical(other.isphoto, isphoto) || other.isphoto == isphoto) &&
            (identical(other.istext, istext) || other.istext == istext) &&
            (identical(other.isaudio, isaudio) || other.isaudio == isaudio) &&
            (identical(other.favorites, favorites) ||
                other.favorites == favorites) &&
            (identical(other.commentcount, commentcount) ||
                other.commentcount == commentcount) &&
            const DeepCollectionEquality()
                .equals(other._followers, _followers) &&
            const DeepCollectionEquality().equals(other._category, _category) &&
            const DeepCollectionEquality()
                .equals(other._followersfavorites, _followersfavorites));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        author,
        description,
        time,
        username,
        userimage,
        const DeepCollectionEquality().hash(_mediagallery),
        date,
        title,
        bookid,
        booklogo,
        categoryKegiatan,
        tanggalkegiatan,
        keterangankegiatan,
        deskripsikegiatan,
        thumbnail,
        mediacategory,
        bookmasterid,
        pdfdownload,
        const DeepCollectionEquality().hash(_mediaFeedList),
        isphoto,
        istext,
        isaudio,
        favorites,
        commentcount,
        const DeepCollectionEquality().hash(_followers),
        const DeepCollectionEquality().hash(_category),
        const DeepCollectionEquality().hash(_followersfavorites)
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FeedModelCopyWith<_$_FeedModel> get copyWith =>
      __$$_FeedModelCopyWithImpl<_$_FeedModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FeedModelToJson(
      this,
    );
  }
}

abstract class _FeedModel implements FeedModel {
  const factory _FeedModel(
      {required final String id,
      required final String author,
      required final String description,
      required final String time,
      required final String username,
      required final String userimage,
      final Map<dynamic, dynamic> mediagallery,
      final String date,
      final String title,
      final String bookid,
      final String booklogo,
      final String categoryKegiatan,
      final String tanggalkegiatan,
      final String keterangankegiatan,
      final String deskripsikegiatan,
      final String thumbnail,
      final String mediacategory,
      final String bookmasterid,
      final String pdfdownload,
      final List<String> mediaFeedList,
      final bool isphoto,
      final bool istext,
      final bool isaudio,
      final int favorites,
      final int commentcount,
      final List<String> followers,
      final List<String> category,
      final List<String> followersfavorites}) = _$_FeedModel;

  factory _FeedModel.fromJson(Map<String, dynamic> json) =
      _$_FeedModel.fromJson;

  @override
  String get id;
  @override
  String get author;
  @override
  String get description;
  @override
  String get time;
  @override
  String get username;
  @override
  String get userimage;
  @override
  Map<dynamic, dynamic> get mediagallery;
  @override
  String get date;
  @override
  String get title;
  @override
  String get bookid;
  @override
  String get booklogo;
  @override //@Default('') String timestamp,
  String get categoryKegiatan;
  @override
  String get tanggalkegiatan;
  @override
  String get keterangankegiatan;
  @override
  String get deskripsikegiatan;
  @override
  String get thumbnail;
  @override
  String get mediacategory;
  @override
  String get bookmasterid;
  @override
  String get pdfdownload;
  @override
  List<String> get mediaFeedList;
  @override
  bool get isphoto;
  @override
  bool get istext;
  @override
  bool get isaudio;
  @override
  int get favorites;
  @override
  int get commentcount;
  @override
  List<String> get followers;
  @override
  List<String> get category;
  @override
  List<String> get followersfavorites;
  @override
  @JsonKey(ignore: true)
  _$$_FeedModelCopyWith<_$_FeedModel> get copyWith =>
      throw _privateConstructorUsedError;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_member_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookMemberModel _$$_BookMemberModelFromJson(Map<String, dynamic> json) =>
    _$_BookMemberModel(
      uid: json['uid'] as String,
      avatar: json['avatar'] as String? ?? '',
      bookmember: json['bookmember'] as String? ?? '',
      date: json['date'] as String? ?? '',
      memberlevel: json['memberlevel'] as String? ?? '',
      time: json['time'] as String? ?? '',
      username: json['username'] as String? ?? '',
      bookid: json['bookid'] as String? ?? '',
    );

Map<String, dynamic> _$$_BookMemberModelToJson(_$_BookMemberModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'avatar': instance.avatar,
      'bookmember': instance.bookmember,
      'date': instance.date,
      'memberlevel': instance.memberlevel,
      'time': instance.time,
      'username': instance.username,
      'bookid': instance.bookid,
    };

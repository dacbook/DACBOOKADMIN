import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_member_model.freezed.dart';
part 'book_member_model.g.dart';

@freezed
class BookMemberModel with _$BookMemberModel {
  //const HomeMenuModel._();
  const factory BookMemberModel({
    required String uid,
    @Default('') String avatar,
    @Default('')  String bookmember,
    @Default('')  String date,
    @Default('')  String memberlevel,
    @Default('')  String time,
    @Default('')  String username,
    @Default('')  String bookid,

  }) = _BookMemberModel;

  factory BookMemberModel.fromJson(Map<String, dynamic> json, String id) =>
      _$BookMemberModelFromJson(json);
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_member_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookMemberModel _$BookMemberModelFromJson(Map<String, dynamic> json) {
  return _BookMemberModel.fromJson(json);
}

/// @nodoc
mixin _$BookMemberModel {
  String get uid => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  String get bookmember => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get memberlevel => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get bookid => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookMemberModelCopyWith<BookMemberModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookMemberModelCopyWith<$Res> {
  factory $BookMemberModelCopyWith(
          BookMemberModel value, $Res Function(BookMemberModel) then) =
      _$BookMemberModelCopyWithImpl<$Res, BookMemberModel>;
  @useResult
  $Res call(
      {String uid,
      String avatar,
      String bookmember,
      String date,
      String memberlevel,
      String time,
      String username,
      String bookid});
}

/// @nodoc
class _$BookMemberModelCopyWithImpl<$Res, $Val extends BookMemberModel>
    implements $BookMemberModelCopyWith<$Res> {
  _$BookMemberModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? avatar = null,
    Object? bookmember = null,
    Object? date = null,
    Object? memberlevel = null,
    Object? time = null,
    Object? username = null,
    Object? bookid = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      bookmember: null == bookmember
          ? _value.bookmember
          : bookmember // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      memberlevel: null == memberlevel
          ? _value.memberlevel
          : memberlevel // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      bookid: null == bookid
          ? _value.bookid
          : bookid // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BookMemberModelCopyWith<$Res>
    implements $BookMemberModelCopyWith<$Res> {
  factory _$$_BookMemberModelCopyWith(
          _$_BookMemberModel value, $Res Function(_$_BookMemberModel) then) =
      __$$_BookMemberModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String uid,
      String avatar,
      String bookmember,
      String date,
      String memberlevel,
      String time,
      String username,
      String bookid});
}

/// @nodoc
class __$$_BookMemberModelCopyWithImpl<$Res>
    extends _$BookMemberModelCopyWithImpl<$Res, _$_BookMemberModel>
    implements _$$_BookMemberModelCopyWith<$Res> {
  __$$_BookMemberModelCopyWithImpl(
      _$_BookMemberModel _value, $Res Function(_$_BookMemberModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? avatar = null,
    Object? bookmember = null,
    Object? date = null,
    Object? memberlevel = null,
    Object? time = null,
    Object? username = null,
    Object? bookid = null,
  }) {
    return _then(_$_BookMemberModel(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      bookmember: null == bookmember
          ? _value.bookmember
          : bookmember // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      memberlevel: null == memberlevel
          ? _value.memberlevel
          : memberlevel // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      bookid: null == bookid
          ? _value.bookid
          : bookid // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookMemberModel implements _BookMemberModel {
  const _$_BookMemberModel(
      {required this.uid,
      this.avatar = '',
      this.bookmember = '',
      this.date = '',
      this.memberlevel = '',
      this.time = '',
      this.username = '',
      this.bookid = ''});

  factory _$_BookMemberModel.fromJson(Map<String, dynamic> json) =>
      _$$_BookMemberModelFromJson(json);

  @override
  final String uid;
  @override
  @JsonKey()
  final String avatar;
  @override
  @JsonKey()
  final String bookmember;
  @override
  @JsonKey()
  final String date;
  @override
  @JsonKey()
  final String memberlevel;
  @override
  @JsonKey()
  final String time;
  @override
  @JsonKey()
  final String username;
  @override
  @JsonKey()
  final String bookid;

  @override
  String toString() {
    return 'BookMemberModel(uid: $uid, avatar: $avatar, bookmember: $bookmember, date: $date, memberlevel: $memberlevel, time: $time, username: $username, bookid: $bookid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BookMemberModel &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.avatar, avatar) || other.avatar == avatar) &&
            (identical(other.bookmember, bookmember) ||
                other.bookmember == bookmember) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.memberlevel, memberlevel) ||
                other.memberlevel == memberlevel) &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.bookid, bookid) || other.bookid == bookid));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, uid, avatar, bookmember, date,
      memberlevel, time, username, bookid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BookMemberModelCopyWith<_$_BookMemberModel> get copyWith =>
      __$$_BookMemberModelCopyWithImpl<_$_BookMemberModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookMemberModelToJson(
      this,
    );
  }
}

abstract class _BookMemberModel implements BookMemberModel {
  const factory _BookMemberModel(
      {required final String uid,
      final String avatar,
      final String bookmember,
      final String date,
      final String memberlevel,
      final String time,
      final String username,
      final String bookid}) = _$_BookMemberModel;

  factory _BookMemberModel.fromJson(Map<String, dynamic> json) =
      _$_BookMemberModel.fromJson;

  @override
  String get uid;
  @override
  String get avatar;
  @override
  String get bookmember;
  @override
  String get date;
  @override
  String get memberlevel;
  @override
  String get time;
  @override
  String get username;
  @override
  String get bookid;
  @override
  @JsonKey(ignore: true)
  _$$_BookMemberModelCopyWith<_$_BookMemberModel> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_position_rank_model.freezed.dart';
part 'book_position_rank_model.g.dart';

@freezed
class BookPositionRank with _$BookPositionRank {
  //const HomeMenuModel._();
  const factory BookPositionRank({
    required String id,
    required String positionName,
    required String positionLevel,

    // required String useravatar,
    // @Default([]) List<String> followers,
  }) = _BookPositionRank;

  factory BookPositionRank.fromJson(Map<String, dynamic> json, String id) =>
      _$BookPositionRankFromJson(json);
}

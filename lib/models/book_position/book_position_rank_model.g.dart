// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_position_rank_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookPositionRank _$$_BookPositionRankFromJson(Map<String, dynamic> json) =>
    _$_BookPositionRank(
      id: json['id'] as String,
      positionName: json['positionName'] as String,
      positionLevel: json['positionLevel'] as String,
    );

Map<String, dynamic> _$$_BookPositionRankToJson(_$_BookPositionRank instance) =>
    <String, dynamic>{
      'id': instance.id,
      'positionName': instance.positionName,
      'positionLevel': instance.positionLevel,
    };

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_organization_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookOrganizationModel _$BookOrganizationModelFromJson(
    Map<String, dynamic> json) {
  return _BookOrganizationModel.fromJson(json);
}

/// @nodoc
mixin _$BookOrganizationModel {
  String get uid => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get positionlevel => throw _privateConstructorUsedError;
  String get positionname => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookOrganizationModelCopyWith<BookOrganizationModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookOrganizationModelCopyWith<$Res> {
  factory $BookOrganizationModelCopyWith(BookOrganizationModel value,
          $Res Function(BookOrganizationModel) then) =
      _$BookOrganizationModelCopyWithImpl<$Res, BookOrganizationModel>;
  @useResult
  $Res call(
      {String uid,
      String avatar,
      String username,
      String positionlevel,
      String positionname,
      String date});
}

/// @nodoc
class _$BookOrganizationModelCopyWithImpl<$Res,
        $Val extends BookOrganizationModel>
    implements $BookOrganizationModelCopyWith<$Res> {
  _$BookOrganizationModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? avatar = null,
    Object? username = null,
    Object? positionlevel = null,
    Object? positionname = null,
    Object? date = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      positionlevel: null == positionlevel
          ? _value.positionlevel
          : positionlevel // ignore: cast_nullable_to_non_nullable
              as String,
      positionname: null == positionname
          ? _value.positionname
          : positionname // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BookOrganizationModelCopyWith<$Res>
    implements $BookOrganizationModelCopyWith<$Res> {
  factory _$$_BookOrganizationModelCopyWith(_$_BookOrganizationModel value,
          $Res Function(_$_BookOrganizationModel) then) =
      __$$_BookOrganizationModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String uid,
      String avatar,
      String username,
      String positionlevel,
      String positionname,
      String date});
}

/// @nodoc
class __$$_BookOrganizationModelCopyWithImpl<$Res>
    extends _$BookOrganizationModelCopyWithImpl<$Res, _$_BookOrganizationModel>
    implements _$$_BookOrganizationModelCopyWith<$Res> {
  __$$_BookOrganizationModelCopyWithImpl(_$_BookOrganizationModel _value,
      $Res Function(_$_BookOrganizationModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? avatar = null,
    Object? username = null,
    Object? positionlevel = null,
    Object? positionname = null,
    Object? date = null,
  }) {
    return _then(_$_BookOrganizationModel(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      positionlevel: null == positionlevel
          ? _value.positionlevel
          : positionlevel // ignore: cast_nullable_to_non_nullable
              as String,
      positionname: null == positionname
          ? _value.positionname
          : positionname // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookOrganizationModel implements _BookOrganizationModel {
  const _$_BookOrganizationModel(
      {required this.uid,
      this.avatar = '',
      this.username = '',
      this.positionlevel = '',
      this.positionname = '',
      this.date = ''});

  factory _$_BookOrganizationModel.fromJson(Map<String, dynamic> json) =>
      _$$_BookOrganizationModelFromJson(json);

  @override
  final String uid;
  @override
  @JsonKey()
  final String avatar;
  @override
  @JsonKey()
  final String username;
  @override
  @JsonKey()
  final String positionlevel;
  @override
  @JsonKey()
  final String positionname;
  @override
  @JsonKey()
  final String date;

  @override
  String toString() {
    return 'BookOrganizationModel(uid: $uid, avatar: $avatar, username: $username, positionlevel: $positionlevel, positionname: $positionname, date: $date)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BookOrganizationModel &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.avatar, avatar) || other.avatar == avatar) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.positionlevel, positionlevel) ||
                other.positionlevel == positionlevel) &&
            (identical(other.positionname, positionname) ||
                other.positionname == positionname) &&
            (identical(other.date, date) || other.date == date));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, uid, avatar, username, positionlevel, positionname, date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BookOrganizationModelCopyWith<_$_BookOrganizationModel> get copyWith =>
      __$$_BookOrganizationModelCopyWithImpl<_$_BookOrganizationModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookOrganizationModelToJson(
      this,
    );
  }
}

abstract class _BookOrganizationModel implements BookOrganizationModel {
  const factory _BookOrganizationModel(
      {required final String uid,
      final String avatar,
      final String username,
      final String positionlevel,
      final String positionname,
      final String date}) = _$_BookOrganizationModel;

  factory _BookOrganizationModel.fromJson(Map<String, dynamic> json) =
      _$_BookOrganizationModel.fromJson;

  @override
  String get uid;
  @override
  String get avatar;
  @override
  String get username;
  @override
  String get positionlevel;
  @override
  String get positionname;
  @override
  String get date;
  @override
  @JsonKey(ignore: true)
  _$$_BookOrganizationModelCopyWith<_$_BookOrganizationModel> get copyWith =>
      throw _privateConstructorUsedError;
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_organization_model.freezed.dart';
part 'book_organization_model.g.dart';

@freezed
class BookOrganizationModel with _$BookOrganizationModel {
  //const HomeMenuModel._();
  const factory BookOrganizationModel({
    required String uid,
    @Default('') String avatar,
    @Default('') String username,
    @Default('') String positionlevel,
    @Default('') String positionname,
    @Default('') String date,
  }) = _BookOrganizationModel;

  factory BookOrganizationModel.fromJson(
          Map<String, dynamic> json, String id) =>
      _$BookOrganizationModelFromJson(json);
}

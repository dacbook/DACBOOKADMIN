// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_organization_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookOrganizationModel _$$_BookOrganizationModelFromJson(
        Map<String, dynamic> json) =>
    _$_BookOrganizationModel(
      uid: json['uid'] as String,
      avatar: json['avatar'] as String? ?? '',
      username: json['username'] as String? ?? '',
      positionlevel: json['positionlevel'] as String? ?? '',
      positionname: json['positionname'] as String? ?? '',
      date: json['date'] as String? ?? '',
    );

Map<String, dynamic> _$$_BookOrganizationModelToJson(
        _$_BookOrganizationModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'avatar': instance.avatar,
      'username': instance.username,
      'positionlevel': instance.positionlevel,
      'positionname': instance.positionname,
      'date': instance.date,
    };

// ignore: file_names
import 'package:flutter/material.dart';

import '../constants.dart';

class CloudStorageInfo {
  final String? svgSrc, title, totalStorage;
  final int? numOfFiles, percentage;
  final Color? color;

  CloudStorageInfo({
    this.svgSrc,
    this.title,
    this.totalStorage,
    this.numOfFiles,
    this.percentage,
    this.color,
  });
}

List demoMyFiles = [
  CloudStorageInfo(
    title: "Buku",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1.9GB",
    color: primaryColor,
    percentage: 0,
  ),
  CloudStorageInfo(
    title: "Organisasi",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1.9GB",
    color: primaryColor,
    percentage: 0,
  ),
  CloudStorageInfo(
    title: "Galeri",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "2.9GB",
     color: const Color(0xFF007EE5),
    percentage: 35,
  ),
  CloudStorageInfo(
    title: "Forum",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1GB",
    color: const Color(0xFF007EE5),
    percentage: 10,
  ),
  CloudStorageInfo(
    title: "Buku Kegiatan",
    numOfFiles: 5328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "7.3GB",
    color: const Color(0xFF007EE5),
    percentage: 78,
  ),
];
List myFilesInActiveCard = [
  CloudStorageInfo(
    title: "Buku",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1.9GB",
     color: const Color(0xFFFFA113),
    percentage: 0,
  ),
  CloudStorageInfo(
    title: "Organisasi",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1.9GB",
     color: const Color(0xFFFFA113),
    percentage: 0,
  ),
  CloudStorageInfo(
    title: "Galeri",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "2.9GB",
    color: const Color(0xFFFFA113),
    percentage: 35,
  ),
  CloudStorageInfo(
    title: "Forum",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1GB",
    color: const Color(0xFFFFA113),
    percentage: 10,
  ),
  CloudStorageInfo(
    title: "Buku Kegiatan",
    numOfFiles: 5328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "7.3GB",
    color: const Color(0xFFFFA113),
    percentage: 78,
  ),
];
List subbookfiles = [
 
  CloudStorageInfo(
    title: "Organisasi",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1.9GB",
    color: primaryColor,
    percentage: 0,
  ),
  CloudStorageInfo(
    title: "Galeri",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "2.9GB",
     color: const Color(0xFF007EE5),
    percentage: 35,
  ),
  CloudStorageInfo(
    title: "Forum",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1GB",
    color: const Color(0xFF007EE5),
    percentage: 10,
  ),
  CloudStorageInfo(
    title: "Buku Kegiatan",
    numOfFiles: 5328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "7.3GB",
    color: const Color(0xFF007EE5),
    percentage: 78,
  ),
];
List subbookInActiveCard = [
  
  CloudStorageInfo(
    title: "Organisasi",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1.9GB",
     color: const Color(0xFFFFA113),
    percentage: 0,
  ),
  CloudStorageInfo(
    title: "Galeri",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "2.9GB",
    color: const Color(0xFFFFA113),
    percentage: 35,
  ),
  CloudStorageInfo(
    title: "Forum",
    numOfFiles: 1328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "1GB",
    color: const Color(0xFFFFA113),
    percentage: 10,
  ),
  CloudStorageInfo(
    title: "Buku Kegiatan",
    numOfFiles: 5328,
    svgSrc: "assets/icons/Documents.svg",
    totalStorage: "7.3GB",
    color: const Color(0xFFFFA113),
    percentage: 78,
  ),
];
